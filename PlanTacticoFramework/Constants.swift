//
//  Constants.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 27/04/23.
//
import SwiftUI


let BUNDLE_NAME = "com.corack.PlanTacticoFramework"
let REALM_CONFIG = RealmConfig.shared.settings

// Colors
let BLACK_ELEKTRA = Color("black-elektra", bundle: .init(identifier: BUNDLE_NAME))
let GRAY_ELEKTRA = Color("gray-elektra", bundle: .init(identifier: BUNDLE_NAME))
let DARK_GRAY_ELEKTRA = Color("darkGray-elektra", bundle: .init(identifier: BUNDLE_NAME))
let RED_ELEKTRA = Color("red-elektra", bundle: .init(identifier: BUNDLE_NAME))
let EXPERIENCIA_COLOR = Color("experiencia", bundle: .init(identifier: BUNDLE_NAME))
let NEGOCIO_COLOR = Color("negocio", bundle: .init(identifier: BUNDLE_NAME))
let SEGUIMIENTO_COLOR = Color("seguimiento", bundle: .init(identifier: BUNDLE_NAME))
let META_COLOR = Color("meta_color", bundle: .init(identifier: BUNDLE_NAME))
let UNKNOWN_COLOR = Color.gray

// Images
let PDV_IMAGE = Image("pdv", bundle: .init(identifier: BUNDLE_NAME))
let LOGO_EKT = Image("logotipo_EKT", bundle: .init(identifier: BUNDLE_NAME))
let ICON_CREADO = Image("creado", bundle: .init(identifier: BUNDLE_NAME))
let ICON_VISUALIZADO = Image("visualizado_eye", bundle: .init(identifier: BUNDLE_NAME))
let ICON_COMPLETADO = Image("finalizado", bundle: .init(identifier: BUNDLE_NAME))
let ICON_TRASH = Image("trash", bundle: .init(identifier: BUNDLE_NAME))
let ICON_RESEND = Image("reenvio", bundle: .init(identifier: BUNDLE_NAME))
let ICON_SAVE = Image("guardado", bundle: .init(identifier: BUNDLE_NAME))
let ICON_GALLERY = Image("carrete", bundle: .init(identifier: BUNDLE_NAME))
let ICON_CAMERA = Image("foto", bundle: .init(identifier: BUNDLE_NAME))

// Alert Images
let IMG_SOPORTE = Image("soporte", bundle: .init(identifier: BUNDLE_NAME))
let IMG_CONEXION = Image("conexion", bundle: .init(identifier: BUNDLE_NAME))
let IMG_INTERNET = Image("internet", bundle: .init(identifier: BUNDLE_NAME))
let IMG_RESULTADOS = Image("resultados", bundle: .init(identifier: BUNDLE_NAME))
let IMG_INTENTAR = Image("intentas", bundle: .init(identifier: BUNDLE_NAME))


// Alert Titles
let TITLE_SOPORTE = "¡Estimado usuario!"
let TITLE_CONEXION = "¡Algo pasó!"
let TITLE_INTERNET = "¡Sin internet!"
let TITLE_RESULTADOS = "¡Estimado usuario!"
let TITLE_INTENTAR = "¡Algo salió mal!"

// Alert Descriptions
let DESC_SOPORTE = "Por el momento no hay información que mostrarle favor de ponerse en contacto con soporte técnico."
let DESC_CONEXION = "Tuvimos un problema de conexión, inténtalo más tarde."
let DESC_INTERNET = "No encontramos una red de internet.\nRevisa bien tu conexión."
let DESC_RESULTADOS = "No se encontraron resultados de la búsqueda."
let DESC_INTENTAR = "Intente más tarde"

// Fonts
func poppin_font(relativeTo: UIFont.TextStyle) -> Font {
    return Font.custom("Poppins", size: relativeTo.customSize)
}

func poppin_semibold_font(relativeTo: UIFont.TextStyle) -> Font {
    return Font.custom("Poppins SemiBold", size: relativeTo.customSize)
}

func poppin_medium_font(relativeTo: UIFont.TextStyle) -> Font {
    return Font.custom("Poppins Medium", size: relativeTo.customSize)
}

func poppin_light_font(relativeTo: UIFont.TextStyle) -> Font {
    return Font.custom("Poppins Light", size: relativeTo.customSize)
}



let static_pdvs: [PDV] = [
    PDV(ceco: "920201", name: "DAZ Plutarco Elias Calles"),
    PDV(ceco: "922187", name: "DAZ Santa Ana Coyoacán"),
    PDV(ceco: "921889", name: "EKT Ermita 2 Granias"),
    PDV(ceco: "929915", name: "EKT Taxqueña"),
    PDV(ceco: "929947", name: "Mega Plaza Central Churubusco"),
    PDV(ceco: "926936", name: "Mega Portales"),
    PDV(ceco: "922740", name: "Presta Prenda Iztacalco"),
    PDV(ceco: "923167", name: "SYR Ermita"),
    PDV(ceco: "925340", name: "TSF Azteca Digital")
]

let indicators: [IPN] = [
    IPN(title: "IPN Colaboradores", description: "Cada PDV tiene un objetivo semanal de venta de Movilidad, que integran: Motocicletas, Cascos de Seguridad y Accesorios."),
    IPN(title: "IPN Cliente", description: "Cada PDV tiene un objetivo semanal de venta de Movilidad, que integran: Motocicletas, Cascos de Seguridad y Accesorios."),
    IPN(title: "Estrellas de Servicio", description: "Cada PDV tiene un objetivo semanal de venta de Movilidad, que integran: Motocicletas, Cascos de Seguridad y Accesorios."),
    IPN(title: "Tendencia de rotación", description: "Cada PDV tiene un objetivo semanal de venta de Movilidad, que integran: Motocicletas, Cascos de Seguridad y Accesorios.")
]

let responsible_indicator: [ResponsibleIndicator] = [
    ResponsibleIndicator(id: 121, idIndicator: 21, idResponsable: 1, responsable: "Gerente Líder Punto de Venta (GLPV)"),
    ResponsibleIndicator(id: 221, idIndicator: 21, idResponsable: 2, responsable: "Líder de Comercio"),
    ResponsibleIndicator(id: 321, idIndicator: 21, idResponsable: 3, responsable: "Líder Financiero"),
    ResponsibleIndicator(id: 421, idIndicator: 21, idResponsable: 4, responsable: "Líder de Operación")
]

let actions_to_do: [Action] = [
    Action(id: 1, description: "Revisar y gestionar las solicitudes pendientes por surtir", idIndicator: 1),
    Action(id: 2, description: "Cambaceo en negocios que realizan entrega a domicilio", idIndicator: 1),
    Action(id: 3, description: "Conocimiento de campañas", idIndicator: 1),
    Action(id: 4, description: "Venta cruzada con todo el equipo del PDV", idIndicator: 1)
]

let perfiles: [(key: String, value: Int)] = [
    ("Region Xochimilco",983448),
    ("Gerente MEGA XOCHIMILCO",1061605),
    ("LIDER FINANCIERO MEGA XOCHIMILCO",35925),
    ("LIDER COMERCIAL MEGA XOCHIMILCO",1067210),
    ("LIDER OPERACION MEGA XOCHIMILCO",7046),
    ("Gerente Mega Miramontes",1068615),
    ("Gerente MEDA DF LA LUNA",313716),
    ("Gerente MEGA AV AZTECAS DF",1067208),
    ("Gerente MEGA SANTO DOMINGO DF",1084733),
    ("Gerente PRESTA PRENDA TLALPAN HUIPULCO",1067221),
    ("Gerente EKT SAN LORENZO TEZONCO",1049490),
    ("Gerente PRESTA PRENDA ARMADA DE MEX CAFETALES",945281),
    ("Gerente MEGA XOCHIMILCO MORELOS ",1080646),
    ("Gerente EKT EKT XOCHIMILCO MANANTIALES",922212),
    ("Gerente EKT EKT DAZ COYOACAN PLAZA CANTIL",1086605),
    ("Gerente DAZ COYOACAN SANTA URSULA",1088078),
    ("Gerente MEGA TLAHUAC CENTER",203977),
]
