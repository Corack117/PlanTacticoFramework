//
//  ContentView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 27/04/23.
//

import SwiftUI

struct ContentView: View {
    @State var employeeNumber: Int
    var dismiss: () -> Void = {}
    @StateObject private var dateModel = DateModel.shared
    @StateObject private var managerModel = ManagerModel.shared
    @StateObject private var keyboardHeightHelper = KeyboardHeightHelper()
    @State private var isKeyboardVisible: Bool = false
    @State private var isCreating: Bool = false
    @State private var isShowEvidenceView:  Bool = false
    @State private var selected:  Bool = false
    @State private var isLoaded:  Bool = false
    @StateObject private var alertModel:  AlertModel = AlertModel.shared
    @StateObject private var carouselModel: CarouselModel = CarouselModel.shared
    @StateObject private var toastManager = FancyToastManager.shared
    @State private var positionButton: CGSize = .zero
    @State private var scrollPosition: CGSize = .zero
    @Namespace private var namespace
    
//    init() {
//        UITextView.appearance().backgroundColor = .clear
//        for family in UIFont.familyNames {
//                 print(family)
//                 for names in UIFont.fontNames(forFamilyName: family){
//                 print("== \(names)")
//                 }
//            }
//    }
    
    var body: some View {
        GeometryReader { geo in
            if selected {
                if managerModel.isLoaded.count > 4 || isLoaded {
                    ZStack(alignment: .topLeading) {
                        Color.white.ignoresSafeArea()
                        Image(systemName: "arrow.left")
                            .frame(width: 15, height: 15)
                            .foregroundColor(DARK_GRAY_ELEKTRA)
                            .font(poppin_medium_font(relativeTo: .title2))
                            .padding(10)
                            .background(GRAY_ELEKTRA)
                            .cornerRadius(.infinity)
                            .onTapGesture {
                                withAnimation(.easeInOut(duration: 0.2)) {
                                    if !isCreating && !isShowEvidenceView {
                                        dismiss()
                                        DispatchQueue.main.async {
                                            managerModel.isLoaded = []
                                        }
                                    }
                                    if !isShowEvidenceView {
                                        isCreating = false
                                        managerModel.pdvSelectedToCreate = nil
                                    }
                                    isShowEvidenceView = false
                                    if !isCreating {
                                        managerModel.pdvName = ""
                                    }
                                }
                            }
                            .padding(.leading)
                            .padding(.top, positionButton.height / 2)
                            .ignoresSafeArea()
                            .zIndex(10)
                        VStack(spacing: 0) {
                            Image("secondHeaderBackground", bundle: .init(identifier: BUNDLE_NAME))
                                .resizable()
                                .frame(minWidth: geo.size.width, maxHeight: geo.size.height / 8)
                                .fixedSize(horizontal: true, vertical: false)
                                .clipped()
                                .contentShape(Rectangle())
                                .ignoresSafeArea(.all)
                                .background (
                                    GeometryReader { geo in
                                        Color.white.onAppear {
                                            self.positionButton = geo.size
                                        }
                                    }
                                )
                                .zIndex(2)
                            VStack(spacing: 0) {
                                HeaderView(managerModel: managerModel, dateModel: dateModel, isCreating: $isCreating, isShowEvidenceView: $isShowEvidenceView, selected: $selected, size: geo.size)
                                    .zIndex(1)
                                GeometryReader { scrollGeo in
                                    RefreshScrollView(
                                        managerModel: managerModel,
                                        isShowEvidenceView: $isShowEvidenceView,
                                        hasAlert: $alertModel.hasAlert,
                                        carousel: $carouselModel.carousel,
                                        isCreating: $isCreating,
                                        width: $scrollPosition.width,
                                        height: $scrollPosition.height
                                    )
                                    .onAppear {
                                        scrollPosition = scrollGeo.size
                                    }
                                    .onChange(of: scrollGeo.size) { newValue in
                                        scrollPosition = newValue
                                    }
                                }
                                .offset(y: -keyboardHeightHelper.keyboardHeight)
                            }
                            .frame(maxWidth: .infinity, maxHeight: .infinity)
//                            if keyboardHeightHelper.keyboardHeight > 0 {
//                                KeyboardAccessoryView()
//                                    .offset(y: -keyboardHeightHelper.keyboardHeight)
//                                    .opacity(isKeyboardVisible ? 1 : 0)
//                            }
                        }
                        .ignoresSafeArea()
                        .zIndex(2)
                        .background(
                            Color.clear
                                .matchedGeometryEffect(id: "errorMessage", in: namespace)
                        )
                        if alertModel.hasAlert {
                            AlertLogicView(alertModel: alertModel, hasAlert: $alertModel.hasAlert, isCreating: $isCreating, widthSize: .constant(geo.size.width), namespace: namespace)
                                .ignoresSafeArea()
                                .zIndex(2)
                        }
                        
                        if carouselModel.carousel {
                            CarouselView( carouselModel: carouselModel )
                                .zIndex(10)
                        }
                        
                        if managerModel.showLoader {
                            LoaderSimpleView()
                            .zIndex(100)
                        }
                        
                        if managerModel.isLoaded.count <= 4 {
                            LoaderSimpleView()
                            .zIndex(2)
                        }
                    }
                } else {
                    LoaderView()
                        .frame(maxWidth: .infinity, maxHeight: .infinity)
                    if alertModel.hasAlert {
                        AlertLogicView(
                            alertModel: alertModel,
                            hasAlert: $alertModel.hasAlert,
                            isCreating: $isCreating,
                            widthSize: .constant(geo.size.width),
                            namespace: namespace
                        )
                        .ignoresSafeArea()
                        .zIndex(2)
                    }
                }
            }
//            } else {
//                ScrollView(.vertical) {
//                    PerfilesView(managerModel: managerModel, selected: $selected)
//                }
//                .frame(maxWidth: .infinity, maxHeight: .infinity)
//                .padding()
//                .padding(.bottom)
//                .background(Color.gray)
//                .ignoresSafeArea(edges: .bottom)
//                .onAppear {
//                    isLoaded = false
//                }
//            }
        }
        .onAppear {
//            selected
            alertModel.dismiss = dismiss
            managerModel.currentEmployee = String(employeeNumber)
            selected = true
        }
        .onChange(of: selected, perform: { newValue in
            if selected {
                managerModel.fetchData(weekData: dateModel.weekData!)
            }
        })
        .onChange(of: managerModel.isLoaded.count) { newValue in
            if newValue > 4 {
                isLoaded = true
            }
        }
        .foregroundColor(.black)
        .ignoresSafeArea(.keyboard)
        .onReceive(NotificationCenter.default.publisher(for: UIResponder.keyboardDidShowNotification)) { _ in
            DispatchQueue.main.async {
                withAnimation(.easeInOut(duration: 0.2)) {
                    isKeyboardVisible = true
                }
            }
        }
        .onReceive(NotificationCenter.default.publisher(for: UIResponder.keyboardWillHideNotification)) { _ in
            DispatchQueue.main.async {
                withAnimation(.easeInOut(duration: 0.2)) {
                    isKeyboardVisible = false
                }
            }
        }
        .toastView(toast: $toastManager.toast)
        .environmentObject(managerModel)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(employeeNumber: 1084733)
    }
}
