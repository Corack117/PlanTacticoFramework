//
//  SimpleAlertView.swift
//  PlanTacticoRealm
//
//  Created by Sergio Ordaz Romero on 24/10/23.
//

import SwiftUI

struct SimpleAlertView: View {
    var title: String
    var titleButton: String
    var subtitleButton: String = "Volver"
    var secondButton: Bool = false
    @ObservedObject var alertModel: AlertModel
    @Binding var hasAlert: Bool
    @Binding var isCreating: Bool
    @Binding var widthSize: CGFloat
    
    var body: some View {
        VStack {
            VStack {
                Text(title)
                    .font(poppin_medium_font(relativeTo: .title2))
                    .multilineTextAlignment(.center)
                    .padding(.bottom, 20)
                if alertModel.showExtensionModal {
                    HStack {
                        KeyResultsView(title: "Resultados Clave", managerModel: ManagerModel.shared, isCreating: $isCreating, isHideBlocked: true)
                    }
                }
                HStack {
                    Button {
                        alertModel.type = .empty
                        alertModel.action()
                    } label: {
                        Text(titleButton)
                            .padding(.horizontal)
                            .padding(.vertical, 10)
                            .padding(.horizontal, secondButton ? 0: 30)
                            .background(RED_ELEKTRA)
                            .foregroundColor(.white)
                            .cornerRadius(5)
                            .font(poppin_semibold_font(relativeTo: .body))
                    }
                    if secondButton {
                        Button {
                            alertModel.type = .empty
                            alertModel.secondAction()
                        } label: {
                            Text(subtitleButton)
                                .padding(.horizontal)
                                .padding(.vertical, 10)
                                .padding(.horizontal, secondButton ? 0: 30)
                                .background(RED_ELEKTRA)
                                .foregroundColor(.white)
                                .cornerRadius(5)
                                .font(poppin_semibold_font(relativeTo: .body))
                        }
                    }
                }
            }
            .frame(maxWidth: widthSize * 3 / 4)
            .padding(.horizontal)
            .padding(.vertical, 40)
            .background(Color.white)
            .cornerRadius(20)
            .shadow(color: .black.opacity(0.3), radius: 5, x: 0, y: 5)
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(Color.black.opacity(0.2))
    }
}
