//
//  EvidenceView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 01/06/23.
//

import SwiftUI
import RealmSwift

struct EvidenceView: View {
    @State private var isShowPhotoLibrary = false
    @Binding var images: [String]
    @Binding var comentaries: String
    @ObservedRealmObject var tacticalPlan: TacticalPlan
    @ObservedObject var alertModel = AlertModel.shared
    
    var body: some View {
        VStack(alignment: .leading) {
            let image = Image("bn_agregar_nuevo", bundle: .init(identifier: BUNDLE_NAME))
            CustomLabelView(
                title: .constant("Cargar Evidencias"),
                image: Image("carga", bundle: .init(identifier: BUNDLE_NAME))
            )
            VerticalLabelView(title: "Agregar nuevo archivo", image: image, width: 100, height: 90)
                .onTapGesture {
                    if images.count < 10 {
                        AlertModel.shared.type = .cameraModal
                        AlertModel.shared.hasAlert = true
                    } else {
                        FancyToastManager.shared.toast = FancyToast(type: .warning, title: "Limite alcanzado", message: "Solo puedes agregar un máximo de 10 imagenes")
                    }
                }
            AttachmentsView(imagesNames: $images , showLabel: false)
            
            Text("Comentarios")
                .font(poppin_medium_font(relativeTo: .body))
            ZStack(alignment: .topLeading) {
                if comentaries.isEmpty {
                    VStack {
                        Text("Agrega algún comentario adicional")
                    }
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .font(poppin_semibold_font(relativeTo: .subheadline))
                    .foregroundColor(.black.opacity(0.3))
                    .padding(.horizontal, 10)
                    .padding(.vertical, 5)
                }
                TextView(text: $comentaries, maxCharacters: 500)
                    .frame(height: 80)
                    .cornerRadius(10)
                    .background(Color.clear)
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke()
    //                            .foregroundColor(managerModel.result.isEmpty ? EXPERIENCIA_COLOR : DARK_GRAY_ELEKTRA)
                            .foregroundColor(
                                comentaries.count < 20 &&
                                comentaries.count > 0
                                ? RED_ELEKTRA : comentaries.count == 0 ? DARK_GRAY_ELEKTRA : NEGOCIO_COLOR
                            )
                    )
            }
            Text("\(comentaries.count)/500")
                .frame(maxWidth: .infinity, alignment: .trailing)
//                .font(poppin_light_font(relativeTo: .caption1))
                .font(poppin_semibold_font(relativeTo: .caption1))
                .padding(0)
        }
        .sheet(isPresented: $alertModel.isShowPhotoLibrary) {
            if AlertModel.shared.camera {
                CameraPicker(selectedImages: self.$images)
            } else {
                ImagePicker(selectedImages: self.$images)
            }
        }
        .onAppear {
            for fileData in tacticalPlan.thaw()!.files {
                if !self.images.contains(where: { $0 == fileData.file }) {
                    if Data(base64Encoded: fileData.file) != nil {
                        self.images.append(fileData.file)
                    }
                }
            }
        }
        .onChange(of: images) { updatedImages in
            let realm = RealmConfig.shared.realm!
            try! realm.write {
                tacticalPlan.thaw()!.files.removeAll()
                for image in updatedImages {
                    if !tacticalPlan.thaw()!.files.contains(where: { $0.file == image }) {
                        let fileData = FileData(
                            idResponsable: ManagerModel.shared.idResponsable,
                            file: image
                        )
                        tacticalPlan.thaw()!.files.append(fileData)
                    }
                }
            }
        }
    }
}

struct EvidenceView_Previews: PreviewProvider {
    static var previews: some View {
        EvidenceView(images: .constant([]), comentaries: .constant(""), tacticalPlan: TacticalPlan())
    }
}
