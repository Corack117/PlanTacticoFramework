//
//  ListPDVsView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 10/05/23.
//

import SwiftUI
import RealmSwift

struct ListPDVsView: View {
    @ObservedObject var managerModel: ManagerModel
    @Binding var isCreating: Bool
    
    var body: some View {
        VStack(spacing: 20) {
            HStack{
                CustomLabelView(title: .constant("Puntos de Venta"), image: PDV_IMAGE)
                let activePlans: Int = managerModel.tacticalPlans.filter(
                    "status == %@ AND (progress != %d AND progress != %d)", TacticalPlan.Status.finished.rawValue, 3, 4
                ).count
                Text("\(activePlans) activos")
                    .font(poppin_font(relativeTo: .subheadline))
                    .padding(.vertical, 3)
                    .padding(.horizontal, 15)
                    .background(RED_ELEKTRA)
                    .foregroundColor(.white)
                    .cornerRadius(5)
            }
            ForEach(managerModel.pdvs, id: \.id) { pdv in
                let pdvPlans = managerModel.tacticalPlans.filter(
                    "ANY pdvs.ceco CONTAINS %@ AND status == %@", pdv.ceco, TacticalPlan.Status.finished.rawValue
                )
                KeyResultsView(title: pdv.full_name, id: pdv.ceco ,managerModel: managerModel, isCreating: $isCreating, ceco: pdv.ceco)
                    .overlay(
                        RoundedRectangle(cornerRadius: 5)
                            .stroke()
                            .foregroundColor(pdvPlans.isEmpty ? .clear : .green)
                    )
            }
        }
    }
}

struct ListPDVsView_Previews: PreviewProvider {
    static var previews: some View {
        ListPDVsView(managerModel: ManagerModel(), isCreating: .constant(false))
    }
}
