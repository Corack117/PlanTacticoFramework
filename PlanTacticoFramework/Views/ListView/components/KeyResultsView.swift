//
//  KeyResultsView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 10/05/23.
//

import SwiftUI
import RealmSwift

struct KeyResultsView: View {
    var title: String
    var id: String?
    @ObservedObject var managerModel: ManagerModel
    @Binding var isCreating: Bool
    @State var notSetHeader = false
    @State var isHideBlocked = false
    @State private var isShow = false
    @State private var scrollHiden = false
    @State private var selectWidth: CGFloat = .zero
    @State private var scrollWidth: CGFloat = .zero
    @State var ceco: String = ""
    @Namespace var namespace
    
    var body: some View {
//        .frame(width: scrollWidth)
        FoldView(title: .constant(title), reset: $scrollHiden, isExpanded: isHideBlocked) {
            ScrollView(.horizontal, showsIndicators: false) {
                if !isShow {
                    HStack(spacing: 0) {
                        let pdvPlans = managerModel.tacticalPlans.filter(
                            "ANY pdvs.ceco CONTAINS %@ AND status == %@", ceco, TacticalPlan.Status.finished.rawValue
                        )
                        let image = Image("bn_agregar_nuevo", bundle: .init(identifier: BUNDLE_NAME))
                        VerticalLabelView(title: "Agregar Nuevo", image: image)
                            .onTapGesture {
                                withAnimation(.spring(response: 0.1, dampingFraction: 0.7)) {
                                    scrollHiden.toggle()
                                }
                                withAnimation(.spring(response: 0.5, dampingFraction: 0.7).delay(0.2)) {
                                    isShow.toggle()
                                }
                                
                                if !notSetHeader {
                                    managerModel.pdvName = title
                                }
                            }
                        ForEach(pdvPlans, id: \.self) { plan in
                            if plan.progress != 4 {
                                ZStack(alignment: .topTrailing) {
                                    VerticalLabelView(title: plan.indicatorSelected!.descripcion, image: plan.key_result!.type.image)
                                        .onTapGesture {
                                            withAnimation(.easeOut(duration: 0.2)) {
                                                isCreating.toggle()
                                                managerModel.key_result = plan.key_result!
                                                managerModel.pdvSelectedToCreate = plan.ceco
                                                managerModel.onlyView = true
                                                managerModel.pdvName = title
                                            }
                                        }
                                    Image(plan.progress == 1 ? "" : (plan.progress == 2 ? "visualizado_eye" : "finalizado"), bundle: .init(identifier: BUNDLE_NAME))
                                        .resizable()
                                        .padding(3)
                                        .background(plan.key_result?.type.color)
                                        .aspectRatio(contentMode: .fill)
                                        .frame(
                                            width: 15,
                                            height: 15
                                        )
                                        .cornerRadius(100)
                                        .offset(x: -15, y: 35)
                                }
                            }
                        }
                    }
                    .frame(minWidth: scrollWidth, alignment: .leading)
                    .padding(0)
                    .offset(x: scrollHiden ? -selectWidth : 0)
                    .matchedGeometryEffect(id: "subselect", in: namespace)
                } else {
                    HStack(spacing: 0) {
                        ForEach(managerModel.key_results) { item in
                            Spacer()
                            VerticalLabelView(
                                title: item.descripcion,
                                image: item.type.image,
                                color: item.type.color
                            )
                            .onTapGesture {
                                if isHideBlocked {
                                    isCreating.toggle()
                                    AlertModel.shared.type = .empty
                                    AlertModel.shared.action = {}
                                    AlertModel.shared.secondAction = {}
                                    AlertModel.shared.showExtensionModal = false
                                    
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                                        withAnimation(.easeOut(duration: 0.2)) {
                                            isCreating.toggle()
                                            managerModel.key_result = item
                                            managerModel.pdvSelectedToCreate = id
                                            managerModel.onlyView = false
                                        }
                                    }
                                } else {
                                    withAnimation(.easeOut(duration: 0.2)) {
                                        isCreating.toggle()
                                        managerModel.key_result = item
                                        managerModel.pdvSelectedToCreate = id
                                        managerModel.onlyView = false
                                    }
                                }
                            }
                            Spacer()
                        }
                    }
                    .frame(minWidth: scrollWidth)
                    .padding(0)
                    .offset(x: scrollHiden ? 0 : selectWidth)
                    .matchedGeometryEffect(id: "subselect", in: namespace)
                }
            }
            .padding(0)
            .frame(maxWidth: .infinity)
            .overlay(
                GeometryReader { geo in
                    Color.clear
                        .onAppear {
                            self.scrollWidth = geo.frame(in: .local).size.width
                        }
                }
            )
        }
        .overlay(
            GeometryReader { geo in
                Color.clear
                    .onAppear {
                        self.selectWidth = geo.frame(in: .local).size.width
                    }
            }
        )
        .onAppear {
            isShow.toggle()
            scrollHiden.toggle()
        }
        .onChange(of: scrollHiden) { newValue in
            if isHideBlocked {
                scrollHiden = true
            }
            if newValue && !isShow || !newValue && isShow {
                if !isHideBlocked {
                    isShow = false
                }
            }
        }
    }
}

struct KeyResultsView_Previews: PreviewProvider {
    static var previews: some View {
        KeyResultsView(title: "Resultados Clave", managerModel: ManagerModel(), isCreating: .constant(true))
    }
}
