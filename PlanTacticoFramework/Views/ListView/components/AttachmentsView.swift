//
//  AttachmentsView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 01/06/23.
//

import SwiftUI
import UIKit
import RealmSwift
import Kingfisher

struct AttachmentsView: View {
    @Binding var imagesNames: [String]
    @State var showLabel: Bool = true
    @State var onlyShow: Bool = false
    @State private var width: CGFloat = .zero
    @State private var items: [GridItem] = [GridItem(.flexible()), GridItem(.flexible())]
    @State private var images: [UIImage] = []
    @State private var webImages: [String] = []
    
    var body: some View {
        VStack(alignment: .leading) {
            if showLabel {
                CustomLabelView(
                    title: .constant("Evidencias"),
                    image: Image("adjuntos", bundle: .init(identifier: BUNDLE_NAME))
                )
            }
            Text("Archivos Adjuntos")
                .font(poppin_medium_font(relativeTo: .body))

            HStack {
                if imagesNames.isEmpty {
                    Text("No hay evidencias")
                        .frame(maxWidth: .infinity, minHeight: 80)
                        .font(poppin_font(relativeTo: .caption1).weight(.bold))
                        .foregroundColor(DARK_GRAY_ELEKTRA)
                        .overlay(
                            RoundedRectangle(cornerRadius: 5)
                                .stroke()
                                .foregroundColor(DARK_GRAY_ELEKTRA)
                        )
                } else {
                    ScrollView(.horizontal) {
                        HStack {
                            ForEach(images, id: \.self) { uiImage in
                                ZStack(alignment: .topTrailing) {
                                    Image(uiImage: uiImage)
                                        .resizable()
                                        .aspectRatio(contentMode: .fill)
                                        .frame(width: width / 3, height: width / 4)
                                        .cornerRadius(10)
                                        .padding(.trailing, 15)
                                        .padding(.top, 15)
                                        .onTapGesture {
                                            CarouselModel.shared.carousel = true
                                            CarouselModel.shared.principalUIImage = uiImage
                                            CarouselModel.shared.uiImages = images
                                        }
                                    if !onlyShow {
                                        Text("X")
                                            .frame(width: width / 12, height: width / 12)
                                            .background(RED_ELEKTRA)
                                            .cornerRadius(100)
                                            .foregroundColor(.white)
                                            .onTapGesture {
                                                DispatchQueue.main.async {
                                                    withAnimation(.easeInOut) {
                                                        if let index = images.firstIndex(of: uiImage) {
                                                            imagesNames.remove(at: index)
                                                        }

                                                    }                                                }
                                            }
                                    }
                                }
                            }
                            
                            ForEach(webImages, id: \.self) { webImageString in
                                ZStack(alignment: .topTrailing) {
                                    KFImage(URL(string: webImageString))
                                        .resizable()
                                        .aspectRatio(contentMode: .fill)
                                        .frame(width: width / 3, height: width / 4)
                                        .cornerRadius(10)
    //                                Text("X")
    //                                    .frame(width: 30, height: 30)
    //                                    .background(RED_ELEKTRA)
    //                                    .cornerRadius(100)
    //                                    .foregroundColor(.white)
    //                                    .offset(x: 15, y: -15)
    //                                    .onTapGesture {
    //                                        let index = webImages.firstIndex(of: webImageString)!
    //                                        webImages.remove(at: index)
    //                                    }
                                }
                            }
                        }
                    }
                    .padding(.vertical)
                    .background(Color.white.opacity(0.00001))
                }

            }
        }
        .overlay(
            GeometryReader { geo in
                Color.clear
                    .onAppear {
                        let newItems = [GridItem(.flexible()), GridItem(.flexible()), GridItem(.flexible())]
                        self.width =  geo.size.width > 800 ? 380 : geo.size.width - 50
                        items = geo.size.width > 800 ? newItems : items
                    }
            }
        ) 
        .onChange(of: imagesNames) { newValue in
            DispatchQueue.main.async {
                images = []
                var webImages: [String] = []
                for base64String in imagesNames {
                    if let data: Data = Data(base64Encoded: base64String) {
                        images.append(UIImage(data: data)!)
                    } else {
                        webImages.append(APIRequest().HOST_IMAGE + base64String)
                    }
                }
                self.webImages = webImages
            }
        }
        .onAppear {
            DispatchQueue.global(qos: .background).async {
                var webImages: [String] = []
                for base64String in imagesNames {
                    if let data: Data = Data(base64Encoded: base64String) {
                        images.append(UIImage(data: data)!)
                    } else {
                        webImages.append(APIRequest().HOST_IMAGE + base64String)
                    }
                }
                self.webImages = webImages
            }
        }
    }
}

struct AttachmentsView_Previews: PreviewProvider {
    static var previews: some View {
        AttachmentsView(imagesNames: .constant([]))
    }
}
