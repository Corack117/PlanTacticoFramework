//
//  PDVSelectView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 10/05/23.
//

import SwiftUI
import RealmSwift

struct PDVSelectView: View {
    @ObservedObject var managerModel: ManagerModel
    @ObservedRealmObject var tacticalPlan: TacticalPlan
    @State private var isShow: Bool
    @State private var isExpanded: Bool
    @State private var isTotalClicked: Bool
    
    init(managerModel: ManagerModel, tacticalPlan: TacticalPlan, isShow: Bool = false, isExpanded: Bool = false) {
        self.managerModel = managerModel
        self.tacticalPlan = tacticalPlan
        
        self.isShow = tacticalPlan.pdvs.count > 0  ? true : false
        self.isExpanded = tacticalPlan.pdvs.count > 0  ? true : false
        isTotalClicked = tacticalPlan.pdvs.count == managerModel.pdvsObject.count ? true : false
    }
    
    var body: some View {
        FoldView(title: .constant("Puntos de Venta"), cancelClose: true, titleBold: true, changeView: $isShow, isExpanded: isExpanded, hideArrow: tacticalPlan.status != .finished ? false : true) {
            var pdvs = managerModel.pdvsObject.filter{
                return $0.isSelected == true }
            VStack {
                if !isShow {
                    CheckboxView(title: "Todos los puntos de venta", isActive: isTotalClicked) { isActive in
                        var updatedPDVs: [PDVObject] = managerModel.pdvsObject
                        updatedPDVs.indices.forEach { index in
                            updatedPDVs[index].isSelected = isActive
                        }
                        managerModel.pdvsObject = updatedPDVs
                        DispatchQueue.main.async {
                            isTotalClicked = isActive
                            isShow = true
                            isExpanded = false
                        }
                    }
                    .padding(5)
                    checkboxList(pdvs: managerModel.pdvsObject)
                        .onAppear {
                            DispatchQueue.main.async {
                                withAnimation(.easeInOut(duration: 0.2)) {
                                    managerModel.scrollValue?.scrollTo(1, anchor: .top)
                                }
                            }
                        }
                } else {
                    if pdvs.count > 0 {
                        if !isTotalClicked {
                            checkboxList(pdvs: pdvs)
                        } else {
                            CheckboxView(title: "Todos los puntos de venta", isActive: isTotalClicked) { isActive in
                                var updatedPDVs: [PDVObject] = managerModel.pdvsObject
                                updatedPDVs.indices.forEach { index in
                                    updatedPDVs[index].isSelected = isActive
                                }
                                managerModel.pdvsObject = updatedPDVs
                                DispatchQueue.main.async {
                                    isTotalClicked = isActive
                                    isShow = false
                                    isExpanded = true
                                }
                            }
                        }
                    } else {
                        EmptyView()
                    }
                }
            }
            .onChange(of: managerModel.pdvsObject) { newValue in
                let actives = pdvs.filter { $0.isSelected }
                if actives.isEmpty {
                    let storedPDVs = tacticalPlan.pdvs.map { $0.ceco }
                    for (index, element) in managerModel.pdvsObject.enumerated() {
                        if storedPDVs.contains(element.ceco) {
                            managerModel.pdvsObject[index].isSelected = true
                        }
                    }
                }
                
                if tacticalPlan.status != .finished {
                    DispatchQueue.main.async {
                        let realm = RealmConfig.shared.realm!
                        pdvs = managerModel.pdvsObject.filter{ $0.isSelected == true }
                        let pdvsId = pdvs.map { $0.ceco }
                        let pdvsToStorage = RealmSwift.List<PDV>()
                        pdvsToStorage.append(objectsIn: managerModel.pdvs.filter { pdvsId.contains($0.ceco) })
                        try! realm.write {
                            tacticalPlan.thaw()!.pdvs.removeAll()
                            for pdv in pdvsToStorage {
                                tacticalPlan.thaw()!.pdvs.append(pdv)
                            }
                            tacticalPlan.thaw()!.step = pdvs.count > 0 ? .responsible : .pdvs
                            
                            isTotalClicked = tacticalPlan.pdvs.count == managerModel.pdvsObject.count ? true : false
                        }
                    }
                }
            }
            .onChange(of: tacticalPlan.step) { newValue in
                let realm = RealmConfig.shared.realm!
                if newValue == .pdvs {
                    DispatchQueue.main.async {
                        withAnimation(.easeInOut(duration: 0.2)) {
                            try! realm.write {
                                tacticalPlan.thaw()!.step = pdvs.count > 0 ? .responsible : .pdvs
                            }
                        }
                    }
                }
            }
        }
        .onAppear {
            let storedPDVs = tacticalPlan.pdvs.map { $0.ceco }
            for (index, element) in managerModel.pdvsObject.enumerated() {
                if storedPDVs.contains(element.ceco) {
                    managerModel.pdvsObject[index].isSelected = true
                }
            }
        }
    }
    
    @ViewBuilder
    func checkboxList(pdvs: [PDVObject]) -> some View {
        VStack {
            ForEach(pdvs, id: \.ceco) { item in
                CheckboxView(title: item.full_name, isActive: item.isSelected) { isActive in
                    if let index = managerModel.pdvsObject.firstIndex(of: item) {
                        managerModel.pdvsObject[index].isSelected = isActive
                    }
                }
                .padding(5)
            }
        }
        .frame(maxWidth: .infinity)
        .padding(.top, 5)
    }
}

struct PDVSelectView_Previews: PreviewProvider {
    static var previews: some View {
        PDVSelectView(managerModel: ManagerModel(), tacticalPlan: TacticalPlan())
    }
}
