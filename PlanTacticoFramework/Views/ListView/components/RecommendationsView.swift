//
//  RecommendationsView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 11/05/23.
//

import SwiftUI
import RealmSwift

struct RecommendationsView: View {
    @ObservedObject var managerModel: ManagerModel
    @ObservedRealmObject var tacticalPlan: TacticalPlan
    @Binding var isOtherSelected: Bool
    @State private var heightIcon: CGFloat = .zero
    
    var body: some View {
        VStack {
            Text("Recomendaciones")
                .frame(maxWidth: .infinity, alignment: .leading)
                .font(poppin_semibold_font(relativeTo: .body))
            ZStack(alignment: .topLeading) {
                if tacticalPlan.recommendations.isEmpty {
                    Text("Describe las acciones a realizar para cumplir tu compromiso")
                        .foregroundColor(.black.opacity(0.2))
                        .padding(.horizontal, 10)
                        .padding(.vertical, 5)
                }
                TextView(text: $tacticalPlan.recommendations, maxCharacters: 500)
                    .frame(height: 80)
                    .cornerRadius(10)
                    .background(Color.clear)
//                    .scrollContentBackground(Visibility.hidden)
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke()
//                            .foregroundColor(managerModel.recommendations.isEmpty ? EXPERIENCIA_COLOR : DARK_GRAY_ELEKTRA)
                            .foregroundColor(
                                tacticalPlan.recommendations.count < 20
                                && tacticalPlan.recommendations.count > 0
                                && isOtherSelected
                                ? RED_ELEKTRA : tacticalPlan.recommendations.count > 0  && isOtherSelected ? NEGOCIO_COLOR : DARK_GRAY_ELEKTRA
                            )
                    )
                    .onTapGesture {
                        DispatchQueue.main.async {
                            withAnimation(.easeInOut(duration: 0.2)) {
                                managerModel.scrollValue?.scrollTo(5, anchor: UnitPoint(x: UnitPoint.bottom.x, y: UnitPoint.bottom.y - CGFloat(1)))
                            }
                        }
                    }
                    .disabled(tacticalPlan.status == .finished)
            }
            if tacticalPlan.status != .finished {
                Text("\(tacticalPlan.recommendations.count)/500")
                    .frame(maxWidth: .infinity, alignment: .trailing)
//                    .font(poppin_light_font(relativeTo: .caption1))
                    .font(poppin_semibold_font(relativeTo: .caption1))
                    .padding(0)
            }
            
            HStack {
                ICON_RESEND
                    .resizable()
                    .frame(width: heightIcon * 2 / 3, height: heightIcon * 2 / 3)
                Text("Reenvíar notificación")
                    .underline()
                    .font(poppin_medium_font(relativeTo: .body))
                    .foregroundColor(.black)
                    .overlay(
                        GeometryReader { geo in
                            Color.clear.onAppear {
                                self.heightIcon = geo.size.height
                            }
                            .onChange(of: geo.size.height) { value in
                                self.heightIcon = value
                            }
                        }
                    )
                    .onTapGesture {
                        let api = APIRequest()
                        api.postNotificactionSend(idPlan: Int(tacticalPlan.ceco)!) {
                            withAnimation(.easeInOut(duration: 0.2)) {
                                AlertModel.shared.hasAlert = true
                                AlertModel.shared.type = .sendMail
                            }
                        } errorRequest: {
                            withAnimation(.easeInOut(duration: 0.2)) {
                                AlertModel.shared.hasAlert = true
                                AlertModel.shared.type = .failureConection
                            }
                        }

                    }
            }
            .frame(maxWidth: .infinity, alignment: .trailing)
            .isHidden(true, remove: true)
//            .isHidden(tacticalPlan.status != .finished, remove: true)
        }
    }
}

struct RecommendationsView_Previews: PreviewProvider {
    static var previews: some View {
        RecommendationsView(managerModel: ManagerModel(), tacticalPlan: TacticalPlan(), isOtherSelected: .constant(false))
    }
}
