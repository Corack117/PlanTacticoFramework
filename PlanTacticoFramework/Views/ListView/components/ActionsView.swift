//
//  ActionsView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 11/05/23.
//

import SwiftUI
import RealmSwift

struct ActionsView: View {
    @ObservedObject var managerModel: ManagerModel
    @ObservedRealmObject var tacticalPlan: TacticalPlan
    @State var actionsFiltered: [ActionObject]
    @Binding var isOtherSelected: Bool
    
    init(managerModel: ManagerModel, tacticalPlan: TacticalPlan, isOtherSelected: Binding<Bool>) {
        self.managerModel = managerModel
        self.tacticalPlan = tacticalPlan
        self._isOtherSelected = isOtherSelected
        if let indicatorSelected = tacticalPlan.indicatorSelected {
            let actionsFilteredByID = managerModel.actions.filter { $0.idIndicator == indicatorSelected.id }
            let actionsStored = tacticalPlan.actions.map { $0.id }
            var actionsFiltered: [ActionObject] = []
            actionsFilteredByID.forEach { action in
                actionsFiltered.append(
                    ActionObject(
                        id: action.id,
                        descripcion: action.descripcion,
                        idIndicator: action.idIndicator,
                        isSelected: action.isSelected
                    )
                )
            }
            for (index, element) in actionsFiltered.enumerated() {
                if actionsStored.contains(element.id) {
                    actionsFiltered[index].isSelected = true
                }
            }
            self.actionsFiltered = actionsFiltered
        } else {
            self.actionsFiltered = []
        }
    }
    
    var body: some View {
        var actions = actionsFiltered.filter{ $0.isSelected == true }
        VStack {
            Text("Acciones a realizar")
                .frame(maxWidth: .infinity, alignment: .leading)
                .font(poppin_semibold_font(relativeTo: .body))
                .onChange(of: actionsFiltered) { newValue in
                    DispatchQueue.main.async {
                        let realm = RealmConfig.shared.realm!
                        actions =  actionsFiltered.filter{ $0.isSelected == true }
                        let actionsID = actions.map { $0.id }
                        let actionsToStorage = RealmSwift.List<Action>()
                        actionsToStorage.append(objectsIn: managerModel.actions.filter { actionsID.contains($0.id) })
                        try! realm.write {
                            tacticalPlan.thaw()!.actions.removeAll()
                            for action in actionsToStorage {
                                tacticalPlan.thaw()!.actions.append(action.thaw()!)
                            }
                            tacticalPlan.thaw()!.step = actions.count > 0 || isOtherSelected ? .recommendations : .actions
                        }
                    }
                }
            ForEach(actionsFiltered, id: \.id) { item in
                if tacticalPlan.status != .finished {
                    CheckboxView(title: item.descripcion, isActive: item.isSelected) { isActive in
                        if let index = actionsFiltered.firstIndex(of: item) {
                            actionsFiltered[index].isSelected = isActive
                        }
                    }
                    .padding(.horizontal, 5)
                    .padding(.vertical, 7)
                } else if tacticalPlan.status == .finished && item.isSelected {
                    CheckboxView(title: item.descripcion, isActive: item.isSelected) { isActive in
                        if let index = actionsFiltered.firstIndex(of: item) {
                            actionsFiltered[index].isSelected = isActive
                        }
                    }
                    .padding(.horizontal, 5)
                    .padding(.vertical, 7)
                }
            }
            if tacticalPlan.status != .finished {
                CheckboxView(title: "Otros", isActive: isOtherSelected) { conditional in
                    isOtherSelected = conditional
                }
                .padding(.horizontal, 5)
                .padding(.vertical, 7)
            }
        }
        .onChange(of: isOtherSelected, perform: { newValue in
            if tacticalPlan.status != .finished {
                DispatchQueue.main.async {
                    let realm = RealmConfig.shared.realm!
                    actions =  actionsFiltered.filter{ $0.isSelected == true }
                    let actionsID = actions.map { $0.id }
                    let actionsToStorage = RealmSwift.List<Action>()
                    actionsToStorage.append(objectsIn: managerModel.actions.filter { actionsID.contains($0.id) })
                    try! realm.write {
                        tacticalPlan.thaw()!.actions.removeAll()
                        for action in actionsToStorage {
                            tacticalPlan.thaw()!.actions.append(action.thaw()!)
                        }
                        tacticalPlan.thaw()!.step = actions.count > 0 || isOtherSelected ? .recommendations : .actions
                    }
                }
            }
        })
        .onChange(of: managerModel.step) { newValue in
            let realm = RealmConfig.shared.realm!
            if newValue == .actions {
                DispatchQueue.main.async {
                    withAnimation(.easeInOut(duration: 0.2)) {
                        try! realm.write {
                            tacticalPlan.thaw()!.step = actions.count > 0 || isOtherSelected ? .recommendations : .actions
                        }
                    }
                }
            }
        }
        .onAppear {
            DispatchQueue.main.async {
                let realm = RealmConfig.shared.realm!
                try! realm.write {
                    tacticalPlan.thaw()!.step = actions.count > 0 || isOtherSelected ? .recommendations : .actions
                }
            }
        }
    }
}

struct ActionsView_Previews: PreviewProvider {
    static var previews: some View {
        ActionsView(managerModel: ManagerModel(), tacticalPlan: TacticalPlan(), isOtherSelected: .constant(false))
    }
}
