//
//  ResultView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 11/05/23.
//

import SwiftUI
import RealmSwift

struct ResultView: View {
    @ObservedObject var managerModel: ManagerModel
    @ObservedRealmObject var tacticalPlan: TacticalPlan
    
    var body: some View {
        VStack {
            Text("Tu compromiso de esta semana")
                .frame(maxWidth: .infinity, alignment: .leading)
                .font(poppin_semibold_font(relativeTo: .body))
            Text("(debe ser retador y alcanzable)")
                .frame(maxWidth: .infinity, alignment: .leading)
                .font(poppin_semibold_font(relativeTo: .subheadline))
                .isHidden(tacticalPlan.status == .finished, remove: true)
            ZStack(alignment: .topLeading) {
                if tacticalPlan.result.isEmpty {
                    VStack {
                        Text(
                            tacticalPlan.indicatorSelected?.ejemplo ??
                            "Coloca tu resultado clave un una forma de %\nEjemplo: Cumplimiento de Tareas Zeus de un\n80% a un 90% en su ejecución"
                        )
                    }
                    .frame(maxWidth: .infinity, alignment: .topLeading)
                    .font(poppin_semibold_font(relativeTo: .subheadline))
                    .foregroundColor(.black.opacity(0.3))
                    .padding(.horizontal, 10)
                    .padding(.vertical, 5)
                }
                TextView(text: $tacticalPlan.result, maxCharacters: 200)
                    .frame(height: 120)
                    .cornerRadius(10)
                    .background(Color.clear)
                    .overlay(
                        RoundedRectangle(cornerRadius: 10)
                            .stroke()
//                            .foregroundColor(managerModel.result.isEmpty ? EXPERIENCIA_COLOR : DARK_GRAY_ELEKTRA)
                            .foregroundColor(
                                tacticalPlan.result.count < 20
                                && tacticalPlan.result.count > 0
                                ? RED_ELEKTRA : tacticalPlan.result.count == 0 ? DARK_GRAY_ELEKTRA : NEGOCIO_COLOR
                            )
                    )
                    .onTapGesture {
                        DispatchQueue.main.async {
                            withAnimation(.easeInOut(duration: 0.2)) {
                                managerModel.scrollValue?.scrollTo(3, anchor: .bottom)
                            }
                        }
                    }
                    .onReceive(
                        NotificationCenter.default.publisher(for: UIResponder.keyboardWillHideNotification)
                    ) { _ in
                        DispatchQueue.main.async {
                            withAnimation(.easeInOut(duration: 0.2)) {
                                managerModel.scrollValue?.scrollTo(4, anchor: .top)
                            }
                        }
                    }
            }
            if tacticalPlan.status != .finished {
                Text("\(tacticalPlan.result.count)/200")
                    .frame(maxWidth: .infinity, alignment: .trailing)
//                    .font(poppin_light_font(relativeTo: .caption1))
                    .font(poppin_semibold_font(relativeTo: .caption1))
                    .padding(0)
            }
        }
        .onChange(of: tacticalPlan.result) { newValue in
            if tacticalPlan.status != .finished {
                DispatchQueue.main.async {
                    let realm = RealmConfig.shared.realm!
                    let validator = newValue.count >= 20
                    try! realm.write {
                        tacticalPlan.thaw()!.step = validator ? .actions : .result
                    }
                }
            }
        }
        .onChange(of: tacticalPlan.step) { newValue in
            DispatchQueue.main.async {
                let realm = RealmConfig.shared.realm!
                if newValue == .result {
                    let validator: Bool = tacticalPlan.result.count >= 20
                    try! realm.write {
                        tacticalPlan.thaw()!.step = validator ? .actions : .result
                    }
                }
            }
        }
        .onAppear {
            DispatchQueue.main.async {
                let realm = RealmConfig.shared.realm!
                let validator = tacticalPlan.result.count >= 20
                try! realm.write {
                    tacticalPlan.thaw()!.step = validator ? .actions : .result
                }
            }
        }
    }
}

struct ResultView_Previews: PreviewProvider {
    static var previews: some View {
        ResultView(managerModel: ManagerModel(), tacticalPlan: TacticalPlan())
    }
}
