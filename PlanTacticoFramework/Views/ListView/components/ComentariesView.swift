//
//  ComentariesView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 14/06/23.
//

import SwiftUI

struct ComentariesView: View {
    @Binding var comentaries: String
    
    var body: some View {
        VStack {
            Text("Comentarios")
                .frame(maxWidth: .infinity, alignment: .leading)
                .font(poppin_medium_font(relativeTo: .body))
            Text(comentaries.isEmpty ? "Sin comentarios" : comentaries)
                .frame(maxWidth: .infinity)
                .multilineTextAlignment(.leading)
                .padding()
                .cornerRadius(10)
                .foregroundColor(comentaries.isEmpty ? DARK_GRAY_ELEKTRA : .black)
                .font(comentaries.isEmpty ? poppin_font(relativeTo: .caption1).weight(.bold) : poppin_font(relativeTo: .body))
                .overlay(
                    RoundedRectangle(cornerRadius: 10)
                        .stroke()
                        .foregroundColor(DARK_GRAY_ELEKTRA)
                )
        }
        .frame(maxWidth: .infinity)
    }
}

struct ComentariesView_Previews: PreviewProvider {
    static var previews: some View {
        ComentariesView(comentaries: .constant(""))
    }
}
