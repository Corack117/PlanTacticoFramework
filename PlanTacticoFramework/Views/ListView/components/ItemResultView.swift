//
//  ItemResultView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 10/05/23.
//

import SwiftUI
import RealmSwift

struct ItemResultView: View {
    @ObservedObject var managerModel: ManagerModel
    @StateRealmObject var tacticalPlan: TacticalPlan
    let idToSerch: String
    @Binding var isCreating: Bool
    @Binding var isShowEvidenceView: Bool
    @Binding var hasAlert: Bool
    @Binding var carousel: Bool
    @State private var isOtherSelected: Bool = false
    @Namespace private var namespace
    @State private var images: [FileData] = []
    @State private var comentaries: [CommentData] = []
    @State private var imagesToSend: [String] = []
    @State private var comentariesToSend: String = ""
    @State private var widthSize: CGFloat = .infinity
    @State private var iconHeight: CGFloat = .zero
    
    var body: some View {
        ZStack {
            VStack(alignment: .leading) {
                if !isShowEvidenceView {
                    VStack(alignment: .leading, spacing: 3) {
                        Text("Resultado Clave")
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .font(poppin_semibold_font(relativeTo: .body))
                        VerticalLabelView(
                            title: tacticalPlan.key_result?.descripcion ?? "",
                            image: tacticalPlan.key_result?.type.image ?? Image(""),
                            color: tacticalPlan.key_result?.type.color ?? Color.clear
                        )
                        Text("Indicador")
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .font(poppin_semibold_font(relativeTo: .body))
                        VStack(spacing: 20) {
                            IndicatorSelectView(tacticalPlan: tacticalPlan)
                                .isHidden(!managerModel.step.renderSteps.contains(PlanSteps.indicator), remove: true)
                                .disabled(tacticalPlan.status == .finished)
                            if managerModel.isManager && tacticalPlan.ceco.contains("general") {
                                PDVSelectView(managerModel: managerModel, tacticalPlan: tacticalPlan)
                                    .isHidden(!tacticalPlan.step.renderSteps.contains(PlanSteps.pdvs), remove: true)
                                    .disabled(tacticalPlan.status == .finished)
                                    .id(1)
                            }
                            if managerModel.isManager && tacticalPlan.status != .finished {
                                ResponsibleView(managerModel: managerModel, tacticalPlan: tacticalPlan)
                                    .isHidden(!tacticalPlan.step.renderSteps.contains(PlanSteps.responsible), remove: true)
                                    .disabled(tacticalPlan.status == .finished)
                                    .id(2)
                            }
                            ResultView(managerModel: managerModel, tacticalPlan: tacticalPlan)
                                .isHidden(!tacticalPlan.step.renderSteps.contains(PlanSteps.result), remove: true)
                                .disabled(tacticalPlan.status == .finished)
                                .id(3)
                            ActionsView(managerModel: managerModel, tacticalPlan: tacticalPlan, isOtherSelected: $isOtherSelected)
                                .isHidden(!tacticalPlan.step.renderSteps.contains(PlanSteps.actions), remove: true)
                                .disabled(tacticalPlan.status == .finished)
                                .id(4)
                            RecommendationsView(managerModel: managerModel, tacticalPlan: tacticalPlan, isOtherSelected: $isOtherSelected)
                                .isHidden(!tacticalPlan.step.renderSteps.contains(PlanSteps.recommendations), remove: true)
                                .id(5)
                            if tacticalPlan.status == .finished {
                                ResponsibleEvidenceView(tacticalPlan: tacticalPlan, carousel: $carousel, imagesNames: $images, comentaries: $comentaries)
                            }
                            Button {
                                DispatchQueue.main.async {
                                    let realm = RealmConfig.shared.realm!
                                    withAnimation(.easeInOut(duration: 0.2)) {
                                        let tacticalPlanUpdated = TacticalPlan(value: tacticalPlan)
                                        tacticalPlanUpdated.ceco = UUID().uuidString
                                        tacticalPlanUpdated.status = .finished
                                        try! realm.write {
                                            realm.delete(tacticalPlan.thaw()!)
                                            realm.create(TacticalPlan.self, value: tacticalPlanUpdated, update: .all)
                                        }
                                        let apiRequest = APIRequest()
                                        managerModel.showLoader = true
                                        
                                        apiRequest.postPlan(tacticPlan: tacticalPlanUpdated) {
                                            AlertModel.shared.action = {
                                                isCreating = false
                                                ManagerModel.shared.fetchData(weekData: DateModel.shared.weekData!)
                                                AlertModel.shared.action = {}
                                            }
                                            
                                            AlertModel.shared.secondAction = {
                                                AlertModel.shared.type = .succesTacticPlan
                                                AlertModel.shared.showExtensionModal = true
                                            }
                                            hasAlert = true
                                            AlertModel.shared.type = .succesTacticPlan
                                            
                                            DispatchQueue.main.async {
                                                managerModel.showLoader = false
                                                managerModel.pdvs.sort { pdv1, pdv2 in
                                                    let status = TacticalPlan.Status.finished
                                                    let fisrtElement = managerModel.tacticalPlans.filter("ANY pdvs.ceco CONTAINS %@ AND status == %@", pdv1.ceco, status.rawValue).count
                                                    let secondElement = managerModel.tacticalPlans.filter("ANY pdvs.ceco CONTAINS %@ AND status == %@", pdv2.ceco, status.rawValue).count
                                                    return fisrtElement > secondElement
                                                }
                                            }
                                        } errorRequest: {
                                            withAnimation(.easeInOut(duration: 0.2)) {
                                                managerModel.showLoader = false
                                                isCreating = false
                                                hasAlert = true
                                                AlertModel.shared.type = .failureConection
                                            }
                                        }
                                    }
                                }
                            } label: {
                                Text("Asignar Plan")
                                    .frame(maxWidth: .infinity, minHeight: 50)
                            }
                            .disabled(
                                tacticalPlan.step != .recommendations || (tacticalPlan.recommendations.count < 20 && isOtherSelected)
                            )
                            .background(
                                tacticalPlan.step != .recommendations || (tacticalPlan.recommendations.count < 20 && isOtherSelected) ?
                                DARK_GRAY_ELEKTRA : RED_ELEKTRA
                            )
                            .foregroundColor(.white)
                            .cornerRadius(10)
                            .shadow(color: .black.opacity(0.5), radius: 3, y: 2)
                            .opacity(
                                tacticalPlan.step != .recommendations || (tacticalPlan.recommendations.count < 20 && isOtherSelected) ? 0.5 : 1
                            )
                            .isHidden(tacticalPlan.status == .finished)
                            .isHidden(!managerModel.isManager, remove: true)
                            .disabled(tacticalPlan.status == .finished)
                        }
                    }
                    .matchedGeometryEffect(id: "changeView", in: namespace)
                    .background(
                        Color.clear
                            .matchedGeometryEffect(id: "changeView", in: namespace)
                    )
                } else {
                    VStack {
                        EvidenceView(images: $imagesToSend, comentaries: $comentariesToSend, tacticalPlan: tacticalPlan)
                    }
                    .matchedGeometryEffect(id: "changeView", in: namespace)
                    .background(
                        Color.clear
                            .matchedGeometryEffect(id: "changeView", in: namespace)
                    )
                }
                
                HStack {
                    Button {
                        
                    } label: {
                        HStack {
                            ICON_SAVE
                                .resizable()
                                .frame(width: iconHeight / 3, height: iconHeight / 3)
                            Text("Guardar")
                                .frame(minHeight: 50)
                                .overlay(
                                    GeometryReader { geo in
                                        Color.clear.onAppear {
                                            self.iconHeight = geo.size.height
                                        }
                                        .onChange(of: geo.size.height) { value in
                                            self.iconHeight = value
                                        }
                                    }
                                )
                        }
                        .frame(maxWidth: .infinity)
                    }
                    .background(GRAY_ELEKTRA)
                    .foregroundColor(.black)
                    .cornerRadius(10)
                    .shadow(color: .black.opacity(0.5), radius: 3, y: 2)
                    .font(poppin_medium_font(relativeTo: .body))
                    .isHidden(managerModel.isManager, remove: true)
                    .isHidden(tacticalPlan.ejecucion, remove: true)
                    .isHidden(!isShowEvidenceView, remove: true)
                    
                    
                    Button {
                        withAnimation(.easeInOut(duration: 0.2)) {
                            if !isShowEvidenceView {
                                isShowEvidenceView = true
                            } else {
                                DispatchQueue.main.async {
    //                                let realm = RealmConfig.shared.realm!
    //                                try! realm.write {
    //                                    tacticalPlan.thaw()!.progress = 3
    //                                    tacticalPlan.thaw()!.ejecucion = true
    //                                }
                                    let idPlan = Int(tacticalPlan.ceco)!
                                    
                                    withAnimation(.easeInOut(duration: 0.2)) {
                                        hasAlert = true
                                        AlertModel.shared.type = .completePlan
                                    }
                                    AlertModel.shared.secondAction = {
                                        DispatchQueue.main.async {
                                            withAnimation(.easeInOut(duration: 0.2)) {
                                                AlertModel.shared.type = .empty
                                                managerModel.showLoader = true
                                                AlertModel.shared.action = {}
                                                AlertModel.shared.secondAction = {}
                                                hasAlert = false
                                            }
                                        }
                                        
                                        let apiRequest = APIRequest()
                                        apiRequest.postImages(idPlan: idPlan, images: imagesToSend, comentaries: comentariesToSend) {
                                            DispatchQueue.main.async {
                                                withAnimation(.easeInOut(duration: 0.2)) {
                                                    managerModel.showLoader = false
                                                    self.isShowEvidenceView = false
                                                    self.isCreating = false
                                                }
                                            }
    
                                            DispatchQueue.main.async {
                                                let realm = RealmConfig.shared.realm!
                                                try! realm.write {
                                                    tacticalPlan.thaw()!.files.removeAll()
                                                    tacticalPlan.thaw()!.progress = 3
                                                    tacticalPlan.thaw()!.ejecucion = true
                                                }
                                            }
                                        } errorData: {
                                            DispatchQueue.main.async {
                                                withAnimation(.easeInOut(duration: 0.2)) {
                                                    managerModel.showLoader = false
                                                    hasAlert = true
                                                    AlertModel.shared.type = .failureEvidence
                                                }
                                            }
                                        } errorRequest: {
                                            DispatchQueue.main.async {
                                                withAnimation(.easeInOut(duration: 0.2)) {
                                                    managerModel.showLoader = false
                                                    hasAlert = true
                                                    AlertModel.shared.type = .failureConection
                                                }
                                            }
                                        }
                                    }
//                                    managerModel.showLoader = true
//                                    let apiRequest = APIRequest()
//                                    apiRequest.postImages(idPlan: idPlan, images: imagesToSend, comentaries: comentariesToSend) {
//                                        managerModel.showLoader = false
//                                        self.isShowEvidenceView = false
//                                        self.isCreating = false
//                                        
//                                        DispatchQueue.main.async {
//                                            let realm = RealmConfig.shared.realm!
//                                            try! realm.write {
//                                                tacticalPlan.thaw()!.progress = 3
//                                                tacticalPlan.thaw()!.ejecucion = true
//                                            }
//                                        }
//                                    } errorRequest: {
//                                        DispatchQueue.main.async {
//                                            withAnimation(.easeInOut(duration: 0.2)) {
//                                                managerModel.showLoader = false
//                                                hasAlert = true
//                                                AlertModel.shared.type = .failureConection
//                                            }
//                                        }
//                                    }
                                }
                            }
                        }
                    } label: {
                        Text(isShowEvidenceView ? "Finalizar Plan" : "Cargar Evidencias")
                            .frame(maxWidth: .infinity, minHeight: 50)
                    }
                    .background(
                        (comentariesToSend.count < 20 && isShowEvidenceView) ||
                        (imagesToSend.isEmpty && isShowEvidenceView) ?
                        DARK_GRAY_ELEKTRA : RED_ELEKTRA)
                    .foregroundColor(.white)
                    .cornerRadius(10)
                    .shadow(color: .black.opacity(0.5), radius: 3, y: 2)
                    .font(poppin_medium_font(relativeTo: .body))
                    .isHidden(managerModel.isManager, remove: true)
                    .isHidden(tacticalPlan.ejecucion, remove: true)
                    .disabled(comentariesToSend.count < 20 && isShowEvidenceView)
                    .disabled(imagesToSend.isEmpty && isShowEvidenceView)
                    .opacity(comentariesToSend.count < 20 && isShowEvidenceView ? 0.5 : 1)
                    .opacity(imagesToSend.isEmpty && isShowEvidenceView ? 0.5 : 1)
                }
            }
            .frame(maxWidth: .infinity)
            .background(
                Color.clear
                    .matchedGeometryEffect(id: "errorMessage", in: namespace)
            )
            .onChange(of: tacticalPlan.files) { newValue in
                images = Array(tacticalPlan.files)
                comentaries = Array(tacticalPlan.comentaries)
            }
            .onChange(of: comentariesToSend) { value in
                let realm = RealmConfig.shared.realm!
                try! realm.write {
                    if let comentarieSaved = self.tacticalPlan.comentaries.first(where: { $0.idResponsable == self.managerModel.idResponsable }) {
                        comentarieSaved.thaw()!.comment = value
                    } else {
                        let comment = CommentData(idResponsable: self.managerModel.idResponsable, comment: value)
                        self.tacticalPlan.thaw()!.comentaries.append(comment)
                    }
                }
            }
            .onAppear {
                DispatchQueue.main.async {
                    let realm = RealmConfig.shared.realm!
                    let unmanagedObject = TacticalPlan(value: tacticalPlan)
                    if unmanagedObject.status == .finished && unmanagedObject.actions.isEmpty {
                        isOtherSelected = true
                    }
                    if !tacticalPlan.ceco.contains("general") && tacticalPlan.status == .onProgress {
                        let pdv = managerModel.pdvs.first(where: { $0.ceco == idToSerch})
                        try! realm.write {
                            unmanagedObject.pdvs.removeAll()
                            unmanagedObject.pdvs.append(pdv!)
                        }
                    }
                    if (tacticalPlan.realm == nil) {
                        try! realm.write {
                            tacticalPlan = realm.create(TacticalPlan.self, value: unmanagedObject, update: .all)
                        }
                    } else {
                        try! realm.write {
                            realm.create(TacticalPlan.self, value: unmanagedObject, update: .all)
                        }
                    }
                    
                    if managerModel.isManager {
                        images = []
                        let apiRequest = APIRequest()
                        apiRequest.fetchImagesPlan(idPlan: tacticalPlan.ceco) { fileCoder in
                            try! realm.write {
                                for ejecicion in fileCoder.ejecuciones {
                                    for file in ejecicion.files {
                                        let fileData = FileData(
                                            idResponsable: ejecicion.idResponsable,
                                            file: file.ruta
                                        )
                                        tacticalPlan.thaw()!.files.append(fileData)
                                        let commentData = CommentData(
                                            idResponsable: ejecicion.idResponsable,
                                            comment: ejecicion.comentaries
                                        )
                                        tacticalPlan.thaw()!.comentaries.append(commentData)
                                    }
                                }
                            }
                        } errorData: {
                            DispatchQueue.main.async {
                                if managerModel.isManager {
                                    if let save = Int(tacticalPlan.ceco) {
                                        AlertModel.shared.type = .withoutEvidence
                                        AlertModel.shared.hasAlert = true
                                    }
                                }
                            }
                        }
                    } else {
                        images = Array(tacticalPlan.files)
                        comentaries = Array(tacticalPlan.comentaries)
                        if let comentarieSaved = self.tacticalPlan.comentaries.first(where: { $0.idResponsable == self.managerModel.idResponsable }) {
                            comentariesToSend = comentarieSaved.comment
                        }
                    }
                }
            }
        }
    }
}

struct ItemResultView_Previews: PreviewProvider {
    static var previews: some View {
        ItemResultView(managerModel: ManagerModel(), tacticalPlan: TacticalPlan(),
                       idToSerch: "", isCreating: .constant(false), isShowEvidenceView: .constant(true), hasAlert: .constant(true), carousel: .constant(false))
    }
}
