//
//  ResponsibleView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 11/05/23.
//

import SwiftUI
import RealmSwift

struct ResponsibleView: View {
    @ObservedObject var managerModel: ManagerModel
    @ObservedRealmObject var tacticalPlan: TacticalPlan
    @State private var isShow: Bool
    @State private var isExpanded: Bool
    
    init(managerModel: ManagerModel, tacticalPlan: TacticalPlan, isShow: Bool = false, isExpanded: Bool = false) {
        self.managerModel = managerModel
        self.tacticalPlan = tacticalPlan
        
        self.isShow = tacticalPlan.responsibles.count > 0  ? true : false
        self.isExpanded = tacticalPlan.responsibles.count > 0  ? true : false
    }
    
    var body: some View {
        FoldView(title: .constant("Responsable del indicador"), cancelClose: true, titleBold: true, changeView: $isShow, isExpanded: isExpanded) {
            var responsibles = managerModel.responsiblesObject.filter{ $0.isSelected == true }
            VStack {
                if !isShow {
                    checkboxList(
                        responsibles: managerModel.responsiblesObject.filter {
                            $0.idIndicador == managerModel.indicatorSelected!.idIndicador
                        }
                    )
                        .onAppear {
                            DispatchQueue.main.async {
                                withAnimation(.easeInOut(duration: 0.2)) {
                                    managerModel.scrollValue?.scrollTo(2, anchor: .top)
                                }
                            }
                        }
                } else {
                    if responsibles.count > 0 {
                        checkboxList(responsibles: responsibles)
                    } else {
                        EmptyView()
                    }
                    
                }
            }
            .onChange(of: managerModel.responsiblesObject) { newValue in
                let actives = responsibles.filter { $0.isSelected }
                if actives.isEmpty {
                    let storedResponsibles = tacticalPlan.responsibles.map { $0.id }
                    for (index, element) in managerModel.responsiblesObject.enumerated() {
                        if storedResponsibles.contains(element.id) {
                            managerModel.responsiblesObject[index].isSelected = true
                        }
                    }
                }
                
                if tacticalPlan.status != .finished {
                    DispatchQueue.main.async {
                        let realm = RealmConfig.shared.realm!
                        responsibles = managerModel.responsiblesObject.filter{ $0.isSelected == true }
                        let responsiblesId = responsibles.map { $0.id }
                        let responsiblesToStorage = RealmSwift.List<ResponsibleIndicator>()
                        responsiblesToStorage.append(objectsIn: managerModel.responsibles.filter { responsiblesId.contains($0.id) })
                        try! realm.write {
                            tacticalPlan.thaw()!.responsibles.removeAll()
                            for responsibles in responsiblesToStorage {
                                tacticalPlan.thaw()!.responsibles.append(responsibles.thaw()!)
                            }
                            tacticalPlan.thaw()!.step = responsiblesToStorage.count > 0 ? .result : .responsible
                        }
                    }
                }
            }
            .onChange(of: tacticalPlan.step) { newValue in
                DispatchQueue.main.async {
                    let realm = RealmConfig.shared.realm!
                    if newValue == .responsible {
                        try! realm.write {
                            tacticalPlan.thaw()!.step = responsibles.count > 0 ? .result : .responsible
                        }
                    }
                }
            }
        }
        .onAppear {
            let storedResponsibles = tacticalPlan.responsibles.map { $0.id }
            for (index, element) in managerModel.responsiblesObject.enumerated() {
                if storedResponsibles.contains(element.id) {
                    managerModel.responsiblesObject[index].isSelected = true
                }
            }
        }
    }
    
    @ViewBuilder
    func checkboxList(responsibles: [ResponsibleIndicatorObject]) -> some View {
        VStack {
            ForEach(responsibles, id: \.id) { item in
                CheckboxView(title: item.responsable, isActive: item.idResponsable == 3 ? true : item.isSelected) { isActive in
                    if let index = managerModel.responsiblesObject.firstIndex(of: item) {
                        managerModel.responsiblesObject[index].isSelected = isActive
                    }
                }
                .padding(5)
                .onAppear {
                    if item.idResponsable == 3 {
                        if let index = managerModel.responsiblesObject.firstIndex(of: item) {
                            managerModel.responsiblesObject[index].isSelected = true
                        }
                    }
                    
                    var responsibles = managerModel.responsiblesObject.filter{ $0.isSelected == true }
                    let actives = responsibles.filter { $0.isSelected }
                    if actives.isEmpty {
                        let storedResponsibles = tacticalPlan.responsibles.map { $0.id }
                        for (index, element) in managerModel.responsiblesObject.enumerated() {
                            if storedResponsibles.contains(element.id) {
                                managerModel.responsiblesObject[index].isSelected = true
                            }
                        }
                    }
                    
                    if tacticalPlan.status != .finished {
                        DispatchQueue.main.async {
                            let realm = RealmConfig.shared.realm!
                            responsibles = managerModel.responsiblesObject.filter{ $0.isSelected == true }
                            let responsiblesId = responsibles.map { $0.id }
                            let responsiblesToStorage = RealmSwift.List<ResponsibleIndicator>()
                            responsiblesToStorage.append(objectsIn: managerModel.responsibles.filter { responsiblesId.contains($0.id) })
                            try! realm.write {
                                tacticalPlan.thaw()!.responsibles.removeAll()
                                for responsibles in responsiblesToStorage {
                                    tacticalPlan.thaw()!.responsibles.append(responsibles.thaw()!)
                                }
                                tacticalPlan.thaw()!.step = responsiblesToStorage.count > 0 ? .result : .responsible
                            }
                        }
                    }
                }
            }
        }
        .frame(maxWidth: .infinity)
        .padding(.top, 5)
    }
}

struct ResponsibleView_Previews: PreviewProvider {
    static var previews: some View {
        ResponsibleView(managerModel: ManagerModel(), tacticalPlan: TacticalPlan())
    }
}
