//
//  IndicadorSelectView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 10/05/23.
//

import SwiftUI
import RealmSwift

struct IndicatorSelectView: View {
    @ObservedRealmObject var tacticalPlan: TacticalPlan
    @State private var isShow: Bool = false
    @State private var indicadorTitle: String = "Selecciona el indicador"
    @State private var height: CGFloat = .zero
    @EnvironmentObject private var managerModel: ManagerModel
    
    var body: some View {
        FoldView(title: $indicadorTitle, titleColor: .gray, cancelClose: true, reset: $isShow, isExpanded: tacticalPlan.indicatorSelected != nil, hideArrow: tacticalPlan.status != .finished ? false : true) {
            if !isShow {
                ForEach(managerModel.indicatorsFiltered) { item in
                    Text(item.descripcion)
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .background(Color.white.opacity(0.00000001))
                        .padding(.vertical, 5)
                        .font(poppin_font(relativeTo: .body))
                        .onTapGesture {
                            let realm = RealmConfig.shared.realm!
                            try! realm.write {
                                tacticalPlan.thaw()!.indicatorSelected = item.thaw()
                            }
                            managerModel.indicatorSelected = item
                            withAnimation(.easeInOut(duration: 0.2)) {
                                indicadorTitle = item.descripcion
                                isShow.toggle()
                            }
                        }
                    if managerModel.indicatorsFiltered.last != item {
                        Divider()
                    }
                }
            } else {
                VStack {
//                    HTMLTextView(
//                        htmlString: (tacticalPlan.indicatorSelected?.objetivo ?? ""),//Erroooooooooooooooooooooor
//                        height: $height
//                    )
                    Text(tacticalPlan.indicatorSelected?.objetivo ?? "")
                        .fixedSize(horizontal: false, vertical: true)
                        .font(poppin_light_font(relativeTo: .callout))
                        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .leading)
                }
//                .frame(height: height) Se requiere para HTMLTExtView
//                Text("\(Text("Objxrizontal: false, vertical: true)
            }
        }
        .onAppear {
            if (tacticalPlan.indicatorSelected != nil) {
                isShow = true
            }
        }
        .onChange(of: isShow) { _ in
            if let description = tacticalPlan.indicatorSelected?.descripcion {
                managerModel.indicatorSelected = tacticalPlan.indicatorSelected!
                indicadorTitle = description
            }
            DispatchQueue.main.async {
                let realm = RealmConfig.shared.realm!
                try! realm.write {
                    if !tacticalPlan.ceco.contains("general") && tacticalPlan.status == .onProgress {
                        tacticalPlan.thaw()!.step = .responsible
                    }else if  tacticalPlan.status != .onProgress {
                        tacticalPlan.thaw()!.step = .finish
                    } else {
                        tacticalPlan.thaw()!.step = .pdvs
                    }
                }
            }
        }
    }
}

struct IndicatorSelectView_Previews: PreviewProvider {
    static var previews: some View {
        IndicatorSelectView(tacticalPlan: TacticalPlan())
    }
}
