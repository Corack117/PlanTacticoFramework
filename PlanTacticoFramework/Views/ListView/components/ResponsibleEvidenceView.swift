//
//  ResponsibleEvidenceView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 13/07/23.
//

import SwiftUI
import RealmSwift
import Kingfisher

struct ResponsibleEvidenceView: View {
    @ObservedRealmObject var tacticalPlan: TacticalPlan
    @Binding var carousel: Bool
    @Binding var imagesNames: [FileData]
    @Binding var comentaries: [CommentData]
    @State private var width: CGFloat = .zero
    @State private var images: [(idResponsable: Int, file: UIImage)] = []
    @State private var webImages: [(idResponsable: Int, file: String)] = []
    
    var body: some View {
        VStack {
            CustomLabelView(
                title: .constant("Evidencias"),
                image: Image("adjuntos", bundle: .init(identifier: BUNDLE_NAME))
            )
            Text("Responsable del indicador")
                .frame(maxWidth: .infinity, alignment: .leading)
                .font(poppin_semibold_font(relativeTo: .body))
            VStack {
                ForEach(tacticalPlan.responsibles) { responsible in
                    FoldView(title: .constant(responsible.responsable), cancelClose: true, isExpanded: true, hideArrow: true) {
                        let idResponsible = responsible.idResponsable
                        let comment: String = comentaries.filter { $0.idResponsable == idResponsible }.first?.comment ?? ""
                        if !comment.isEmpty {
                            VStack {
                                ScrollView(.horizontal) {
                                    HStack {
                                        ForEach(images.filter { $0.idResponsable == idResponsible }, id: \.file) { uiImage in
                                            Image(uiImage: uiImage.file)
                                                .resizable()
                                                .aspectRatio(contentMode: .fill)
                                                .frame(width: width / 3, height: width / 4)
                                                .cornerRadius(10)
                                                .onTapGesture {
                                                    carousel = true
                                                    CarouselModel.shared.principalUIImage = uiImage.file
                                                    CarouselModel.shared.uiImages = images.filter { $0.idResponsable == idResponsible }.map { $0.file }
                                                }
                                        }
                                        
                                        ForEach(webImages.filter { $0.idResponsable == idResponsible }, id: \.file) { webImageString in
                                            KFImage(URL(string: webImageString.file))
                                                .resizable()
                                                .aspectRatio(contentMode: .fill)
                                                .frame(width: width / 3, height: width / 4)
                                                .cornerRadius(10)
                                                .onTapGesture {
                                                    carousel = true
                                                    CarouselModel.shared.principalImage = webImageString.file
                                                    CarouselModel.shared.images = webImages.filter { $0.idResponsable == idResponsible }.map { $0.file }
                                                }
                                        }
                                    }
                                    .padding(.vertical)
                                }
                                
                                Text("Comentarios")
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .font(poppin_semibold_font(relativeTo: .body))
                                Text("\(comment)")
                                    .frame(maxWidth: .infinity, alignment: .leading)
                                    .font(poppin_font(relativeTo: .body))
                            }
                        }
                    }
                }
            }
        }
        .overlay(
            GeometryReader { geo in
                Color.clear
                    .onAppear {
                        self.width =  geo.size.width > 800 ? 380 : geo.size.width - 50
                    }
            }
        )
        .onChange(of: imagesNames) { newValue in
            DispatchQueue.main.async {
                images = []
                var webImages: [(idResponsable: Int, file: String)] = []
                for imageData in imagesNames {
                    if let data: Data = Data(base64Encoded: imageData.file) {
                        images.append((
                            idResponsable: imageData.idResponsable,
                            file: UIImage(data: data)!
                        ))
                    } else {
                        webImages.append((
                            idResponsable: imageData.idResponsable,
                            file: APIRequest().HOST_IMAGE + imageData.file
                        ))
                    }
                }
                self.webImages = webImages
            }
        }
        .onAppear {
            DispatchQueue.global(qos: .background).async {
                var webImages: [(idResponsable: Int, file: String)] = []
                for imageData in imagesNames {
                    if let data: Data = Data(base64Encoded: imageData.file) {
                        images.append((
                            idResponsable: imageData.idResponsable,
                            file: UIImage(data: data)!
                        ))
                    } else {
                        webImages.append((
                            idResponsable: imageData.idResponsable,
                            file: APIRequest().HOST_IMAGE + imageData.file
                        ))
                    }
                }
                self.webImages = webImages
            }
        }
    }
}

struct ResponsibleEvidenceView_Previews: PreviewProvider {
    static var previews: some View {
        ResponsibleEvidenceView(tacticalPlan: TacticalPlan(), carousel: .constant(false), imagesNames: .constant([
            FileData(idResponsable: 3, file: "https://ca-times.brightspotcdn.com/dims4/default/c3f4b96/2147483647/strip/true/crop/1970x1108+39+0/resize/1200x675!/quality/80/?url=https%3A%2F%2Fcalifornia-times-brightspot.s3.amazonaws.com%2F12%2Fa5%2F79e097ccf62312d18a025f22ce48%2Fhoyla-recuento-11-cosas-aman-gatos-top-001"),
            
            FileData(idResponsable: 3, file: "https://ca-times.brightspotcdn.com/dims4/default/c3f4b96/2147483647/strip/true/crop/1970x1108+39+0/resize/1200x675!/quality/80/?url=https%3A%2F%2Fcalifornia-times-brightspot.s3.amazonaws.com%2F12%2Fa5%2F79e097ccf62312d18a025f22ce48%2Fhoyla-recuento-11-cosas-aman-gatos-top-001"),
            FileData(idResponsable: 3, file: "https://ca-times.brightspotcdn.com/dims4/default/c3f4b96/2147483647/strip/true/crop/1970x1108+39+0/resize/1200x675!/quality/80/?url=https%3A%2F%2Fcalifornia-times-brightspot.s3.amazonaws.com%2F12%2Fa5%2F79e097ccf62312d18a025f22ce48%2Fhoyla-recuento-11-cosas-aman-gatos-top-001"),
            FileData(idResponsable: 3, file: "https://ca-times.brightspotcdn.com/dims4/default/c3f4b96/2147483647/strip/true/crop/1970x1108+39+0/resize/1200x675!/quality/80/?url=https%3A%2F%2Fcalifornia-times-brightspot.s3.amazonaws.com%2F12%2Fa5%2F79e097ccf62312d18a025f22ce48%2Fhoyla-recuento-11-cosas-aman-gatos-top-001"),
        ]), comentaries: .constant([
            CommentData(idResponsable: 1, comment: "Comentarios de prueba"),
            CommentData(idResponsable: 1, comment: "Comentarios de prueba"),
            CommentData(idResponsable: 1, comment: "Comentarios de prueba"),
            CommentData(idResponsable: 1, comment: "Comentarios de prueba"),
        ]))
    }
}
