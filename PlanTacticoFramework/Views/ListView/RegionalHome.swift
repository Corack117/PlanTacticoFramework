//
//  RegionalHome.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 31/05/23.
//

import SwiftUI
import RealmSwift

struct RegionalHome: View {
    @ObservedObject var managerModel: ManagerModel
    @Binding var isCreating: Bool
    @State private var scrollWidth: CGFloat = .zero
    @Namespace var namespace
    
    var body: some View {
        VStack {
            CustomLabelView(
                title: $managerModel.currentCECOName,
                image: Image("pdv", bundle: .init(identifier: BUNDLE_NAME))
            )
            
//            let pdvPlans = tacticalPlans.where {
//                return $0.pdvs.ceco.contains(managerModel.currentCECO ?? "") && $0.status == .finished }
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(spacing: 0) {
                    if managerModel.tacticalPlans.isEmpty {
                        Text("Sin planes tácticos asignados")
                            .frame(maxWidth: .infinity)
                            .font(poppin_font(relativeTo: .caption1).weight(.bold))
                            .foregroundColor(DARK_GRAY_ELEKTRA)
                    }
                    ForEach(managerModel.tacticalPlans, id: \.self) { plan in
                        if plan.progress != 4 {
                            ZStack(alignment: .topTrailing) {
                                VerticalLabelView(title: plan.indicatorSelected!.descripcion, image: plan.key_result!.type.image)
                                    .onTapGesture {
                                        withAnimation(.easeOut(duration: 0.2)) {
                                            managerModel.key_result = plan.key_result!
                                            managerModel.pdvSelectedToCreate = plan.ceco
                                            managerModel.onlyView = true
                                            managerModel.pdvName = managerModel.currentCECOName
                                            isCreating.toggle()
                                        }
                                        let apiRequest = APIRequest()
                                        apiRequest.fetchImagesPlan(idPlan: plan.ceco) { fileCoder in
                                            let realm = try Realm(configuration: RealmConfig.shared.settings)
                                            try! realm.write {
//                                                plan.thaw()!.files.removeAll()
                                                for ejecucion in fileCoder.ejecuciones {
//                                                    plan.thaw()!.files.append("http://10.51.158.195:8181" + fileData.ruta)
                                                    for file in ejecucion.files {
                                                        if !plan.thaw()!.files.contains(where: { $0.file == file.ruta }) {
                                                            let fileData = FileData(
                                                                idResponsable: ejecucion.idResponsable,
                                                                file: file.ruta
                                                            )
                                                            plan.thaw()!.files.append(fileData)
                                                        }
                                                    }
                                                    let commentData = CommentData(
                                                        idResponsable: ejecucion.idResponsable,
                                                        comment: ejecucion.comentaries
                                                    )
                                                    plan.thaw()!.comentaries.append(commentData)
                                                }
                                                
                                            }
                                        }
                                        if plan.progress == 1 {
                                            apiRequest.putProgress(idPlan: Int(plan.ceco)!, progress_id: 2) {
                                                DispatchQueue.main.async {
                                                    let realm = RealmConfig.shared.realm!
                                                    try! realm.write {
                                                        plan.thaw()!.progress = 2
                                                    }
                                                }
                                            }
                                        }
                                    }
                                Image(plan.progress == 1 ? "" : (plan.progress == 2 ? "visualizado_eye" : "finalizado"), bundle: .init(identifier: BUNDLE_NAME))
                                    .resizable()
                                    .padding(3)
                                    .background(plan.key_result?.type.color)
                                    .aspectRatio(contentMode: .fill)
                                    .frame(
                                        width: 15,
                                        height: 15
                                    )
                                    .cornerRadius(100)
                                    .offset(x: -15, y: 35)
                            }
                        }
                    }
                }
                .frame(minWidth: scrollWidth, alignment: .leading)
                .padding(0)
            }
            .frame(maxWidth: .infinity, minHeight: 80)
            .padding(5)
            .overlay(
                RoundedRectangle(cornerRadius: 5)
                    .stroke()
                    .foregroundColor(DARK_GRAY_ELEKTRA)
            )
            .overlay(
                GeometryReader { geo in
                    Color.clear
                        .onAppear {
                            self.scrollWidth = geo.frame(in: .local).size.width
                        }
                }
            )
        }
    }
}

struct RegionalHome_Previews: PreviewProvider {
    static var previews: some View {
        RegionalHome(managerModel: ManagerModel(), isCreating: .constant(true))
    }
}
