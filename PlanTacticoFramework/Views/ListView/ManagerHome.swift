//
//  ManagerHome.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 31/05/23.
//

import SwiftUI

struct ManagerHome: View {
    @ObservedObject var managerModel: ManagerModel
    @Binding var isCreating: Bool
    
    var body: some View {
        CustomLabelView(
            title: .constant(managerModel.regionName),
            image: Image("pdv", bundle: .init(identifier: BUNDLE_NAME))
        )
        KeyResultsView(
            title: "Resultados Clave",
            managerModel: managerModel,
            isCreating: $isCreating,
            notSetHeader: true
        )
        .id(0)
        ListPDVsView(managerModel: managerModel, isCreating: $isCreating)
            .padding(.top, 30)
    }
}

struct ManagerHome_Previews: PreviewProvider {
    static var previews: some View {
        ManagerHome(managerModel: ManagerModel(), isCreating: .constant(false))
    }
}
