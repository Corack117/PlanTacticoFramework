//
//  ListView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 03/05/23.
//

import SwiftUI
import RealmSwift

struct ListView: View {
    @ObservedObject var managerModel: ManagerModel
    @Binding var isCreating: Bool
    @Binding var isShowEvidenceView: Bool
    @Binding var hasAlert: Bool
    @Binding var carousel: Bool
    @Namespace var namespace
    @State private var isSaved: Bool = false
    
    var body: some View {
        VStack {
            if !isCreating {
                VStack(spacing: 5) {
                    if managerModel.isManager {
                        ManagerHome(managerModel: managerModel, isCreating: $isCreating)
                    } else {
                        RegionalHome(managerModel: managerModel, isCreating: $isCreating)
                    }
                }
                .onAppear {
                    withAnimation(.easeInOut(duration: 0.2)) {
                        managerModel.scrollValue?.scrollTo(0, anchor: .bottom)
                    }
                }
                .matchedGeometryEffect(id: "changeView", in: namespace)
            } else {
                VStack {
                    let currentWeekNumber = DateModel.shared.getCurrentWeekNumber()
                    let suffix = String(managerModel.key_result!.id) + String(currentWeekNumber)
                    let idTofindOnProgress = managerModel.pdvSelectedToCreate ?? "general"
                    let idTofindFinished = managerModel.pdvSelectedToCreate ?? "general"
                    let idToSerch = managerModel.onlyView ? idTofindFinished : idTofindOnProgress
                    let status: TacticalPlan.Status = managerModel.onlyView ? .finished : .onProgress
                    let tacticalPlan = managerModel.tacticalPlans.filter {
                        
                        if status == .finished {
                            return ($0.ceco == idToSerch && $0.status == status)
                        }
                        
                        return ($0.ceco == idToSerch + suffix && $0.status == status)
                    }.first ?? TacticalPlan(
                        ceco: (managerModel.pdvSelectedToCreate ?? "general") + suffix,
                        key_result: managerModel.key_result!
                    )
                    ItemResultView(managerModel: managerModel, tacticalPlan: tacticalPlan, idToSerch: idToSerch, isCreating: $isCreating, isShowEvidenceView: $isShowEvidenceView, hasAlert: $hasAlert, carousel: $carousel)
                }
                .matchedGeometryEffect(id: "changeView", in: namespace)
                .onDisappear {
                    managerModel.reset()
                }
            }
            
        }
        .padding()
        .background(
            Color.white
                .onTapGesture {
                    hideKeyboard()
                }
        )
    }
}

struct ListView_Previews: PreviewProvider {
    static var previews: some View {
        ListView(managerModel: ManagerModel(), isCreating: .constant(false), isShowEvidenceView: .constant(false), hasAlert: .constant(false), carousel: .constant(false))
    }
}
