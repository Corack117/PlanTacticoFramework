//
//  RefreshScrollView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 23/06/23.
//

import SwiftUI
import UIKit

struct RefreshScrollView: UIViewRepresentable {
    @ObservedObject var managerModel: ManagerModel
    @Binding var isShowEvidenceView: Bool
    @Binding var hasAlert: Bool
    @Binding var carousel: Bool
    @Binding var isCreating: Bool
    @Binding var width: CGFloat
    @Binding var height: CGFloat
    
    @State private var refreshVC: UIHostingController<MainView>?
    
    func makeUIView(context: Context) -> UIScrollView {
        let scrollView = UIScrollView()
        scrollView.refreshControl = UIRefreshControl()
        scrollView.refreshControl?.addTarget(context.coordinator, action: #selector(Coordinator.handleRefreshControl(sender:)), for: .valueChanged)
        
        DispatchQueue.main.async {
            refreshVC = UIHostingController(
                rootView: MainView(
                    managerModel: managerModel,
                    isShowEvidenceView: $isShowEvidenceView,
                    hasAlert: $hasAlert,
                    carousel: $carousel,
                    isCreating: $isCreating
                )
            )
            refreshVC!.view.frame = CGRect(x: 0, y: 0, width: width, height: height)
            refreshVC!.view.backgroundColor = .clear
            scrollView.addSubview(refreshVC!.view)
        }
        
        return scrollView
    }
    
    func updateUIView(_ uiView: UIScrollView, context: Context) {
        refreshVC?.view.frame = CGRect(x: 0, y: 0, width: width, height: height)
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    class Coordinator: NSObject {
        var refreshScrollView: RefreshScrollView
        
        init(_ refreshScrollView: RefreshScrollView) {
            self.refreshScrollView = refreshScrollView
        }
        
        @objc func handleRefreshControl(sender: UIRefreshControl) {
            sender.endRefreshing()
            if !refreshScrollView.isCreating {
                refreshScrollView.managerModel.fetchData(weekData: DateModel.shared.weekData!)
            }
        }
    }
}
