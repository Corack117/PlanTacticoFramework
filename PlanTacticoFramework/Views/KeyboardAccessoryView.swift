//
//  KeyboardAccessoryView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 16/05/23.
//

import SwiftUI

struct KeyboardAccessoryView: View {
    var body: some View {
        HStack {
            Spacer()
            
            Button {
                hideKeyboard()
            } label: {
                Text("Terminar")
                    .foregroundColor(RED_ELEKTRA)
            }
            .padding(.trailing)
        }
        .frame(height: 44)
        .background(Color(white: 0.9))
        .ignoresSafeArea(.keyboard, edges: .bottom)
    }
}

struct KeyboardAccessoryView_Previews: PreviewProvider {
    static var previews: some View {
        KeyboardAccessoryView()
    }
}
