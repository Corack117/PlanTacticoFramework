//
//  GoalsIconView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 03/05/23.
//

import SwiftUI

struct GoalsIconView: View {
    @State var size: CGFloat
    var title: String
    
    var body: some View {
        VStack {
            Image(systemName: "calendar")
                .resizable()
                .padding()
                .frame(width: size * 2 / 3, height: size * 2 / 3)
            Text(title)
        }
        .frame(maxWidth: .infinity)
        .foregroundColor(DARK_GRAY_ELEKTRA)
    }
}

struct GoalsIconView_Previews: PreviewProvider {
    static var previews: some View {
        GoalsIconView(size: 200, title: "Calendario")
    }
}
