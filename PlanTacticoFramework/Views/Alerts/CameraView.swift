//
//  CameraView.swift
//  PlanTacticoRealm
//
//  Created by Sergio Ordaz Romero on 06/11/23.
//

import SwiftUI

struct CameraView: View {
    @State private var height: CGFloat = .zero
    
    var body: some View {
        VStack {
            VStack {
                Text("Selecciona un medio")
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .font(poppin_semibold_font(relativeTo: .title3))
                    .padding(.horizontal)
                Group {
                    HStack {
                        ICON_GALLERY
                            .resizable()
                            .frame(width: self.height * 2 / 3, height: self.height * 2 / 3)
                        Text("Adjuntar archivo")
                    }
                    .onTapGesture {
                        withAnimation(.easeInOut) {
                            AlertModel.shared.type = .empty
                            AlertModel.shared.hasAlert = false
                            AlertModel.shared.camera = false
                            AlertModel.shared.isShowPhotoLibrary = true
                        }
                    }
                    HStack {
                        ICON_CAMERA
                            .resizable()
                            .frame(width: self.height * 2 / 3, height: self.height * 2 / 3)
                        Text("Tomar foto")
                            .overlay(
                                GeometryReader { geo in
                                    Color.clear.onAppear {
                                        self.height = geo.size.height
                                    }
                                    .onChange(of: geo.size.height) { value in
                                        self.height = value
                                    }
                                }
                            )
                    }
                    .onTapGesture {
                        withAnimation(.easeInOut) {
                            AlertModel.shared.type = .empty
                            AlertModel.shared.hasAlert = false
                            AlertModel.shared.camera = true
                            AlertModel.shared.isShowPhotoLibrary = true
                        }
                    }
                }
                .font(poppin_medium_font(relativeTo: .body))
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding(.horizontal)
                .padding(.vertical, 10)
            }
            .padding(.top)
            .padding(.bottom, 20)
            .background(Color.white)
            .cornerRadius(20, corners: [.topLeft, .topRight])
            .overlay(
                VStack {
                    Image(systemName: "plus")
                        .padding()
                        .background(Color.white.opacity(0.0001))
                        .font(poppin_semibold_font(relativeTo: .title2))
                        .foregroundColor(.black)
                        .frame(alignment: .center)
                        .rotation3DEffect(.degrees(45), axis: (0, 0, 1))
                        .onTapGesture {
                            withAnimation(.easeInOut) {
                                AlertModel.shared.type = .empty
                                AlertModel.shared.hasAlert = false
                            }
                        }
                }
                    .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .topTrailing)
            )
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity, alignment: .bottom)
        .background(Color.black.opacity(0.3))
    }
}
