//
//  AlertView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 22/06/23.
//

import SwiftUI

struct AlertView: View {
    let image: Image
    let title: String
    let description: String
    @Binding var isShow: Bool
    var content: () -> Void = {}
    
    var body: some View {
        GeometryReader { geo in
            VStack {
                Spacer()
                image
                    .resizable()
                    .scaledToFit()
                    .frame(
                        width: geo.size.width * 2 / 3,
                        height: geo.size.width * 2 / 3
                    )
                Text(title)
                    .font(poppin_semibold_font(relativeTo: .title1))
                    .padding()
                Text(description)
                    .font(poppin_medium_font(relativeTo: .body))
                    .frame(width: geo.size.width * 3 / 4)
                    .multilineTextAlignment(.center)
                Spacer()
                Button {
                    isShow = false
                    content()
                } label: {
                    Text("Entendido")
                        .frame(maxWidth: .infinity)
                        .font(poppin_semibold_font(relativeTo: .title2))
                        .padding(.horizontal)
                        .padding(.vertical, 13)
                        .background(RED_ELEKTRA)
                        .foregroundColor(.white)
                        .cornerRadius(10)
                        .padding(.horizontal)
                }

            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
        }
    }
}

struct AlertView_Previews: PreviewProvider {
    static var previews: some View {
        AlertView(
            image: IMG_SOPORTE,
            title: TITLE_SOPORTE,
            description: DESC_SOPORTE,
            isShow: .constant(true)
        )
    }
}
