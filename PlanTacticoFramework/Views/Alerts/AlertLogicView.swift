//
//  AlertLogicView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 23/06/23.
//

import SwiftUI

struct AlertLogicView: View {
    @ObservedObject var alertModel: AlertModel
    @Binding var hasAlert: Bool
    @Binding var isCreating: Bool
    @Binding var widthSize: CGFloat
    var namespace: Namespace.ID
    
    var body: some View {
        switch alertModel.type {
        case .empty:
            Color.clear
        case .errorConection:
            AlertView(image: IMG_CONEXION, title: TITLE_CONEXION, description: DESC_CONEXION, isShow: $hasAlert) {
//                isCreating = false
                alertModel.type = .empty
            }
            .padding(.vertical)
            .background(
                Color.white
                    .ignoresSafeArea()
                    .matchedGeometryEffect(id: "errorMessage", in: namespace)
            )
            .padding(.vertical)
        case .cameraModal:
            CameraView()
        default:
            SimpleAlertView(
                title: alertModel.type.title,
                titleButton: alertModel.type.titleButton,
                subtitleButton: alertModel.type.subtitleButton, 
                secondButton: alertModel.type.isSecondButton,
                alertModel: alertModel,
                hasAlert: $hasAlert,
                isCreating: $isCreating,
                widthSize: $widthSize
            )
        }
    }
}

struct AlertLogicView_Previews: PreviewProvider {
    
    struct TestView: View {
            @Namespace var namespace
            var body: some View {
                AlertLogicView(alertModel: AlertModel.shared, hasAlert: .constant(false), isCreating: .constant(false), widthSize: .constant(500), namespace: namespace)
            }
        }
    
    static var previews: some View {
        TestView()
    }
}
