//
//  HTMLTextView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 08/06/23.
//

import SwiftUI

struct HTMLTextView: UIViewRepresentable {
    let htmlString: String
    @Binding var height: CGFloat
    
    func makeUIView(context: Context) -> UITextView {
        let textview = UITextView()
        textview.backgroundColor = .clear
        
        return textview
    }
    
    func updateUIView(_ uiView: UITextView, context: Context) {
        DispatchQueue.main.async {
            let style = """
        <head>
            <style>
            * {font-size: \(UIFont.TextStyle.subheadline.customSize)px}
            body {
                font-family: "Poppins Light"
            }
            b { font-family: "Poppins SemiBold" }
            </style>
        </head>
        """
            
            if let attributedText = (style + htmlString).htmlToAttributedString {
                uiView.attributedText = attributedText
            } else {
                uiView.attributedText = NSAttributedString(string: htmlString)
            }
            height = uiView.contentSize.height
        }
    }
}
