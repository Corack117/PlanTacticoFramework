//
//  MainView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 23/06/23.
//

import SwiftUI

struct MainView: View {
    @ObservedObject var managerModel: ManagerModel
    @Binding var isShowEvidenceView: Bool
    @Binding var hasAlert: Bool
    @Binding var carousel: Bool
    @Binding var isCreating: Bool
    
    var body: some View {
        VStack {
            ScrollViewReader { scroll in
                ScrollView(.vertical, showsIndicators: false) {
                    ListView(managerModel: managerModel, isCreating: $isCreating, isShowEvidenceView: $isShowEvidenceView, hasAlert: $hasAlert, carousel: $carousel)
                    Spacer(minLength: 40)
                }
                .onAppear {
                    managerModel.scrollValue = scroll
                }
                .zIndex(0)
            }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView(
            managerModel: ManagerModel.shared,
            isShowEvidenceView: .constant(false),
            hasAlert: .constant(false),
            carousel: .constant(false),
            isCreating: .constant(false)
        )
    }
}
