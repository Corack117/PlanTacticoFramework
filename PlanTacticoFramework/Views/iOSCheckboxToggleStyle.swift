//
//  iOSCheckboxToggleStyle.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 10/05/23.
//

import SwiftUI

struct iOSCheckboxToggleStyle: ToggleStyle {
    var principalColor: Color
    var secondaryColor: Color = .black
    
    
    func makeBody(configuration: Configuration) -> some View {
        
        Button(action: {
            DispatchQueue.main.async {
                configuration.isOn.toggle()
            }
        }, label: {
            HStack(alignment: .top) {
                Image(systemName: configuration.isOn ? "checkmark.square.fill" : "square")
                    .resizable()
                    .renderingMode(.template)
                    .foregroundColor(secondaryColor)
                    .fixedSize()
                    .scaleEffect(1.5)
                    .padding(.top, 5)
                configuration.label
                    .frame(maxWidth: .infinity, alignment: .topLeading)
                    .multilineTextAlignment(.leading)
                    .lineLimit(4)
                    .foregroundColor(principalColor)
                    .padding(.leading, 5)
                    .fixedSize(horizontal: false, vertical: true)
            }
            .frame(maxWidth: .infinity, alignment: .leading)
        })
    }
}
