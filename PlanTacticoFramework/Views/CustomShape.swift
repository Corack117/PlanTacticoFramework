//
//  SwiftUIView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 29/04/23.
//

import SwiftUI

struct CustomShape: Shape {
    let alignment: Alignment
    var height: CGFloat = .zero
    var width: CGFloat = .zero
    
    func path(in rect: CGRect) -> Path {
        var path = Path()
        let height = height == .zero ? rect.height : self.height
        let width = width == .zero ? rect.width : self.width
        
        switch alignment {
        case .top:
            path.addRect(CGRect(x: 0, y: 0, width: rect.width, height: height))
        case .bottom:
            path.addRect(CGRect(x: 0, y: height / 2, width: width, height: height / 2))
        case .leading:
            path.addRect(CGRect(x: 0, y: 0, width: width / 2, height: height))
        case .trailing:
            path.addRect(CGRect(x: width / 2, y: 0, width: width / 2, height: height))
        default:
            path.addRect(rect)
        }
        
        return path
    }
}
