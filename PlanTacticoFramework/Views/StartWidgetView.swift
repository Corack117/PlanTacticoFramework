//
//  StartWidgetView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 03/05/23.
//

import SwiftUI

struct StartWidgetView: View {
    @Binding var isShow: Bool
    var namespace: Namespace.ID
    
    var body: some View {
        GeometryReader { geo in
            VStack {
                Text("Objetivos")
                    .font(poppin_semibold_font(relativeTo: .title2))
                    .frame(maxWidth: .infinity, alignment: .topLeading)
                HStack {
                    GoalsIconView(size: geo.size.width / 4, title: "Experiencia")
                    GoalsIconView(size: geo.size.width / 4, title: "Negocio")
                    GoalsIconView(size: geo.size.width / 4, title: "Segimiento")
                }
                .frame(maxWidth: .infinity)
                Text("Puntos de Venta")
                    .font(poppin_semibold_font(relativeTo: .title2))
                    .padding(.vertical)
                    .frame(maxWidth: .infinity, alignment: .topLeading)
                Spacer()
                Button {
                    withAnimation(.spring(response: 0.5, dampingFraction: 0.8)) {
                        isShow.toggle()
                    }
                } label: {
                    Text("Comenzar")
                        .font(poppin_semibold_font(relativeTo: .title2))
                        .frame(maxWidth: .infinity, maxHeight: 50)
                }
                .background(Color("red-elektra", bundle: .init(identifier: BUNDLE_NAME)))
                .foregroundColor(.white)
                .cornerRadius(10)
                .matchedGeometryEffect(id: "shape", in: namespace)
            }
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .padding()
            .cornerRadius(10)
            .overlay(
                RoundedRectangle(cornerRadius: 10)
                    .stroke(lineWidth: 0.5)
                    .foregroundColor(Color("black-elektra", bundle: .init(identifier: BUNDLE_NAME)))
            )
        }
    }
}

struct StartWidgetView_Previews: PreviewProvider {
    @Namespace static var namespace
    
    static var previews: some View {
        StartWidgetView(isShow: .constant(true), namespace: namespace)
    }
}
