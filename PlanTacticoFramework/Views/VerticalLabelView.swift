//
//  VerticalLabelView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 09/05/23.
//

import SwiftUI

struct VerticalLabelView: View {
    var title: String
    var image: Image
    var color: Color = DARK_GRAY_ELEKTRA
    var width: CGFloat = 80
    var height: CGFloat = 90
    
    var body: some View {
        VStack(spacing: 2) {
            image
                .resizable()
                .fixedSize()
            Text(title)
                .multilineTextAlignment(.center)
                .lineLimit(4)
                .font(poppin_font(relativeTo: .caption1).weight(.regular))
                .foregroundColor(color)
        }
        .frame(width: width, height: height, alignment: .center)
    }
}

struct VerticalLabelView_Previews: PreviewProvider {
    static var previews: some View {
        VerticalLabelView(title: "Agregar Nuevo", image: Image("bn_agregar_nuevo", bundle: .init(identifier: BUNDLE_NAME)))
    }
}
