//
//  LoaderView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 18/05/23.
//

import SwiftUI

struct LoaderSimpleView: View {
    @State private var isLoading = false
 
    var body: some View {
        ZStack {
            Color.black.opacity(0.2)
                .ignoresSafeArea()
            VStack(spacing: 20) {
                ZStack {
                    Circle()
                        .stroke(.black.opacity(0.05), lineWidth: 12)
                        .frame(width: 70, height: 70)
                    Circle()
                        .trim(from: 0, to: 0.25)
                        .stroke(
                            RED_ELEKTRA,
                            style: StrokeStyle(lineWidth: 12, lineCap: .round)
                        )
                        .frame(width: 70, height: 70)
                        .rotationEffect(Angle(degrees: isLoading ? 360 : 0))
                        .animation(Animation.linear(duration: 1).repeatForever(autoreverses: false))
                        .onAppear() {
                            self.isLoading = true
                        }
                }
            }
            .frame(maxWidth: 200, maxHeight: 200)
            .padding()
        }
    }
}

struct LoaderSimpleView_Previews: PreviewProvider {
    static var previews: some View {
        LoaderSimpleView()
    }
}
