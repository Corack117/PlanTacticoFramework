//
//  DropdownView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 29/04/23.
//

import SwiftUI

struct DropdownView: View {
    @State private var expandedView: Bool = false
    @Binding var selection: DropdownItem
    var items: [DropdownItem]
    var dynamic: Bool = true
    var width: Double = .infinity
    var height: Double = 30
    var activeTint: Color = .black.opacity(0.1)
    var inActiveTint: Color = GRAY_ELEKTRA
    var isStroked: Bool = false
    
    @ObservedObject private var managerModel = ManagerModel.shared
    
    var body: some View {
        GeometryReader { geo in
            VStack(alignment: .leading, spacing: 0) {
                if !dynamic {
                    ClassicRowView(item: selection, size: geo.size)
                        .overlay(
                            RoundedRectangle(cornerRadius: 10)
                                .stroke(lineWidth: isStroked ? 1 : 0)
                        )
                }
                ForEach(items.filter {
                    dynamic ? true : $0 != selection
                }, id: \.id) { item in
                    ClassicRowView(item: item, size: geo.size)
                }
            }
            .background(expandedView ? inActiveTint : Color.clear)
            .cornerRadius(10)
            .offset(y: dynamic ? CGFloat(items.firstIndex(of: selection) ?? 0) * -height : 0)
        }
        .frame(height: height)
        .overlay(
            Image(systemName: managerModel.pdvSelectedToCreate == nil ? "chevron.down" : "")
                .padding(.trailing, 10)
                .rotation3DEffect(
                    .degrees(expandedView ? 180 : 0),
                    axis: (x: 1, y: 0, z: 0)
                )
            , alignment: .trailing
        )
        .mask(
            CustomShape(
                alignment: .top,
                height: expandedView ? CGFloat(items.count) * height : 0
            )
            .offset(y: dynamic && expandedView ? CGFloat(items.firstIndex(of: selection) ?? 0) * -height : 0)
        )
        .background(
            managerModel.pdvSelectedToCreate == nil ? inActiveTint.clipShape(
                RoundedRectangle(cornerRadius: 10)
            ) : Color.clear.clipShape(
                RoundedRectangle(cornerRadius: 10)
            )
        )
        .background(Color.clear)
        .shadow(color: .black.opacity(expandedView ? 0.2 : 0), radius: 3, x: 0, y: 3)
        .disabled(ManagerModel.shared.pdvSelectedToCreate != nil)
    }
    
    @ViewBuilder
    func ClassicRowView(item: DropdownItem, size: CGSize) -> some View {
        Label {
            Text(item.title)
                .font(poppin_medium_font(relativeTo: .headline).weight(.medium))
            Text(item.subtitle)
                .font(poppin_medium_font(relativeTo: .caption1))
        } icon: {
            item.image
                .resizable()
                .scaledToFit()
                .padding(1)
                .foregroundColor(RED_ELEKTRA)
        }
        .padding(5)
        .frame(width: size.width, height: size.height, alignment: .leading)
        .background(selectBackground(item: item))
        .opacity(expandedView || (!expandedView && selection.title == item.title) ? 1 : 0)
        .onTapGesture {
            withAnimation(.spring(response: 0.3, dampingFraction: 0.8)) {
                if expandedView {
                    if selection != item {
                        if let date = item.weekData?.date {
                            DateModel.shared.weekData = DateModel.shared.getCurrentWeekOfOperation(date: date)
                        }
                        ManagerModel.shared.fetchData(weekData: DateModel.shared.weekData!)
                    }
                    selection = item
                    expandedView = false
                } else {
                    if selection.title == item.title {
                        expandedView = true
                    }
                }
            }
        }
    }
    
    @ViewBuilder
    func selectBackground(item: DropdownItem) -> some View {
        if selection == item && expandedView {
            activeTint
        } else {
            inActiveTint.opacity(0.0001)
        }
    }
}

struct DropdownView_Previews: PreviewProvider {
    static var previews: some View {
        HStack {
            DropdownView(
                selection: .constant(DropdownItem(title: "Semana 14", subtitle: "(3 abril - 9 abril)")),
                items: [
                    DropdownItem(title: "Sergio"),
                    DropdownItem(title: "Enrique"),
                    DropdownItem(title: "Rocío"),
                    DropdownItem(title: "Carlos"),
                    DropdownItem(title: "David")
                ]
            )
        }
        .frame(width: 400, height: 600)
        .padding()
        .background(Color.gray)
    }
}
