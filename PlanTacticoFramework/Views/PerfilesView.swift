//
//  PerfilesView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 17/07/23.
//

import SwiftUI

struct PerfilesView: View {
    @ObservedObject var managerModel: ManagerModel
    @Binding var selected:  Bool
    
    var body: some View {
        VStack {
            ForEach(perfiles, id: \.key) { element in
                Group {
                    Text(element.key)
                        .frame(maxWidth: .infinity)
                        .padding()
                        .cornerRadius(10)
                        .background(RED_ELEKTRA)
                        .foregroundColor(.white)
                        .onTapGesture {
                            managerModel.currentEmployee = String(element.value)
                            selected = true
                        }
                }
            }
        }
    }
}

struct PerfilesView_Previews: PreviewProvider {
    static var previews: some View {
        PerfilesView(managerModel: ManagerModel.shared, selected: .constant(false))
    }
}
