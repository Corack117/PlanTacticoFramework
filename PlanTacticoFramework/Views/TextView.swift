import SwiftUI
import UIKit

struct TextView: UIViewRepresentable {
    @Binding var text: String
    var maxCharacters: Int
    var color: Color = .clear

    func makeUIView(context: Context) -> UITextView {
        let textView = UITextView()
        textView.font = .init(
            name: "Poppins",
            size: UIFont.TextStyle.body.customSize
        )
        textView.isScrollEnabled = true
        textView.isEditable = true
        textView.isUserInteractionEnabled = true
        textView.backgroundColor = UIColor(color)
        textView.textColor = UIColor(.black)
        textView.delegate = context.coordinator
        return textView
    }

    func updateUIView(_ uiView: UITextView, context: Context) {
        uiView.text = text
    }

    func makeCoordinator() -> Coordinator {
        Coordinator(text: $text, maxCharacters: maxCharacters)
    }

    class Coordinator: NSObject, UITextViewDelegate {
        @Binding var text: String
        var maxCharacters: Int

        init(text: Binding<String>, maxCharacters: Int) {
            self._text = text
            self.maxCharacters = maxCharacters
        }

        func textViewDidChange(_ textView: UITextView) {
            DispatchQueue.main.async {
                self.text = textView.text
            }
        }
        
        func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
            // get the current text, or use an empty string if that failed
            let currentText = textView.text ?? ""

            // attempt to read the range they are trying to change, or exit if we can't
            guard let stringRange = Range(range, in: currentText) else { return false }

            // add their new text to the existing text
            let updatedText = currentText.replacingCharacters(in: stringRange, with: text)

            // make sure the result is under 16 characters
            return updatedText.count <= maxCharacters
        }
    }
}
