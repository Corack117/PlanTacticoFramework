//
//  HeaderView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 28/04/23.
//

import SwiftUI
import RealmSwift

struct HeaderView: View {
    @ObservedObject var managerModel: ManagerModel
    @ObservedObject var dateModel: DateModel
    @Binding var isCreating: Bool
    @Binding var isShowEvidenceView: Bool
    @Binding var selected: Bool
    @State var selectedItem = DropdownItem(title: "")
    @State var items: [DropdownItem] = []
    @State var objectFinded: TacticalPlan?
    @State private var heightSection: CGFloat = .zero
    @State private var contentHeight: CGFloat = .zero
    var size: CGSize
    
    var body: some View {
        let dropdownWidth = size.width * 3 / 4
        VStack(alignment: .leading) {
            HStack(alignment: .bottom) {
                VStack(alignment: .leading, spacing: 0) {
//                    Image(systemName: "arrow.left")
//                        .frame(width: 15, height: 15)
//                        .foregroundColor(DARK_GRAY_ELEKTRA)
//                        .font(poppin_medium_font(relativeTo: .title2))
//                        .padding(10)
//                        .background(GRAY_ELEKTRA)
//                        .cornerRadius(.infinity)
//                        .onTapGesture {
//                            withAnimation(.easeInOut(duration: 0.2)) {
//                                if !isCreating && !isShowEvidenceView {
//                                    selected = false
//                                    DispatchQueue.main.async {
//                                        managerModel.isLoaded = []
//                                    }
//                                }
//                                if !isShowEvidenceView {
//                                    isCreating = false
//                                    managerModel.pdvSelectedToCreate = nil
//                                }
//                                isShowEvidenceView = false
//                                if !isCreating {
//                                    managerModel.pdvName = ""
//                                }
//                            }
//                        }
//                    Spacer()
                    Text("Plan Táctico")
                        .frame(maxWidth: .infinity, alignment: .leading)
                        .font(poppin_medium_font(relativeTo: .title2))
                        .foregroundColor(BLACK_ELEKTRA)
                        .padding(2)
                        .padding(.bottom, 4)
                    Spacer()
                    Group {
                        Label {
                            Text(managerModel.regionName)
                                .font(poppin_medium_font(relativeTo: .body))
                        } icon: {
                            Image(systemName: "map")
                                .foregroundColor(RED_ELEKTRA)
                        }
                        .background(
                            GeometryReader { geo in
                                Color.clear
                                    .onAppear {
                                        heightSection = geo.size.height
                                    }
                            }
                        )
                        
                        if isCreating && !managerModel.pdvName.isEmpty {
                            Label {
                                Text(managerModel.pdvName)
                                    .minimumScaleFactor(0.5)
                                    .lineLimit(1)
                                    .font(poppin_medium_font(relativeTo: .headline))
                                    .frame(maxHeight: .infinity)
                            } icon: {
                                PDV_IMAGE
                                    .resizable()
                                    .scaledToFit()
                                    .frame(maxHeight: heightSection)
                                    .padding(.vertical, 5)
                            }
                            .frame(height: heightSection)
                        }
                    }
                    .padding(5)
                    .frame(maxWidth: size.width * 3 / 4, alignment: .leading)
                    .font(poppin_medium_font(relativeTo: .headline))
//                    .background(GRAY_ELEKTRA)
                    .background(Color.clear)
                    .cornerRadius(10)
                    
                    
                    DropdownView(
                        selection: $selectedItem,
                        items: items,
                        dynamic: false,
                        width: dropdownWidth
                    )
                    .frame(maxWidth: size.width * 3 / 4, alignment: .leading)
                    .onAppear {
                        let currentWeek = dateModel.getCurrentWeekOfOperation()
                        let weeks = dateModel.getFiveWeeks()
                        selectedItem = DropdownItem(
                            title: currentWeek.week,
                            subtitle: currentWeek.rangeWeek,
                            image: Image("calendario", bundle: .init(identifier: BUNDLE_NAME)),
                            weekData: currentWeek
                        )
                        for week in weeks {
                            items.append(
                                DropdownItem(
                                    title: week.week,
                                    subtitle: week.rangeWeek,
                                    image: Image("calendario", bundle: .init(identifier: BUNDLE_NAME)),
                                    weekData: week
                                )
                            )
                        }
                    }
                }
                .background(
                    GeometryReader { geo in
                        Color.clear.onAppear {
                            contentHeight = geo.size.height
                        }
                        .onChange(of: geo.size.height) { newValue in
                            withAnimation(.easeInOut) {
                                contentHeight = newValue
                            }
                        }
                    }
                )
                Button {
                    if let pdv  = managerModel.pdvSelectedToCreate {
                        let api = APIRequest()
                        DispatchQueue.main.async {
                            let realm = RealmConfig.shared.realm!
                            try! realm.write {
                                if let obj = objectFinded {
                                    managerModel.pdvSelectedToCreate = nil
                                    realm.delete(obj)
                                }
                                self.isShowEvidenceView = false
                                self.isCreating = false
                            }
                        }
                        api.putProgress(idPlan: Int(pdv) ?? -1, progress_id: 4) { 
                            api.deletePlan(idPlan: Int(pdv) ?? -1) {}
                        }
                    }
                } label: {
                    ICON_TRASH
                        .resizable()
                        .scaledToFill()
                        .frame(width: 20, height: 20)
                        .padding(15)
                }
                .isHidden(managerModel.pdvSelectedToCreate == nil || objectFinded?.progress != 1 || !managerModel.isManager)
            }
        }
        .onChange(of: managerModel.pdvSelectedToCreate) { newValue in
            let realm = RealmConfig.shared.realm!
            objectFinded = realm.objects(TacticalPlan.self).first(where: { $0.ceco == managerModel.pdvSelectedToCreate ?? "" })
        }
        .frame(
            maxWidth: .infinity,
            maxHeight: contentHeight
        )
        .padding([.horizontal, .bottom])
        .padding([.leading, .trailing], 5)
        .background(
            Color.white
                .cornerRadius(20, corners: [.bottomLeft, .bottomRight])
                .shadow(color: .black.opacity(0.05), radius: 5, x: 0, y: 10)
                .shadow(color: .gray.opacity(0.2), radius: 10, x: 0, y: 20)
                .onTapGesture {
                    hideKeyboard()
                }
        )
        .fixedSize(horizontal: false, vertical: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/)
    }
}

struct HeaderView_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            HeaderView(
                managerModel: ManagerModel(),
                dateModel: DateModel.shared,
                isCreating: .constant(true),
                isShowEvidenceView: .constant(false),
                selected: .constant(true),
                size: .init(width: 400, height: 1000)
            )
            Spacer()
        }
    }
}
