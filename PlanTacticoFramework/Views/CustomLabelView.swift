//
//  ListTitleView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 10/05/23.
//

import SwiftUI

struct CustomLabelView: View {
    @Binding var title: String
    var image: Image
    @State private var labelHeight = CGFloat.zero
    
    var body: some View {
        Label {
            Text(title)
                .minimumScaleFactor(0.5)
                .lineLimit(1)
                .font(poppin_semibold_font(relativeTo: .title3))
                .frame(maxHeight: .infinity)
        } icon: {
            image
                .resizable()
                .scaledToFit()
                .padding(.horizontal, 2)
                .padding(.vertical, 10)
        }
        .font(poppin_semibold_font(relativeTo: .title3))
        .foregroundColor(RED_ELEKTRA)
        .frame(maxWidth: .infinity, maxHeight: 40, alignment: .leading)
    }
}

struct CustomLabelView_Previews: PreviewProvider {
    static var previews: some View {
        CustomLabelView(
            title: .constant("Región Taxqueña"),
            image: Image("pdv", bundle: .init(identifier: BUNDLE_NAME))
        )
    }
}
