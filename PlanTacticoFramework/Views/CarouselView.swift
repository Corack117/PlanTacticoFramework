//
//  CarouselView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 13/07/23.
//

import SwiftUI
import Kingfisher

struct CarouselView: View {
    @State var size: CGSize = .zero
    @ObservedObject var carouselModel: CarouselModel
    @State private var scrollR: ScrollViewProxy?
    @State private var width: CGFloat = .zero
    
    var body: some View {
        ZStack(alignment: .topTrailing) {
            VStack {
                if !carouselModel.principalImage.isEmpty {
                    KFImage(URL(string: carouselModel.principalImage))
                        .resizable()
                        .scaledToFit()
                        .cornerRadius(10)
                        .frame(
                            maxWidth: .infinity,
                            minHeight: size.height / 2.5,
                            maxHeight: size.height / 2.5
                        )
                        .background(DARK_GRAY_ELEKTRA.opacity(0.1))
                        .cornerRadius(10)
    //                    .overlay(
    //                        RoundedRectangle(cornerRadius: 10)
    //                            .stroke(lineWidth: 3)
    //                            .foregroundColor(DARK_GRAY_ELEKTRA)
    //                    )
                        .padding(.top, 40)
                } else {
                    Image(uiImage: carouselModel.principalUIImage)
                        .resizable()
                        .scaledToFit()
                        .cornerRadius(10)
                        .frame(
                            maxWidth: .infinity,
                            minHeight: size.height / 2.5,
                            maxHeight: size.height / 2.5
                        )
                        .background(DARK_GRAY_ELEKTRA.opacity(0.1))
                        .cornerRadius(10)
    //                    .overlay(
    //                        RoundedRectangle(cornerRadius: 10)
    //                            .stroke(lineWidth: 3)
    //                            .foregroundColor(DARK_GRAY_ELEKTRA)
    //                    )
                        .padding(.top, 40)
                }
                HStack {
                    Image(systemName: "chevron.left")
                        .padding(10)
                        .onTapGesture {
                            if !carouselModel.principalImage.isEmpty {
                                let currentIndex = carouselModel.images.firstIndex(of: carouselModel.principalImage)!
                                print(currentIndex)
                                if currentIndex == 0 {
                                    print(carouselModel.principalImage)
                                    carouselModel.principalImage = carouselModel.images.last ?? ""
                                    print(carouselModel.principalImage)
                                } else {
                                    carouselModel.principalImage = carouselModel.images[currentIndex - 1]
                                }
                                scrollR?.scrollTo(carouselModel.images.firstIndex(of: carouselModel.principalImage)!, anchor: .leading)
                            } else {
                                let currentIndex = carouselModel.uiImages.firstIndex(of: carouselModel.principalUIImage)!
                                if currentIndex == 0 {
                                    carouselModel.principalUIImage = carouselModel.uiImages.last ?? UIImage()
                                } else {
                                    carouselModel.principalUIImage = carouselModel.uiImages[currentIndex - 1]
                                }
                                scrollR?.scrollTo(carouselModel.uiImages.firstIndex(of: carouselModel.principalUIImage)!, anchor: .leading)
                            }
                        }
                        .font(poppin_semibold_font(relativeTo: .title2))
                        .foregroundColor(.gray)
                    Spacer()
                    Image(systemName: "chevron.right")
                        .padding(10)
                        .onTapGesture {
                            if !carouselModel.images.isEmpty {
                                let imageCount = carouselModel.images.count
                                let currentIndex = carouselModel.images.firstIndex(of: carouselModel.principalImage)!
                                if currentIndex == imageCount - 1 {
                                    carouselModel.principalImage = carouselModel.images.first ?? ""
                                    
                                } else {
                                    carouselModel.principalImage = carouselModel.images[currentIndex + 1]
                                }
                                scrollR?.scrollTo(carouselModel.images.firstIndex(of: carouselModel.principalImage)!, anchor: .leading)
                            } else {
                                let imageCount = carouselModel.uiImages.count
                                let currentIndex = carouselModel.uiImages.firstIndex(of: carouselModel.principalUIImage)!
                                if currentIndex == imageCount - 1 {
                                    carouselModel.principalUIImage = carouselModel.uiImages.first ?? UIImage()
                                    
                                } else {
                                    carouselModel.principalUIImage = carouselModel.uiImages[currentIndex + 1]
                                }
                                scrollR?.scrollTo(carouselModel.uiImages.firstIndex(of: carouselModel.principalUIImage)!, anchor: .leading)
                            }
                        }
                        .font(poppin_semibold_font(relativeTo: .title2))
                        .foregroundColor(.gray)
                }
                ScrollViewReader { scrollReader in
                    ScrollView(.horizontal) {
                        HStack {
                            if !carouselModel.images.isEmpty {
                                ForEach(carouselModel.images, id: \.self) { imageString in
                                    KFImage(URL(string: imageString))
                                        .resizable()
                                        .scaledToFill()
                                        .frame(minWidth: width, maxWidth: width, maxHeight: size.width / 4)
                                        .background(
                                            GeometryReader { geo in
                                                DARK_GRAY_ELEKTRA
                                                    .onAppear {
                                                        DispatchQueue.main.async {
                                                            self.width = geo.size.height * 1.2
                                                        }
                                                    }
                                                    .onChange(of: geo.size) { newValue in
                                                        DispatchQueue.main.async {
                                                            self.width = newValue.height * 1.2
                                                        }
                                                    }
                                            }
                                        )
                                        .cornerRadius(10)
                                        .overlay(
                                            RoundedRectangle(cornerRadius: 10)
                                                .stroke(lineWidth: 3)
                                                .foregroundColor(DARK_GRAY_ELEKTRA)
                                        )
                                        .onTapGesture {
                                            self.carouselModel.principalImage = imageString
                                        }
                                        .id(carouselModel.images.firstIndex(of: imageString)!)
                                }
                            } else {
                                ForEach(carouselModel.uiImages, id: \.self) { uiImage in
                                    Image(uiImage: uiImage)
                                        .resizable()
                                        .scaledToFill()
                                        .frame(minWidth: width, maxWidth: width, maxHeight: size.width / 4)
                                        .background(
                                            GeometryReader { geo in
                                                DARK_GRAY_ELEKTRA
                                                    .onAppear {
                                                        DispatchQueue.main.async {
                                                            self.width = geo.size.height * 1.2
                                                        }
                                                    }
                                                    .onChange(of: geo.size) { newValue in
                                                        DispatchQueue.main.async {
                                                            self.width = newValue.height * 1.2
                                                        }
                                                    }
                                            }
                                        )
                                        .cornerRadius(10)
                                        .overlay(
                                            RoundedRectangle(cornerRadius: 10)
                                                .stroke(lineWidth: 3)
                                                .foregroundColor(DARK_GRAY_ELEKTRA)
                                        )
                                        .onTapGesture {
                                            self.carouselModel.principalUIImage = uiImage
                                        }
                                        .id(carouselModel.uiImages.firstIndex(of: uiImage)!)
                                }
                            }
                        }
                        .padding(.bottom)
                    }
                    .onAppear {
                       scrollR = scrollReader
                    }
                }
            }
            .padding()
            .frame(maxWidth: size.width * 9 / 10, maxHeight: size.height * 2 / 3, alignment: .top)
            .background(Color.white)
            .cornerRadius(10)
            .shadow(radius: 3)
            Text("x")
                .frame(width: 30, height: 30)
                .font(poppin_semibold_font(relativeTo: .title1))
                .cornerRadius(100)
                .foregroundColor(.gray)
                .offset(x: -10, y: 10)
                .onTapGesture {
                    carouselModel.carousel = false
                    CarouselModel.shared.principalImage = ""
                    CarouselModel.shared.images = []
                    CarouselModel.shared.principalUIImage = UIImage()
                    CarouselModel.shared.uiImages = []
                }
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(
            GeometryReader { geo in
                Color.black.opacity(0.2)
                    .onAppear {
                        self.size = geo.size
                        self.width = self.size.width
                    }
            }
        )
        .ignoresSafeArea()
    }
}

struct CarouselView_Previews: PreviewProvider {
    static var previews: some View {
        CarouselView(
            carouselModel: CarouselModel.shared
        )
    }
}
