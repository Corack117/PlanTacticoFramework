//
//  HorizontalSelectView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 09/05/23.
//

import SwiftUI

struct FoldView<Content: View>: View {
    @Binding var title: String
    var titleColor: Color
    var cancelClose: Bool
    var titleBold: Bool
    @Binding var changeView: Bool
    @Binding var reset: Bool
    @ViewBuilder var content: () -> Content
    @State private var isExpanded: Bool
    var hideArrow: Bool
    
    init(title: Binding<String>, titleColor: Color = .black, cancelClose: Bool = false, titleBold: Bool = false, isExpanded: Bool = false, hideArrow: Bool = false, @ViewBuilder content: @escaping () -> Content) {
        self._title = title
        self.titleColor = titleColor
        self.cancelClose = cancelClose
        self.titleBold = titleBold
        self._changeView = Binding.constant(false)
        self._reset = Binding.constant(false)
        self.content = content
        self.isExpanded = isExpanded
        self.hideArrow = hideArrow
    }
    
    init(title: Binding<String>, titleColor: Color = .black, cancelClose: Bool = false, titleBold: Bool = false, reset: Binding<Bool>, isExpanded: Bool = false, hideArrow: Bool = false, @ViewBuilder content: @escaping () -> Content) {
        self._title = title
        self.titleColor = titleColor
        self.cancelClose = cancelClose
        self.titleBold = titleBold
        self._changeView = Binding.constant(false)
        self._reset = reset
        self.content = content
        self.isExpanded = isExpanded
        self.hideArrow = hideArrow
    }
    
    init(title: Binding<String>, titleColor: Color = .black, cancelClose: Bool = false, titleBold: Bool = false, changeView: Binding<Bool>, isExpanded: Bool = false, hideArrow: Bool = false, @ViewBuilder content: @escaping () -> Content) {
        self._title = title
        self.titleColor = titleColor
        self.cancelClose = cancelClose
        self.titleBold = titleBold
        self._changeView = changeView
        self._reset = Binding.constant(false)
        self.content = content
        self.isExpanded = isExpanded
        self.hideArrow = hideArrow
    }
    
    var body: some View {
        VStack {
            HStack {
                Text(title)
                    .lineLimit(1)
                    .font(isExpanded && titleBold ? poppin_medium_font(relativeTo: .headline) : poppin_font(relativeTo: .subheadline))
                    .foregroundColor(isExpanded ? .black : titleColor)
                Spacer()
                if !hideArrow {
                    Image(systemName: "chevron.down")
                        .foregroundColor(DARK_GRAY_ELEKTRA)
                        .font(poppin_font(relativeTo: .title3))
                }
            }
            .frame(maxWidth: .infinity)
            if isExpanded {
                content()
            }
        }
        .frame(maxWidth: .infinity)
        .padding()
        .cornerRadius(5)
        .overlay(
            RoundedRectangle(cornerRadius: 5)
                .stroke()
                .foregroundColor(DARK_GRAY_ELEKTRA)
        )
        .background(Color.white.opacity(0.00001).padding(.horizontal))
        .onTapGesture {
            DispatchQueue.main.async {
                withAnimation(.easeInOut(duration: 0.2)) {
                    if !cancelClose {
                        isExpanded.toggle()
                    } else {
                        if isExpanded {
                            changeView.toggle()
                        }
                        isExpanded = true
                    }
                    if !isExpanded {
                        reset = true
                    } else {
                        reset = false
                    }
                }
            }
        }
    }
}

struct FoldView_Previews: PreviewProvider {
    static var previews: some View {
        FoldView(title: .constant("Resultados Clave")) {
            HStack(spacing: 0) {
                let image = Image("bn_agregar_nuevo", bundle: .init(identifier: BUNDLE_NAME))
                VerticalLabelView(title: "Agregar Nuevo", image: image)
                VerticalLabelView(title: "Agregar Nuevo", image: image)
                VerticalLabelView(title: "Agregar Nuevo", image: image)
                VerticalLabelView(title: "Agregar Nuevo", image: image)
            }
            .padding(0)
        }
    }
}
