//
//  CheckboxView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 10/05/23.
//

import SwiftUI

struct CheckboxView: View {
    var title: String
    @State var isActive: Bool
    var content: (Bool) -> Void = {isActive in }
    
    var body: some View {
        Toggle(isOn: $isActive) {
            Text(title)
                .foregroundColor(.black)
        }
        .toggleStyle(iOSCheckboxToggleStyle(principalColor: .black, secondaryColor: RED_ELEKTRA))
        .font(poppin_font(relativeTo: .headline).weight(.regular))
        .onChange(of: isActive) { newValue in
            content(newValue)
        }
    }
}

struct CheckboxView_Previews: PreviewProvider {
    static var previews: some View {
        CheckboxView(
            title: ManagerModel().pdvs.first?.full_name ?? "Texto prueba\nhola\nhola\nhola",
            isActive: false
        )
    }
}
