//
//  ExpandModalView.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 09/05/23.
//

import SwiftUI

struct ExpandModalView: View {
    @State var isShow: Bool
    @Namespace var namespace
    
    var body: some View {
        if !isShow {
            Button {
                withAnimation(.spring(response: 0.5, dampingFraction: 0.7, blendDuration: 1)) {
                    isShow.toggle()
                }
            } label: {
                Label {
                    Text("Agregar Nuevo")
                        .font(poppin_medium_font(relativeTo: .title3))
                } icon: {
                    Image(systemName: "plus.circle")
                        .font(poppin_font(relativeTo: .title1))
                }
                .frame(maxWidth: .infinity)
                .foregroundColor(Color.gray.opacity(0.8))
                .padding()
                .overlay(
                    RoundedRectangle(cornerRadius: 10)
                        .stroke(.gray.opacity(0.8), lineWidth: 1)
                )
            }
            .matchedGeometryEffect(id: "shape", in: namespace)
        } else {
            StartWidgetView(isShow: $isShow, namespace: namespace)
        }
    }
}

struct ExpandModalView_Previews: PreviewProvider {
    static var previews: some View {
        ExpandModalView(isShow: false)
    }
}
