//
//  Action.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 11/05/23.
//

import Foundation
import RealmSwift

struct ActionsCoder: Decodable {
    let actions: [Action]
    
    enum CodingKeys: CodingKey {
        case resultado
        
        enum ArrayKey: CodingKey {
            case acciones
        }
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let nestedContainer = try container.nestedContainer(keyedBy: CodingKeys.ArrayKey.self, forKey: .resultado)
        self.actions = try nestedContainer.decode([Action].self, forKey: .acciones)
    }
}

final class Action: Object, Identifiable, Codable, ObjectKeyIdentifiable {
    @objc dynamic var id: Int = 0
    @objc dynamic var descripcion: String = ""
    @objc dynamic var idIndicator: Int = 0
    @objc dynamic var isSelected: Bool = false
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    enum CodingKeys: CodingKey {
        case idAccion, idIndicador, descripcion, isSelected
    }
    
    override init() { }
    
    init(id: Int, description: String, idIndicator: Int, isSelected: Bool = false) {
        super.init()
        self.id = id
        self.descripcion = description
        self.isSelected = isSelected
        self.idIndicator = idIndicator
    }
    
    init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .idAccion)
        descripcion = try container.decode(String.self, forKey: .descripcion)
        idIndicator = try container.decode(Int.self, forKey: .idIndicador)
        do {
            isSelected = try container.decode(Bool.self, forKey: .isSelected)
        } catch {
            isSelected = false
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .idAccion)
        try container.encode(descripcion, forKey: .descripcion)
        try container.encode(idIndicator, forKey: .idIndicador)
        try container.encode(isSelected, forKey: .isSelected)
    }
}

struct ActionObject: Equatable {
    var id: Int
    var descripcion: String
    var idIndicator: Int
    var isSelected: Bool
}
