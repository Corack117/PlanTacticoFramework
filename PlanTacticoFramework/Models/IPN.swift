//
//  IPNModel.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 10/05/23.
//

import Foundation

struct IPN: Identifiable, Equatable, Codable {
    var id: UUID = UUID()
    var title: String
    var description: String
}
