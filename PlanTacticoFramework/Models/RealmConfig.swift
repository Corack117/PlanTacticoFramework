//
//  RealmConfig.swift
//  PlanTacticoRealm
//
//  Created by Sergio Ordaz Romero on 04/08/23.
//

import Foundation
import RealmSwift

class RealmConfig {
    var realm: Realm?
    var settings: Realm.Configuration = Realm.Configuration.defaultConfiguration
    
    static var shared = RealmConfig()
    
    func setConfig() {
        var config = Realm.Configuration(
            // Otras configuraciones de Realm...
            schemaVersion: 0,
            objectTypes: [
                Action.self,
                KeyResult.self,
                IndicatorItem.self,
                PDVsCoder.self,
                PDV.self,
                ResponsibleIndicator.self,
                TacticalPlan.self,
                FileData.self,
                CommentData.self,
                UserProfile.self,
            ] // Agrega KeyResult a la lista de clases administradas
        )
        config.fileURL!.deleteLastPathComponent()
        config.fileURL!.appendPathComponent("tacticalPlan")
        config.fileURL!.appendPathExtension("realm")
        settings = config
        realm = try! Realm(configuration: config)
        
    }
    
    init() {
        setConfig()
    }
}
