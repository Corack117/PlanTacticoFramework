//
//  FancyToastModifier.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 14/06/23.
//

import Foundation
import SwiftUI

struct FancyToastModifier: ViewModifier {
    @Binding var toast: FancyToast?
    @State private var workItem: DispatchWorkItem?
    @State private var opacity: Double = 1
    
    func body(content: Content) -> some View {
        content
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .overlay(
                ZStack(alignment: .top) {
                    mainToastView()
                }
                    .frame(maxHeight: .infinity, alignment: .top)
                    .opacity(opacity)
                    .animation(.spring(), value: toast)
            )
            .onChange(of: toast) { value in
                showToast()
            }
    }
    
    @ViewBuilder func mainToastView() -> some View {
        if let toast = toast {
            VStack {
                FancyToastView(
                    type: toast.type,
                    title: toast.title,
                    message: toast.message
                ) {
                    dismissToast()
                }
            }
            .transition(.move(edge: .top))
        }
    }
    
    private func showToast() {
        guard let toast = toast else { return }
        opacity = 1
        
        UIImpactFeedbackGenerator(style: .light).impactOccurred()
        
        if toast.duration > 0 {
            workItem?.cancel()
            
            let task = DispatchWorkItem {
               dismissToast()
            }
            
            workItem = task
            DispatchQueue.main.asyncAfter(deadline: .now() + toast.duration, execute: task)
        }
    }
    
    private func dismissToast() {
        withAnimation(.easeInOut(duration: 0.5)) {
            toast = nil
            opacity = 0
        }
        
        workItem?.cancel()
        workItem = nil
    }
}
