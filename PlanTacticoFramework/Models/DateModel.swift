//
//  DateModel.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 28/04/23.
//

import Foundation

class DateModel: ObservableObject {
    let calendar = Calendar.current
    let currentDate = Date()
    
    var weekData: WeekData? = nil
    
    static let shared = DateModel()
    
    init() {
        self.weekData = getCurrentWeekOfOperation()
    }
    
    func getCurrentWeekOfOperation(date: Date = Date()) -> WeekData {
        let today = calendar.startOfDay(for: date)
        let dayOfWeek = calendar.component(.weekday, from: today)
        let monday = calendar.date(byAdding: .day, value: 2 - dayOfWeek, to: today)
        let sunday = calendar.date(byAdding: .day, value: 8 - dayOfWeek, to: today)
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMM"
        let monday_string = formatter.string(from: monday!)
        let sunday_string = formatter.string(from: sunday!)
        let week = "Semana \(self.getCurrentWeekNumber(date: today))"
        let weekOfOperation = "(\(monday_string) - \(sunday_string))"
        formatter.dateFormat = "dd/MM/yyyy"
        let weekData = WeekData(
            week: week, 
            rangeWeek: weekOfOperation,
            startWeek: formatter.string(from: monday!),
            endWeek: formatter.string(from: sunday!),
            date: date
        )
        return weekData
    }
    
    func getCurrentWeekNumber(date: Date = Date()) -> Int {
        let currentWeek = calendar.component(.weekOfYear, from: date)
        return currentWeek
    }
    
    func getFiveWeeks() -> [WeekData] {
        var weeks: [WeekData] = []
        let today = calendar.startOfDay(for: Date())
        for i in -12...0 {
            let date = calendar.date(byAdding: .weekOfYear, value: i, to: today)
            let weekData = self.getCurrentWeekOfOperation(date: date!)
            weeks.insert(weekData, at: 0)
        }
        return weeks
    }
    
    struct WeekData: Identifiable, Equatable {
        let id: UUID = UUID()
        var week: String = ""
        var rangeWeek: String = ""
        var startWeek: String = ""
        var endWeek: String = ""
        var date: Date
        
        static func == (lwd: WeekData, rwd: WeekData) -> Bool {
            return lwd.week == rwd.week
            
        }
    }
}
