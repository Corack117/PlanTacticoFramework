//
//  PDVs.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 10/05/23.
//

import Foundation
import RealmSwift

final class PDVsCoder: Object, Identifiable, Codable, ObjectKeyIdentifiable {
    @objc dynamic var regionName: String = "Región"
    var pdvs: List<PDV> = List<PDV>()
    @objc dynamic var cecoPadre: String = ""
    
    
    override static func primaryKey() -> String? {
        return "regionName"
    }
    
    private enum ResultsCodingKey: String, CodingKey {
        case resultado
        
        enum ArrayKey: String, CodingKey {
            case region, puntos_contacto, cecoPadre
        }
    }
    
    override init() { }
    
    init(from decoder:Decoder) throws {
        let container =  try decoder.container(keyedBy: ResultsCodingKey.self)
        let resultContainer = try container.nestedContainer(keyedBy: ResultsCodingKey.ArrayKey.self, forKey: .resultado)
        regionName = try resultContainer.decode(String.self, forKey: .region).capitalized.replacingOccurrences(of: "Formatos Propios ", with: "")
        cecoPadre = try resultContainer.decode(String.self, forKey: .cecoPadre)
        pdvs = try resultContainer.decode(List<PDV>.self, forKey: .puntos_contacto)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: ResultsCodingKey.self)
        var resultContainer = container.nestedContainer(keyedBy: ResultsCodingKey.ArrayKey.self, forKey: .resultado)
        try resultContainer.encode(regionName, forKey: .region)
        try resultContainer.encode(pdvs, forKey: .puntos_contacto)
    }
}

final class PDV: Object, Identifiable, Codable {
    @objc dynamic var ceco: String = ""
    @objc dynamic var name: String = ""
    var isSelected: Bool
//    @objc dynamic var key_results: [ManagerModel]
    
    var full_name: String {
        return ceco + " " + name
    }
    
    override static func primaryKey() -> String? {
        return "ceco"
    }
    
    private enum PDVCodingKey: String, CodingKey {
        case ceco, descripcion_ceco
    }
    
    override init() {
        self.isSelected = false
    }
    
    convenience init(ceco: String, name: String, isSelected: Bool = false) {
        self.init()
        self.ceco = ceco
        self.name = name
        self.isSelected = isSelected
//        self.key_results = []
    }
    
    convenience init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: PDVCodingKey.self)
        self.ceco = try container.decode(String.self, forKey: .ceco)
        self.name = try container.decode(String.self, forKey: .descripcion_ceco).capitalized
        self.isSelected = false
//        self.key_results = []
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: PDVCodingKey.self)
        try container.encode(ceco, forKey: .ceco)
        try container.encode(name, forKey: .descripcion_ceco)
    }
}

struct PDVObject: Equatable {
    var ceco: String
    var name: String
    var isSelected: Bool
    
    var full_name: String = ""
    
    init(ceco: String, name: String, isSelected: Bool) {
        self.ceco = ceco
        self.name = name
        self.isSelected = isSelected
        self.full_name = ceco + " " + name
    }
}
