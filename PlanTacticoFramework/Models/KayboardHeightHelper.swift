//
//  KayboardHeightHelper.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 11/05/23.
//

import Foundation
import UIKit
import SwiftUI

class KeyboardHeightHelper: ObservableObject {
    @Published var keyboardHeight: CGFloat = 0
    
    init() {
        self.listenForKeyboardNotifications()
    }
    
    private func listenForKeyboardNotifications() {
        NotificationCenter.default.addObserver(
            forName: UIResponder.keyboardDidShowNotification,
            object: nil,queue: .main) { notification in
                guard let userInfo = notification.userInfo, let keyboardRect = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect else { return }
                DispatchQueue.main.async {
                    withAnimation(.easeInOut(duration: 0.1)) {
                        self.keyboardHeight = keyboardRect.height
                    }
                }
        }
        
        NotificationCenter.default.addObserver(
            forName: UIResponder.keyboardDidHideNotification,
            object: nil,
            queue: .main) { notification in
                DispatchQueue.main.async {
                    withAnimation(.easeInOut(duration: 0.1)) {
                        self.keyboardHeight = 0   
                    }
                }
        }
    }
}
