//
//  UserProfile.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 07/06/23.
//

import Foundation
import RealmSwift

struct UserProfileCoder: Decodable {
    let userProfile: UserProfile
    
    enum CodingKeys: CodingKey {
        case resultado
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        userProfile = try container.decode(UserProfile.self, forKey: .resultado)
    }
}

final class UserProfile: Object, Identifiable, Decodable {
    @objc dynamic var id: Int = 0
    @objc dynamic var idPerfil: Int = 0
    @objc dynamic var nombre: String = ""
    @objc dynamic var esRegional: Bool = false
    @objc dynamic var esGerente: Bool = false
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    enum CodingKeys: CodingKey {
        case idPerfil, nombre, esRegional, esGerente
    }
    
    override init() { }
    
    init(id: Int = 1, idPerfil: Int, nombre: String, esRegional: Bool, esGerente: Bool) {
        super.init()
        self.id = id
        self.idPerfil = idPerfil
        self.nombre = nombre
        self.esRegional = esRegional
        self.esGerente = esGerente
    }
    
    init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = 1
        self.idPerfil = try container.decode(Int.self, forKey: .idPerfil)
        self.nombre = try container.decodeIfPresent(String.self, forKey: .nombre) ?? ""
        self.esRegional = try container.decode(Bool.self, forKey: .esRegional)
        self.esGerente = try container.decode(Bool.self, forKey: .esGerente)
    }
}
