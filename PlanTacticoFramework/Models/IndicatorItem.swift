//
//  IndicadorItem.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 03/05/23.
//

import Foundation
import RealmSwift

struct IndicatorsCoder: Decodable {
    let indicators: [IndicatorItem]
    
    enum CodingKeys: CodingKey {
        case resultado
        
        enum ArrayKeys: CodingKey {
            case indicadores
        }
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let nestedContainer = try container.nestedContainer(keyedBy: CodingKeys.ArrayKeys.self, forKey: .resultado)
        indicators = try nestedContainer.decode([IndicatorItem].self, forKey: .indicadores)
    }
}

final class IndicatorItem: Object, Identifiable, Codable, ObjectKeyIdentifiable {
    @objc dynamic var id: Int = 0
    @objc dynamic var idResultadoClave: Int = 0
    @objc dynamic var idIndicador: Int = 0
    @objc dynamic var descripcion: String = ""
    @objc dynamic var objetivo: String = ""
    @objc dynamic var ejemplo: String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    enum CodingKeys: CodingKey {
        case idIndicador, idResultadoClave, descripcion, objetivo, ejemplo
    }
    
    override init() { }
    
    init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .idIndicador)
        self.idResultadoClave = try container.decode(Int.self, forKey: .idResultadoClave)
        self.idIndicador = try container.decode(Int.self, forKey: .idIndicador)
        self.descripcion = try container.decode(String.self, forKey: .descripcion)
        self.objetivo = try container.decode(String.self, forKey: .objetivo)
        self.ejemplo = try container.decodeIfPresent(String.self, forKey: .ejemplo) ?? "Coloca tu resultado clave un una forma de %\nEjemplo: Cumplimiento de Tareas Zeus de un\n80% a un 90% en su ejecución"
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .idIndicador)
        try container.encode(idResultadoClave, forKey: .idResultadoClave)
        try container.encode(descripcion, forKey: .descripcion)
        try container.encode(objetivo, forKey: .objetivo)
    }
    
    static func == (li: IndicatorItem, ri: IndicatorItem) -> Bool {
        return li.id == ri.id
    }
}
