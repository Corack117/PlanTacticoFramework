//
//  PlanTacticoModel.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 10/05/23.
//

import SwiftUI
import RealmSwift


class ManagerModel: ObservableObject {
    @ObservedResults(KeyResult.self, configuration: RealmConfig.shared.settings)var key_results
    @ObservedResults(PDVsCoder.self, configuration: RealmConfig.shared.settings)var pdvModel
    @ObservedResults(IndicatorItem.self, configuration: RealmConfig.shared.settings)var indicators
    @ObservedResults(ResponsibleIndicator.self, configuration: RealmConfig.shared.settings)var responsibles
    @ObservedResults(Action.self, configuration: RealmConfig.shared.settings)var actions
    @ObservedResults(TacticalPlan.self, configuration: RealmConfig.shared.settings)var tacticalPlans
    @Published var indicatorsFiltered: [IndicatorItem] = []
    @Published var result: String = ""
    @Published var actionsFiltered: [Action] = []
    @Published var recommendations: String = ""
    
    @Published var key_result: KeyResult? {
        didSet {
            indicatorsFiltered = indicators.filter {
                $0.idResultadoClave == key_result?.id
            }
        }
    }
    @Published var indicatorSelected: IndicatorItem?
    
    @Published var step: PlanSteps = .indicator
    @Published var scrollValue: ScrollViewProxy?
    @Published var isLoaded: [Bool] = []
    
    @Published var pdvs: [PDV] = []
    @Published var pdvsObject: [PDVObject] = []
    @Published var responsiblesObject: [ResponsibleIndicatorObject] = []
    @Published var pdvSelectedToCreate: String?
    @Published var onlyView: Bool = false
    @Published var isManager: Bool = true
    @Published var showLoader: Bool = false
    @Published var pdvName: String = ""
    @Published var idResponsable: Int = 0
    
    private var privateRegionName: String = "Región"
    var currentCECO: String? = ""
    var currentCECOName: String = ""
    var currentEmployee: String = ""
    var regionName: String {
        return privateRegionName
    }
    
    static var shared: ManagerModel = {
        let instance = ManagerModel(fetchData: true)
        return instance
    }()
    
    
    init(fetchData: Bool = false) {
//        print(Realm.Configuration.defaultConfiguration.fileURL!)
        self.initializePersistence()
//        if fetchData {
//            self.fetchData()
//        }
    }
    
    func initializePersistence() -> Void {
        withAnimation(.easeInOut(duration: 0.5)) {
            if let current_pdvModel = pdvModel.first {
                privateRegionName = current_pdvModel.regionName
                pdvs = Array(current_pdvModel.pdvs)
                pdvs.sort { pdv1, pdv2 in
                    let status = TacticalPlan.Status.finished
                    let fisrtElement = self.tacticalPlans.filter("ANY pdvs.ceco CONTAINS %@ AND status == %@", pdv1.ceco, status.rawValue).count
                    let secondElement = self.tacticalPlans.filter("ANY pdvs.ceco CONTAINS %@ AND status == %@", pdv2.ceco, status.rawValue).count
                    return fisrtElement > secondElement
                }
//                pdvs.sort { $0.name < $1.name }
                var temppdvsObject: [PDVObject] = []
                pdvs.forEach { pdv in
                    temppdvsObject.append(
                        PDVObject(
                            ceco: pdv.ceco,
                            name: pdv.name,
                            isSelected: pdv.isSelected
                        )
                    )
                }
                pdvsObject = temppdvsObject
            }
        }
        initializeResponsibles()
    }
    
    func fetchData(weekData: DateModel.WeekData) -> Void {
        self.isLoaded = []
        let api = APIRequest()
        let realm = RealmConfig.shared.realm!
//        Fetch Perfil de Usuario
        api.fetchUserProfile(employee: currentEmployee) { userProfile in
            self.isManager = userProfile.esRegional
            try! realm.write {
                realm.add(userProfile, update: .all)
            }
            
            if !userProfile.esRegional {
                api.fetchPlansByGerente(employee: self.currentEmployee, weekData: weekData) { tacticalPlanCoder in
                    self.privateRegionName = tacticalPlanCoder.descripcion.capitalized.replacingOccurrences(of: "Formatos Propios", with: "")
                    self.currentCECOName = tacticalPlanCoder.desc
                    self.idResponsable = tacticalPlanCoder.idResponsable
                    try! realm.write {
                        
                        var plans: [TacticalPlan] = []
                        
                        for plan in self.tacticalPlans {
                            if !tacticalPlanCoder.tacticPlansData.contains(where: { String($0.ceco_id) == plan.ceco }) {
                                plans.append(plan.thaw()!)
                            }
                        }
                        
                        realm.delete(plans)
                        for data in tacticalPlanCoder.tacticPlansData {
                            let oldPlan = self.tacticalPlans.first(where: { $0.ceco == String(data.ceco_id)})
                            let plan = TacticalPlan(planData: data, cecoData: tacticalPlanCoder.ceco)
                            if let oldImages = oldPlan?.files {
                                for image in oldImages {
                                    plan.files.append(image)
                                }
                            }
                            if let oldComentaries = oldPlan?.comentaries {
                                for comment in oldComentaries {
                                    plan.comentaries.append(comment)
                                }
                            }
                            realm.create(TacticalPlan.self, value: plan, update: .modified)
                        }
                    }
                    self.isLoaded.append(true)
//                    try! realm.write {
//                        for plan in tacticalPlanCoder.tacticPlans {
//                            realm.create(TacticalPlan.self, value: plan, update: .modified)
//                        }
//                    }
                }
            }
            
//            Fetch Resultados Clave
            api.fetchKeyResults { key_results in
                key_results.forEach { item in
                    try! realm.write {
                        realm.add(item, update: .modified)
                    }
                }
                self.isLoaded.append(true)
            }
            
//            Fetch Puntos de Venta
            api.fetchPDVs(employee: self.currentEmployee) { pdvCoder in
                try! realm.write {
                    realm.add(pdvCoder, update: .modified)
                }
                withAnimation(.easeInOut(duration: 0.5)) {
                    self.privateRegionName = pdvCoder.regionName
                    self.pdvs = Array(pdvCoder.pdvs)
                    if !self.tacticalPlans.isEmpty {
                        self.pdvs.sort { pdv1, pdv2 in
                            let status = TacticalPlan.Status.finished
                            let fisrtElement = self.tacticalPlans.filter("ANY pdvs.ceco CONTAINS %@ AND status == %@", pdv1.ceco, status.rawValue).count
                            let secondElement = self.tacticalPlans.filter("ANY pdvs.ceco CONTAINS %@ AND status == %@", pdv2.ceco, status.rawValue).count
                            return fisrtElement > secondElement
                        }
                    }
//                    self.pdvs.sort { $0.name < $1.name }
                    var temppdvsObject: [PDVObject] = []
                    self.pdvs.forEach { pdv in
                        temppdvsObject.append(
                            PDVObject(
                                ceco: pdv.ceco,
                                name: pdv.name,
                                isSelected: pdv.isSelected
                            )
                        )
                    }
                    self.pdvsObject = temppdvsObject
                }
                
                api.fetchPlansByRegion(cecoPadre: pdvCoder.cecoPadre, weekData: weekData) { regionalPlanCoder in
                    try! realm.write {
                        let allPlans = realm.objects(TacticalPlan.self).filter("NOT ceco CONTAINS %@", "general")
                        realm.delete(allPlans)
                        for ceco in regionalPlanCoder.cecos {
                            for data in ceco.tacticPlansData {
                                let plan = TacticalPlan(planData: data, cecoData: ceco.ceco)
                                realm.create(TacticalPlan.self, value: plan, update: .modified)
                            }
                        }
                        
                        if !self.pdvs.isEmpty {
                            self.pdvs.sort { pdv1, pdv2 in
                                let status = TacticalPlan.Status.finished
                                let fisrtElement = self.tacticalPlans.filter("ANY pdvs.ceco CONTAINS %@ AND status == %@", pdv1.ceco, status.rawValue).count
                                let secondElement = self.tacticalPlans.filter("ANY pdvs.ceco CONTAINS %@ AND status == %@", pdv2.ceco, status.rawValue).count
                                return fisrtElement > secondElement
                            }
        //                    self.pdvs.sort { $0.name < $1.name }
                        }
                        self.isLoaded.append(true)
                    }
                }
            }
            
//            Fetch Indicadores
            api.fetchIndicators { indicators in
                indicators.forEach { item in
                    try! realm.write {
                        realm.add(item, update: .all)
                    }
                }
                self.isLoaded.append(true)
            }
            
//            Fetch Responsables
            api.fetchResponsibles { responsibles in
                responsibles.forEach { item in
                    try! realm.write {
                        realm.add(item, update: .all)
                    }
                }
                self.initializeResponsibles(data: responsibles)
                self.isLoaded.append(true)
            }
            
//            Fetch Acciones
            api.fetchActions { actions in
                actions.forEach { item in
                    try! realm.write {
                        realm.add(item, update: .all)
                    }
                }
                self.isLoaded.append(true)
            }
        } errorRequest: {
            AlertModel.shared.action = {
                withAnimation(.easeInOut) {
                    AlertModel.shared.hasAlert = false
                }
                self.fetchData(weekData: weekData)
            }
            AlertModel.shared.secondAction = {
                AlertModel.shared.dismiss()
            }
            DispatchQueue.main.async {
                AlertModel.shared.hasAlert = true
                AlertModel.shared.type = .retryConection
            }
        }
    }
    
    func initializeResponsibles(data: [ResponsibleIndicator] = []) {
        self.responsiblesObject = []
        if data.isEmpty {
            var tempresponsiblesObject: [ResponsibleIndicatorObject] = []
            responsibles.forEach { responsible in
                tempresponsiblesObject.append(
                    ResponsibleIndicatorObject(
                        id: responsible.id,
                        idResponsable: responsible.idResponsable,
                        idIndicador: responsible.idIndicador,
                        responsable: responsible.responsable,
                        isSelected: responsible.isSelected
                    )
                )
            }
            self.responsiblesObject = tempresponsiblesObject
        } else {
            var tempresponsiblesObject: [ResponsibleIndicatorObject] = []
            data.forEach { responsible in
                tempresponsiblesObject.append(
                    ResponsibleIndicatorObject(
                        id: responsible.id,
                        idResponsable: responsible.idResponsable,
                        idIndicador: responsible.idIndicador,
                        responsable: responsible.responsable,
                        isSelected: responsible.isSelected
                    )
                )
            }
            self.responsiblesObject = tempresponsiblesObject
        }
    }
    
    func reset() {
        DispatchQueue.main.async { [self] in
            indicatorsFiltered = []
            result = String()
            recommendations = String()
            key_result = nil //actionsFiltered borrar
            step = .indicator
            pdvsObject.indices.forEach { index in pdvsObject[index].isSelected = false }
            responsiblesObject.indices.forEach { index in responsiblesObject[index].isSelected = false }
        }
//        print(indicatorSelected)
    }
    
    func save() {
//        let encoder = JSONEncoder()
//        let copyClass = ManagerModel()
//        copyClass.key_result = key_result
//        copyClass.indicatorSelected = indicatorSelected
//        copyClass.pdvs = pdvs.filter { $0.isSelected == true }
//        copyClass.responsibles = responsibles.filter { $0.isSelected == true }
//        copyClass.result = result
//        copyClass.actionsFiltered = actionsFiltered.filter { $0.isSelected == true }
//        copyClass.recommendations = recommendations
//        copyClass.step = step
//        var objects = getAllObjects(key: "user_objects", type: [ManagerModel].self)
//        objects.append(self)
//        if let encoded = try? encoder.encode(objects){
//            UserDefaults.standard.set(encoded, forKey: "user_objects")
//        }
     }
}
