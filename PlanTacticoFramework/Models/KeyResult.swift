//
//  KeyResult.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 17/05/23.
//

import Foundation
import SwiftUI
import RealmSwift

struct KeyResultsCoder: Decodable {
    let keyResults: [KeyResult]
    
    enum CodingKeys: CodingKey {
        case resultado
        
        enum ArrayKeys: CodingKey {
            case resultadosClave
        }
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let nestedContainer = try container.nestedContainer(keyedBy: CodingKeys.ArrayKeys.self, forKey: .resultado)
        self.keyResults = try nestedContainer.decode([KeyResult].self, forKey: .resultadosClave)
    }
}

final class KeyResult: Object, Identifiable, Codable, ObjectKeyIdentifiable {
    @objc dynamic var id: Int = 0
    @objc dynamic var descripcion: String = ""
    @objc dynamic var type: KeyResultsEnum = .experiencia
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    enum CodingKeys: CodingKey {
        case idResultado
        case descripcion
    }
    
    override init() { }
    
    init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Int.self, forKey: .idResultado)
        self.descripcion = try container.decode(String.self, forKey: .descripcion)
        self.type = try container.decode(KeyResultsEnum.self, forKey: .descripcion)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .idResultado)
        try container.encode(descripcion, forKey: .descripcion)
    }
}

@objc enum KeyResultsEnum: Int, CaseIterable, Codable, RealmEnum {
    case experiencia = 0
    case negocio = 1
    case seguimiento = 2
    case unknown = 3
    case meta = 4
    
    var imageName: String {
        switch self {
            case .experiencia: return "bn_experiencia"
            case .negocio: return "bn_negocio"
            case .seguimiento: return "bn_seguimiento"
            case .meta: return "bn_meta"
            case .unknown: return "questionmark.app"
        }
    }
    
    var color: Color {
        switch self {
            case .experiencia: return EXPERIENCIA_COLOR
            case .negocio: return NEGOCIO_COLOR
            case .seguimiento: return SEGUIMIENTO_COLOR
            case .meta: return META_COLOR
            case .unknown: return UNKNOWN_COLOR
        }
    }
    
    var image: Image {
        switch self {
            case .unknown: return Image(systemName: imageName)
            default: return Image(imageName, bundle: .init(identifier: BUNDLE_NAME))
        }
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        let status = try? container.decode(String.self)
        switch status {
            case "Experiencia": self = .experiencia
            case "Negocio": self = .negocio
            case "Seguimiento": self = .seguimiento
            case "Meta": self = .meta
            default:
                self = .meta
        }
    }
}


