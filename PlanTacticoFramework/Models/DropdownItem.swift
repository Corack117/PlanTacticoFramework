//
//  DropdowItem.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 03/05/23.
//

import Foundation
import SwiftUI

struct DropdownItem: Identifiable, Equatable {
    var id: UUID = UUID()
    var title: String
    var subtitle: String = ""
    var image: Image = Image("")
    var weekData: DateModel.WeekData?
    
    static func == (ldi: DropdownItem, rdi: DropdownItem) -> Bool {
        return ldi.title == rdi.title
    }
}
