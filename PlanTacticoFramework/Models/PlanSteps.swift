//
//  PlanSteps.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 11/05/23.
//

import Foundation
import RealmSwift

@objc enum PlanSteps: Int, CaseIterable, RealmEnum {
    case indicator = 0
    case pdvs = 1
    case responsible = 2
    case result = 3
    case actions = 4
    case recommendations = 5
    case finish = 6
    
    var renderSteps: [Self] {
        switch self {
            case .indicator: return [.indicator]
            case .pdvs: return [.indicator, .pdvs]
            case .responsible: return [.indicator, .pdvs, .responsible]
            case .result: return [.indicator, .pdvs, .responsible, .result]
            case .actions: return [.indicator, .pdvs, .responsible, .result, .actions]
            case .recommendations: return [.indicator, .pdvs, .responsible, .result, .actions, .recommendations]
            case .finish: return [.indicator, .pdvs, .responsible, .result, .actions, .recommendations, .finish]
        }
    }
}
