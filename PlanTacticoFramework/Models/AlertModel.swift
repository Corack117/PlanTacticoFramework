//
//  AlertsModel.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 23/06/23.
//

import Foundation

enum AlertEnum {
    case empty, succesTacticPlan, errorConection, failureConection, failureEvidence, retryConection, sendMail, withoutEvidence, cameraModal, completePlan
    
    var title: String {
        switch self {
            case .succesTacticPlan:
                return "Tu Plan Táctico ha sido asignado éxito.\n ¿Quieres asignar un nuevo plan a la misma sucursal?"
            case .failureConection, .retryConection:
                return "Error de conexión"
            case .failureEvidence:
                return "No ha sido posible cargar tu evidencia, intenta de nuevo."
            case .sendMail:
                return "Se reenvió el correo."
            case .withoutEvidence:
                return "Las evidencias aún no han sido cargadas"
            case .completePlan:
                return "¿Estás seguro que deseas finalizar tu plan táctico?"
            default:
                return ""
        }
    }
    
    var titleButton: String {
        switch self {
            case .succesTacticPlan:
                return "Ver mis Planes"
            case .failureConection:
                return "Volver"
            case .retryConection:
                return "Reintentar"
            case .completePlan:
                return "Cancelar"
            default:
                return "Volver"
        }
    }
    
    var subtitleButton: String {
        switch self {
            case .retryConection:
                return "Volver"
            case .succesTacticPlan:
                return "Crear Nuevo"
            case .completePlan:
                return "Aceptar"
            default:
                return ""
        }
    }
    
    var isSecondButton: Bool {
        switch self {
            case .retryConection:
                return true
            case .succesTacticPlan:
                return true
            case .completePlan:
                return true
            default:
                return false
        }
    }
}

class AlertModel: ObservableObject {
    @Published var type: AlertEnum = .succesTacticPlan
    @Published var hasAlert: Bool = false
    @Published var showExtensionModal: Bool = false
    @Published var camera: Bool = false
    @Published var isShowPhotoLibrary = false
    
    
    var action: () -> Void = {}
    var secondAction: () -> Void = {}
    var dismiss: () -> Void = {}
    
    static var shared = AlertModel()
}
