//
//  APIRequest.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 16/05/23.
//

import Foundation
import RealmSwift
import SwiftUI

class APIRequest {
    let HOST_NAME: String = "https://franquiciags.com.mx/checklist/central/encriptados?consumoservicio=" //Prod
    let API_PATH: String = "/plataforma-franquicia-core/api-local/plan-tactico/"
    let HOST_IMAGE: String = "https://franquiciags.com.mx/checklist"
//    let HOST_NAME: String = "http://10.51.158.195:8080" //Dev
//    let API_PATH: String = "/plataforma-franquicia-core/api-local/plan-tactico/"
//    let HOST_IMAGE: String = "https://www.productividadru.com/plan-tactico/image/?url=/checklist"
    var rootUrl: String {
        return HOST_NAME + API_PATH
    }
    
    func fetchUserProfile(employee: String, _ content: @escaping (_ userProfile: UserProfile) -> Void, errorRequest: @escaping () -> Void = {}) {
        let urlString = rootUrl + "usuario-perfil/v1/plan-perfilado/\(employee)"
        fetch(urlString: urlString, type: UserProfileCoder.self) { model in
            content(model.userProfile)
        } errorRequest: {
            errorRequest()
        }
    }
    
    func fetchPlansByRegion(cecoPadre: String, weekData: DateModel.WeekData, _ content: @escaping (_ regionalPlanCoder: RegionalPlanCoder) -> Void, errorData: @escaping () -> Void = {}, errorRequest: @escaping () -> Void = {}) {
        let urlString = rootUrl + "recursos/v1/historicoPlan/\(cecoPadre)"
        let body: [String: AnyHashable] = [
            "fechaInicio": weekData.startWeek,
            "fechaFin": weekData.endWeek
        ]
        
        postWithCoder(urlString: urlString, body: body, coder: RegionalPlanCoder.self) { model in
            content(model)
        } errorRequest: {
            errorRequest()
        }
    }
    
    func fetchPlansByGerente(employee: String, weekData: DateModel.WeekData, _ content: @escaping (_ tacticalPlanCoder: TacticalPlanCoder) -> Void, errorData: @escaping () -> Void = {}, errorRequest: @escaping () -> Void = {}) {
        let urlString = rootUrl + "ejecucion-plan/v1/planes-historico/\(employee)"
        let body: [String: AnyHashable] = [
            "fechaInicio": weekData.startWeek,
            "fechaFin": weekData.endWeek
        ]
        
        postWithCoder(urlString: urlString, body: body, coder: TacticalPlanCoder.self) { model in
            content(model)
        } errorData: {
            errorData()
        }
    }
    
    func fetchImagesPlan(idPlan: String,  _ content: @escaping (_ fileCoder: ImagePlanCoder) throws -> Void, errorData: @escaping () -> Void = {}) {
        let urlString = rootUrl + "plan-tactico/v1/" + idPlan
        fetch(urlString: urlString, type: ImagePlanCoder.self) { model in
            try! content(model)
        } errorData: {
            errorData()
        }
    }
    
    func fetchKeyResults(_ content: @escaping (_ key_results: [KeyResult]) -> Void) {
        let urlString = rootUrl + "resultado-clave/v1/"
        fetch(urlString: urlString, type: KeyResultsCoder.self) { model in
            content(model.keyResults)
        }
    }

    func fetchPDVs(employee: String, _ content: @escaping (_ pdvCoder: PDVsCoder) -> Void) {
        let urlString = rootUrl + "region-perfil/v1/\(employee)"
        fetch(urlString: urlString, type: PDVsCoder.self) { model in
            content(model)
        }
    }
    
    func fetchIndicators(_ content: @escaping (_ indicators: [IndicatorItem]) -> Void) {
        let urlString = rootUrl + "indicadores/v1/"
        fetch(urlString: urlString, type: IndicatorsCoder.self) { model in
            content(model.indicators)
        }
    }

    func fetchResponsibles(_ content: @escaping (_ responsibles: [ResponsibleIndicator]) -> Void) {
        let urlString = rootUrl + "indicador-responsable/v1/nombre-responsable/"
        fetch(urlString: urlString, type: ResponsibleIndicatorCoder.self) { model in
            content(model.responsibles)
        }
    }

    func fetchActions(_ content: @escaping (_ actions: [Action]) -> Void) {
        let urlString = rootUrl + "acciones/v1/"
        fetch(urlString: urlString, type: ActionsCoder.self) { model in
            content(model.actions)
        }
    }
    
    @MainActor func postPlan(tacticPlan: TacticalPlan, content: @escaping () -> Void, errorData: @escaping () -> Void = {}, errorRequest: @escaping () -> Void) {
        let urlString = rootUrl + "plan-tactico/v1/"
        
        var body: [String: AnyHashable] = [
            "idUsuario": ManagerModel.shared.currentEmployee,
            "idIndicador": tacticPlan.indicatorSelected?.id,
            "idProgreso": 1, // El 1 es de recién creado
            "resultadoClv": tacticPlan.result,
            "recomendaciones": tacticPlan.recommendations,
        ]
        var cecos: [[String: AnyHashable]]  = []
        var responsables: [[String: AnyHashable]]  = []
        var acciones: [[String: AnyHashable]]  = []
        for pdv in tacticPlan.pdvs {
            cecos.append(["idCeco": pdv.ceco])
        }
        for responsable in tacticPlan.responsibles {
            responsables.append(["idResponsable": responsable.idResponsable])
        }
        
        for action in tacticPlan.actions {
            acciones.append(["idAccion": action.id])
        }
                    
        body["Cecos"] = cecos
        body["responsables"] = responsables
        body["acciones"] = acciones
        post(urlString: urlString, body: body) {
            content()
        } errorRequest: {
            errorRequest()
        }
    }
    
    @MainActor func postNotificactionSend(idPlan: Int, content: @escaping () -> Void, errorData: @escaping () -> Void = {}, errorRequest: @escaping () -> Void) {
        let urlString = rootUrl + "reenvio-notificacion/plan/\(idPlan)"
        
        post(urlString: urlString, body: [:]) {
            content()
        } errorRequest: {
            errorRequest()
        }
    }
    
    @MainActor func postImages(idPlan: Int, images: [String], comentaries: String, _ content: @escaping () -> Void, errorData: @escaping () -> Void = {}, errorRequest: @escaping () -> Void) {
        let urlString = rootUrl + "ejecucion-plan/v1/"
        
        var body: [String: AnyHashable] = [
            "idPlanTactico": idPlan,
            "idResponsable": ManagerModel.shared.idResponsable,
            "comentarios": comentaries
        ]
        var archivos: [[String: AnyHashable]]  = []
        
        for image in images {
            archivos.append([
                "tipoArchivo": "jpeg",
                "nombre": UUID().uuidString + ".jpeg",
                "archivo": image
            ])
        }
        
        body["archivos"] = archivos
        
        post(urlString: urlString, body: body) {
            content()
        } errorData: {
            errorData()
        } errorRequest: {
            errorRequest()
        }
    }
    
    @MainActor func putProgress(idPlan: Int, progress_id: Int, _ content: @escaping () -> Void) {
        let urlString = rootUrl + "plan-tactico/v1/progreso/\(idPlan)"
        
        let body: [String: AnyHashable] = [
            "idProgreso": progress_id
        ]
        
        put(urlString: urlString, body: body) {
            content()
        }
    }
    
    func deletePlan(idPlan: Int, _ content: @escaping () -> Void) {
        let urlString = rootUrl + "plan-tactico/v1/\(idPlan)"
        
        delete(urlString: urlString) {
            content()
        }
    }
    
    
    func post(urlString: String, body: [String: AnyHashable], content: @escaping () -> Void = {}, errorData: @escaping () -> Void = {}, errorRequest: @escaping () -> Void) {
        if let url = URL(string: urlString) {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            request.httpBody = try? JSONSerialization.data(withJSONObject: body, options: .fragmentsAllowed)
            
            let task = URLSession.shared.dataTask(with: request) { data, _, error in
                guard let data = data, error == nil else {
                    errorRequest()
                    return
                }
                
                do {
                    _ = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    content()
                } catch {
                    errorData()
                    print(error.localizedDescription, urlString)
                }
            }
            task.resume()
        }
    }
    
    func postWithCoder<T: Decodable>(urlString: String, body: [String: AnyHashable], coder: T.Type, content: @escaping (_ model: T) -> Void, errorData: @escaping () -> Void = {}, errorRequest: @escaping () -> Void = {}) {
        if let url = URL(string: urlString) {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            request.httpBody = try? JSONSerialization.data(withJSONObject: body, options: .fragmentsAllowed)
            
            let task = URLSession.shared.dataTask(with: request) { data, _, error in
                guard let data = data, error == nil else {
                    errorRequest()
                    return
                }
                
                do {
                    let decoder = JSONDecoder()
                    let results = try decoder.decode(coder, from: data)
                    DispatchQueue.main.async {
                        content(results)
                    }
                } catch {
                    errorData()
                    print(error.localizedDescription, urlString)
                }
            }
            task.resume()
        }
    }
    
    func put(urlString: String, body: [String: AnyHashable], _ content: @escaping () -> Void = {}, errorData: @escaping () -> Void = {}, errorRequest: @escaping () -> Void = {}) {
        if let url = URL(string: urlString) {
            var request = URLRequest(url: url)
            request.httpMethod = "PUT"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            request.httpBody = try? JSONSerialization.data(withJSONObject: body, options: .fragmentsAllowed)
            
            let task = URLSession.shared.dataTask(with: request) { data, _, error in
                guard let data = data, error == nil else {
                    return
                }
                
                do {
                    _ = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    content()
                } catch {
                    errorData()
                    print(error.localizedDescription, urlString)
                }
            }
            task.resume()
        }
    }
    
    func delete(urlString: String, _ content: @escaping () -> Void = {}, errorData: @escaping () -> Void = {}, errorRequest: @escaping () -> Void = {}) {
        if let url = URL(string: urlString) {
            var request = URLRequest(url: url)
            request.httpMethod = "DELETE"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            
            let task = URLSession.shared.dataTask(with: request) { data, _, error in
                guard let data = data, error == nil else {
                    return
                }
                
                do {
                    _ = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                    content()
                } catch {
                    errorData()
                    print(error.localizedDescription, urlString)
                }
            }
            task.resume()
        }
    }
    
    
    func fetch<T: Decodable>(urlString: String, type: T.Type, _ content: @escaping (_ model: T) -> Void, errorData: @escaping () -> Void = {}, errorRequest: @escaping () -> Void = {}) {
        if let url = URL(string: urlString) {
            let configuration = URLSessionConfiguration.default
            configuration.timeoutIntervalForRequest = 60 // Segundos de espera
            let session = URLSession(configuration: configuration)
            let task = session.dataTask(with: url) { data, urlResponse, error in
                if let e = error {
                    errorRequest()
                    print(e.localizedDescription)
                    return
                }
                let decoder = JSONDecoder()
                if let safeData = data {
                    do {
                        let results = try decoder.decode(type, from: safeData)
                        DispatchQueue.main.async {
                            content(results)
                        }
                    } catch {
                        errorData()
                        print(error.localizedDescription, urlString)
                    }
                }
            }
            task.resume()
        }
    }
}
