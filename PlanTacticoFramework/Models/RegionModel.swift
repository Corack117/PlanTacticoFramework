//
//  RegionModel.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 09/05/23.
//

import Foundation

class RegionModel: ObservableObject {
    var regionName: String
    
    init() {
        regionName = "Región Taxqueña"
    }
}
