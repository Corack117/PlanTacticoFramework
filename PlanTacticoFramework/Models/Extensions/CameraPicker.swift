//
//  CameraPicker.swift
//  PlanTacticoRealm
//
//  Created by Sergio Ordaz Romero on 12/10/23.
//

import SwiftUI

struct CameraPicker: UIViewControllerRepresentable {
    @Binding var selectedImages: [String]
    @Environment(\.presentationMode) private var presentationMode
    
    func makeUIViewController(context: Context) -> UIImagePickerController {
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .camera
        imagePicker.delegate = context.coordinator

        return imagePicker
    }
    
    func updateUIViewController(_ uiViewController: UIImagePickerController, context: Context) {
    }
    

    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }

    final class Coordinator: NSObject, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

        var parent: CameraPicker

        init(_ parent: CameraPicker) {
            self.parent = parent
        }

        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

            if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                DispatchQueue.main.async {
                    let imagenCorregida = self.corregirOrientacionDeImagen(image)
                    DispatchQueue.main.async {
                        var scale: CGFloat = 1
                        var count: CGFloat = 2
                        while imagenCorregida.size.width * scale > 1820 || imagenCorregida.size.height * scale > 1820 {
                            scale = scale / count
                            count += 1
                        }
                        let newImage = CIImage(cgImage: imagenCorregida.cgImage!) //1
                        let filter = CIFilter(name: "CILanczosScaleTransform") //2
                        filter?.setValue(newImage, forKey: kCIInputImageKey)
                        filter?.setValue(scale, forKey: kCIInputScaleKey) //3
                        let result = filter?.outputImage
                        let converter = UIImage(ciImage: result!)
                        
                        let jpegImage = converter.jpegData(compressionQuality: 0.1)
                        let base64String = jpegImage!.base64EncodedString()
                        
                        self.parent.selectedImages.append(base64String)
                    }
                }
            }

            parent.presentationMode.wrappedValue.dismiss()
        }
        
        func corregirOrientacionDeImagen(_ imagen: UIImage) -> UIImage {
            if imagen.imageOrientation == .up {
                return imagen
            }

            UIGraphicsBeginImageContextWithOptions(imagen.size, false, imagen.scale)
            imagen.draw(in: CGRect(origin: .zero, size: imagen.size))
            let imagenCorregida = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()

            return imagenCorregida ?? imagen
        }

    }
}
