//
//  UIFont.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 18/05/23.
//

import UIKit
import SwiftUI

extension UIFont.TextStyle {
    var toFontTextStyle: Font.TextStyle {
        switch self {
        case .largeTitle:
            return .largeTitle
        case .title1:
            return .title
        case .title2:
            return .title2
        case .title3:
            return .title3
        case .headline:
            return .headline
        case .subheadline:
            return .subheadline
        case .body:
            return .body
        case .callout:
            return .callout
        case .footnote:
            return .footnote
        case .caption1:
            return .caption
        case .caption2:
            return .caption2
        default:
            return .body
        }
    }
    
    var customSize: CGFloat {
        switch self {
            case .largeTitle:
                return 34
            case .title1:
                return 28
            case .title2:
                return 22
            case .title3:
                return 20
            case .body:
                return 17
            case .headline:
                return 17
            case .callout:
                return 16
            case .subheadline:
                return 15
            case .footnote:
                return 13
            case .caption1:
                return 12
            case .caption2:
                return 11
            default:
                return 17
        }
    }
}
