//
//  ImagePicker.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 01/06/23.
//

import UIKit
import SwiftUI
import PhotosUI

struct ImagePicker: UIViewControllerRepresentable {
    @Binding var selectedImages: [String]
    @Environment(\.presentationMode) var presentationMode

    func makeUIViewController(context: Context) -> PHPickerViewController {
        var configuration = PHPickerConfiguration()
        configuration.selectionLimit = 10 - self.selectedImages.count
        configuration.filter = PHPickerFilter.any(of: [.images])
        let picker = PHPickerViewController(configuration: configuration)
        picker.delegate = context.coordinator
        return picker
    }

    func updateUIViewController(_ uiViewController: PHPickerViewController, context: Context) {}

    func makeCoordinator() -> Coordinator {
        Coordinator(parent: self)
    }
    
    class Coordinator: NSObject, PHPickerViewControllerDelegate {
        var parent: ImagePicker

        init(parent: ImagePicker) {
            self.parent = parent
        }

        func picker(_ picker: PHPickerViewController, didFinishPicking results: [PHPickerResult]) {

            for result in results {
                result.itemProvider.loadObject(ofClass: UIImage.self) { (image, error) in
                    if let error = error {
                        // Maneja el error si ocurre alguno
                        print("Error al cargar la imagen: \(error.localizedDescription)")
                    } else if let image = image as? UIImage {
                        let imagenCorregida = self.corregirOrientacionDeImagen(image)
                        DispatchQueue.main.async {
                            var scale: CGFloat = 1
                            var count: CGFloat = 2
                            while imagenCorregida.size.width * scale > 1820 || imagenCorregida.size.height * scale > 1820 {
                                scale = scale / count
                                count += 1
                            }
                            let newImage = CIImage(cgImage: imagenCorregida.cgImage!) //1
                            let filter = CIFilter(name: "CILanczosScaleTransform") //2
                            filter?.setValue(newImage, forKey: kCIInputImageKey)
                            filter?.setValue(scale, forKey: kCIInputScaleKey) //3
                            let result = filter?.outputImage
                            let converter = UIImage(ciImage: result!)
                            
                            let jpegImage = converter.jpegData(compressionQuality: 0.1)
                            let base64String = jpegImage!.base64EncodedString()
                            
                            self.parent.selectedImages.append(base64String)
                        }
                    }
                }
            }

            picker.dismiss(animated: true, completion: nil)
        }
        
        func corregirOrientacionDeImagen(_ imagen: UIImage) -> UIImage {
            if imagen.imageOrientation == .up {
                return imagen
            }

            UIGraphicsBeginImageContextWithOptions(imagen.size, false, imagen.scale)
            imagen.draw(in: CGRect(origin: .zero, size: imagen.size))
            let imagenCorregida = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()

            return imagenCorregida ?? imagen
        }
    }

}
