//
//  DisableScrolling.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 01/06/23.
//

import SwiftUI

struct DisableScrolling: ViewModifier {
    var disabled: Bool
    
    func body(content: Content) -> some View {
        if disabled {
            content
                .simultaneousGesture(DragGesture(minimumDistance: 0))
        } else {
            content
        }
    }
}
