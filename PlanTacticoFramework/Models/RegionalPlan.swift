//
//  RegionalPlans.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 12/06/23.
//

import Foundation
import RealmSwift

class RegionalPlanCoder: Decodable {
    let cecoPadre: String
    let descripcion: String
    let cecos: [CecoCoder]
    
    
    private enum ResponseCodingKey: String, CodingKey {
        case resultado
        
        enum ResultKey: String, CodingKey {
            case cecoPadre, desc, cecos
        }
    }
    
    
    required init(from decoder:Decoder) throws {
        let container =  try decoder.container(keyedBy: ResponseCodingKey.self)
        let resultContainer = try container.nestedContainer(keyedBy: ResponseCodingKey.ResultKey.self, forKey: .resultado)
        
        cecoPadre = try resultContainer.decode(String.self, forKey: .cecoPadre)
        descripcion = try resultContainer.decode(String.self, forKey: .desc)
        cecos = try resultContainer.decode([CecoCoder].self, forKey: .cecos)
    }
    
    class CecoCoder: Decodable {
        let ceco: String
        let desc: String
        let tacticPlansData: [PlanCoder]
        
        enum CecosKey: String, CodingKey {
            case ceco, desc, planesTacticos
        }
        
        required init(from decoder:Decoder) throws {
            let container =  try decoder.container(keyedBy: CecosKey.self)
            
            ceco = try container.decode(String.self, forKey: .ceco)
            let descCeco = try container.decode(String.self, forKey: .desc)
            desc = descCeco.replacingOccurrences(of: "FORMATOS PROPIOS", with: "").capitalized
            tacticPlansData = try container.decode([PlanCoder].self, forKey: .planesTacticos)
        }
    }
}
