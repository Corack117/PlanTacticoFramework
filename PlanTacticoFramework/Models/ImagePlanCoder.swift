//
//  ImagePlanCoder.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 09/06/23.
//

import Foundation

class ImagePlanCoder: Decodable {
    let ejecuciones: [EjecucionPlanCoder]
    
    
    private enum ResponseCodingKey: String, CodingKey {
        case resultado
        
        enum ResultKey: String, CodingKey {
            case ejecucionesPlan
        }
    }
    
    required init(from decoder:Decoder) throws {
        let container =  try decoder.container(keyedBy: ResponseCodingKey.self)
        let resultContainer = try container.nestedContainer(keyedBy: ResponseCodingKey.ResultKey.self, forKey: .resultado)
        ejecuciones = try resultContainer.decode([EjecucionPlanCoder].self, forKey: .ejecucionesPlan)
    }
    
    class EjecucionPlanCoder: Decodable {
        let files: [FileCoder]
        let idResponsable: Int
        let comentaries: String
        
        enum ExecuteKey: String, CodingKey {
            case archivos, comentario, idResponsable
        }
        
        init(files: [FileCoder] = [], idResponsable: Int ,comentaries: String = "") {
            self.files = files
            self.idResponsable = idResponsable
            self.comentaries = comentaries
        }
        
        required init(from decoder:Decoder) throws {
            let container =  try decoder.container(keyedBy: ExecuteKey.self)
            comentaries = try container.decodeIfPresent(String.self, forKey: .comentario) ?? ""
            idResponsable = try container.decode(Int.self, forKey: .idResponsable)
            files = try container.decodeIfPresent([FileCoder].self, forKey: .archivos) ?? []
        }
    }
}

class FileCoder: Decodable {
    let idArchivEjecucion: Int
    let ruta: String
    let base64String: String
    let nombreArchivo: String
    
    
    enum FilesKey: String, CodingKey {
        case ruta, nombreArchivo, idArchivEjecucion, archivo
    }
    
    required init(from decoder:Decoder) throws {
        let container =  try decoder.container(keyedBy: FilesKey.self)
        idArchivEjecucion = try container.decode(Int.self, forKey: .idArchivEjecucion)
        ruta = try container.decode(String.self, forKey: .ruta).replacingOccurrences(of: " ", with: "%20")
        base64String = try container.decode(String.self, forKey: .archivo)
        nombreArchivo = try container.decode(String.self, forKey: .nombreArchivo)
    }
}
