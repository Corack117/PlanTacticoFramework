//
//  TacticalPlan.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 23/05/23.
//

import Foundation
import RealmSwift

class TacticalPlanCoder: Decodable {
    let cecoPadre: String
    let descripcion: String
    let ceco: String
    let desc: String
    let idResponsable: Int
    let tacticPlansData: [PlanCoder]
    
    
    private enum ResponseCodingKey: String, CodingKey {
        case resultado
        
        enum ResultKey: String, CodingKey {
            case cecoPadre, descripcion, cecos
            
            enum CecosKey: String, CodingKey {
                case ceco, desc, idResponsable, planesTacticos
                
                enum PlansKey: String, CodingKey {
                    case idUsuario, acciones, responsables, indicadores, idProgreso, resultadoClv, recomendaciones, idPlanTactico
                    
                    enum IndicatorKey: String, CodingKey {
                        case idIndicador, idResultadoClave
                    }
                }
            }
        }
    }
    
    
    required init(from decoder:Decoder) throws {
        let container =  try decoder.container(keyedBy: ResponseCodingKey.self)
        let resultContainer = try container.nestedContainer(keyedBy: ResponseCodingKey.ResultKey.self, forKey: .resultado)
        let cecoContainer = try resultContainer.nestedContainer(keyedBy: ResponseCodingKey.ResultKey.CecosKey.self, forKey: .cecos)
        
        cecoPadre = try resultContainer.decode(String.self, forKey: .cecoPadre)
        descripcion = try resultContainer.decode(String.self, forKey: .descripcion)
        ceco = try cecoContainer.decode(String.self, forKey: .ceco)
        let descCeco = try cecoContainer.decode(String.self, forKey: .desc)
        desc = (ceco + " " + descCeco).capitalized
        idResponsable = try cecoContainer.decode(Int.self, forKey: .idResponsable)
        tacticPlansData = try cecoContainer.decode([PlanCoder].self, forKey: .planesTacticos)
    }
    
}

class PlanCoder: Decodable {
    let key_result_id: Int
    let indicator_id: Int
    let ceco_id: Int
    let resultado: String
    let actions: [PlanActionCoder]
    let recomendaciones: String
    let progress_id: Int
    let responsables: [Int]
    let status: Status
    let ejecucion: Bool
    
    enum Status: String {
        case onProgress, finished
    }
    
    
    enum PlansKey: String, CodingKey {
        case idUsuario, acciones, responsables, indicadores, idProgreso, resultadoClv, recomendaciones, idPlanTactico, ejecucion
        
        enum IndicatorKey: String, CodingKey {
            case idIndicador, idResultadoClave
        }
    }
    
    
    required init(from decoder:Decoder) throws {
        let container =  try decoder.container(keyedBy: PlansKey.self)
        let indicador =  try container.nestedContainer(keyedBy: PlansKey.IndicatorKey.self, forKey: .indicadores)
        self.key_result_id = try indicador.decode(Int.self, forKey: .idResultadoClave)
        self.indicator_id = try indicador.decode(Int.self, forKey: .idIndicador)
        self.ceco_id = try container.decode(Int.self, forKey: .idPlanTactico)
        self.resultado = try container.decode(String.self, forKey: .resultadoClv)
        self.actions = try container.decode([PlanActionCoder].self, forKey: .acciones)
        self.recomendaciones = try container.decodeIfPresent(String.self, forKey: .recomendaciones) ?? ""
        self.progress_id = try container.decode(Int.self, forKey: .idProgreso)
        self.status = .finished
        self.ejecucion = try container.decodeIfPresent(Bool.self, forKey: .ejecucion) ?? false
        self.responsables = try container.decode([PlanResponsableCoder].self, forKey: .responsables).map { $0.idResponsable }
    }
    
    class PlanResponsableCoder: Decodable {
        let idResponsable: Int
        
        enum ResponsablesKey: String, CodingKey {
            case idResponsable, idReponsable
        }
        
        required init(from decoder:Decoder) throws {
            let container =  try decoder.container(keyedBy: ResponsablesKey.self)
            let idResponsable = try container.decodeIfPresent(Int.self, forKey: .idResponsable)
            if let id = idResponsable {
                self.idResponsable = id
            } else {
                self.idResponsable = try container.decode(Int.self, forKey: .idReponsable)
            }
        }
    }
    
    class PlanActionCoder: Decodable {
        let idAccion: Int
        
        enum ActionsKey: String, CodingKey {
            case idAccion
        }
        
        required init(from decoder:Decoder) throws {
            let container =  try decoder.container(keyedBy: ActionsKey.self)
            idAccion = try container.decode(Int.self, forKey: .idAccion)
        }
    }
}

final class TacticalPlan: Object, Identifiable, Decodable, ObjectKeyIdentifiable {
    @objc dynamic var ceco: String = ""
    @objc dynamic var key_result: KeyResult?
    @objc dynamic var indicatorSelected: IndicatorItem?
    var pdvs: List<PDV> = List<PDV>()
    var responsibles: List<ResponsibleIndicator> = List<ResponsibleIndicator>()
    @objc dynamic var result: String = ""
    var actions: List<Action> = List<Action>()
    var files: List<(FileData)> = List<FileData>()
    var comentaries: List<(CommentData)> = List<CommentData>()
    @objc dynamic var recommendations: String = ""
    @objc dynamic var step: PlanSteps = .indicator
    @objc dynamic var status: Status = .onProgress
    @objc dynamic var progress: Int = 1
    @objc dynamic var ejecucion: Bool = false
    
    override static func primaryKey() -> String? {
        return "ceco"
    }
    
    override init() {}
    
    convenience init(ceco: String, key_result: KeyResult) {
        self.init()
        self.ceco = ceco
        self.key_result = key_result
    }
    
    convenience init(planData: PlanCoder, cecoData: String) {
        self.init()
        let key_result_finded = ManagerModel.shared.key_results.first(where: { $0.id == planData.key_result_id })
        let indicator = ManagerModel.shared.indicators.first(where: { $0.id == planData.indicator_id })
        let action_ids = planData.actions.map { $0.idAccion }
        let actions = ManagerModel.shared.actions.filter { action_ids.contains($0.id) }
        let responsibles = ManagerModel.shared.responsibles.filter { planData.responsables.contains($0.idResponsable) }
        var pdvs: List<PDV> = List<PDV>()
        let pdv_finded = ManagerModel.shared.pdvs.first(where: { $0.ceco == cecoData })
        
        let realm = RealmConfig.shared.realm!
        let plan_with_id = realm.objects(TacticalPlan.self).first(where: { $0.ceco == String(planData.ceco_id) })
        if let plan = plan_with_id {
            pdvs = plan.pdvs
        }
        
        self.ceco = String(planData.ceco_id)
        self.key_result = key_result_finded
        self.indicatorSelected = indicator
        self.result = planData.resultado
        self.ejecucion = planData.ejecucion
        if let pdv = pdv_finded {
            if !pdvs.contains(pdv.thaw()!) {
                pdvs.append(pdv.thaw()!)
            }
        }
        self.pdvs = pdvs
        for responsible in responsibles {
            if !self.responsibles.contains(where: { $0.idResponsable == responsible.idResponsable }) {
                
                self.responsibles.append(responsible)
            }
        }
        for action in actions {
            self.actions.append(action)
        }
        self.recommendations = planData.recomendaciones
        self.progress = planData.progress_id
        self.status = .finished
    }
    
    @objc enum Status: Int, CaseIterable, Codable, RealmEnum {
        case onProgress = 0, finished = 1
    }
    
    
    enum PlansKey: String, CodingKey {
        case idUsuario, acciones, responsables, indicadores, idProgreso, resultadoClv, recomendaciones, idPlanTactico
        
        enum IndicatorKey: String, CodingKey {
            case idIndicador, idResultadoClave
        }
    }
    
    
    convenience init(from decoder:Decoder) throws {
        self.init()
        let container =  try decoder.container(keyedBy: PlansKey.self)
        let indicador =  try container.nestedContainer(keyedBy: PlansKey.IndicatorKey.self, forKey: .indicadores)
        let key_result_id = try indicador.decode(Int.self, forKey: .idResultadoClave)
        let indicator_id = try indicador.decode(Int.self, forKey: .idIndicador)
        
        let ceco_id = try container.decode(Int.self, forKey: .idPlanTactico)
        let key_result_finded = ManagerModel.shared.key_results.first(where: { $0.id == key_result_id })
        let indicator = ManagerModel.shared.indicators.first(where: { $0.id == indicator_id })
        let resultado = try container.decode(String.self, forKey: .resultadoClv)
        let recomendaciones = try container.decodeIfPresent(String.self, forKey: .recomendaciones)
        let progress_id = try container.decode(Int.self, forKey: .idProgreso)
        self.ceco = String(ceco_id)
        self.key_result = key_result_finded
        self.indicatorSelected = indicator
        self.result = resultado
        self.recommendations = recomendaciones ?? ""
        self.progress = progress_id
        self.status = .finished
    }
}

class FileData: Object{
    @objc dynamic var idResponsable: Int = 0
    @objc dynamic var file: String = ""
    
    override init() { }
    
    convenience init(idResponsable: Int, file: String) {
        self.init()
        self.idResponsable = idResponsable
        self.file = file
    }
}

class CommentData: Object{
    @objc dynamic var idResponsable: Int = 0
    @objc dynamic var comment: String = ""
    
    override init() { }
    
    convenience init(idResponsable: Int, comment: String) {
        self.init()
        self.idResponsable = idResponsable
        self.comment = comment
    }
}
