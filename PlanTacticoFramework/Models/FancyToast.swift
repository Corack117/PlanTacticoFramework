//
//  FancyToastStyle.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 14/06/23.
//

import Foundation
import SwiftUI

struct FancyToast: Equatable {
    var type: FancyToastStyle
    var title: String
    var message: String
    var duration: Double = 3
}

class FancyToastManager: ObservableObject {
    @Published var toast: FancyToast? = nil
    
    static var shared = FancyToastManager()
}

enum FancyToastStyle {
    case error
    case warning
    case success
    case info
}

extension FancyToastStyle {
    var themeColor: Color {
        switch self {
        case .error: return Color.red
        case .warning: return Color.orange
        case .info: return Color.blue
        case .success: return Color.green
        }
    }
    
    var iconFileName: String {
        switch self {
        case .info: return "info.circle.fill"
        case .warning: return "exclamationmark.triangle.fill"
        case .success: return "checkmark.circle.fill"
        case .error: return "xmark.circle.fill"
        }
    }
}
