//
//  CarouselModel.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 13/07/23.
//

import Foundation
import UIKit

class CarouselModel: ObservableObject {
    static var shared = CarouselModel()
    
    @Published var carousel = false 
    @Published var principalImage: String = ""
    @Published var images: [String] = []
    @Published var principalUIImage: UIImage = UIImage()
    @Published var uiImages: [UIImage] = []
}
