//
//  ResponsibleIndicator.swift
//  Plan Tactico
//
//  Created by Sergio Ordaz Romero on 11/05/23.
//

import Foundation
import RealmSwift

struct ResponsibleIndicatorCoder: Decodable {
    let responsibles: [ResponsibleIndicator]
    
    enum CodingKeys: CodingKey {
        case resultado
        
        enum ArrayKeys: CodingKey {
            case indicadorResponsablesCatalog
        }
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let nestedContainer = try container.nestedContainer(keyedBy: CodingKeys.ArrayKeys.self, forKey: .resultado)
        self.responsibles = try nestedContainer.decode([ResponsibleIndicator].self, forKey: .indicadorResponsablesCatalog)
    }
}

final class ResponsibleIndicator: Object, Identifiable, Codable, ObjectKeyIdentifiable {
    @objc dynamic var id: Int = 0
    @objc dynamic var idResponsable: Int = 0
    @objc dynamic var idIndicador: Int = 0
    @objc dynamic var responsable: String = ""
    @objc dynamic var isSelected: Bool = false
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    enum CodingKeys: CodingKey {
        case idResponsable, idIndicador, responsable, isSelected
    }
    
    override init() { }
    
    init(id: Int, idIndicator: Int, idResponsable: Int, responsable: String, isSelected: Bool = false) {
        super.init()
        self.id = id
        self.idResponsable = idResponsable
        self.idIndicador = idIndicator
        self.responsable = responsable
        self.isSelected = isSelected
    }
    
    init(from decoder: Decoder) throws {
        super.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        idResponsable = try container.decode(Int.self, forKey: .idResponsable)
        idIndicador = try container.decode(Int.self, forKey: .idIndicador)
        id = Int(String(idResponsable) + String(idIndicador))!
        responsable = try container.decode(String.self, forKey: .responsable)
        do {
            isSelected = try container.decode(Bool.self, forKey: .isSelected)
        } catch {
            isSelected = false
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .idResponsable)
        try container.encode(idResponsable, forKey: .idResponsable)
        try container.encode(idIndicador, forKey: .idIndicador)
        try container.encode(responsable, forKey: .responsable)
        try container.encode(isSelected, forKey: .isSelected)
    }
}

struct ResponsibleIndicatorObject: Equatable {
    var id: Int
    var idResponsable: Int
    var idIndicador: Int
    var responsable: String
    var isSelected: Bool
}

