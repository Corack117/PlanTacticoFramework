//
//  TacticalRealmApp.swift
//  TacticalRealm
//
//  Created by Sergio Ordaz Romero on 23/07/23.
//

import SwiftUI
import RealmSwift

public func initPTNavigation(employeeNumber: Int, dismiss: @escaping () -> Void) -> UIViewController {
    return UIHostingController(rootView: TacticalRealmApp(employeeNumber: employeeNumber, dismiss: dismiss))
}

struct TacticalRealmApp: View {
    @State var employeeNumber: Int
    var dismiss: () -> Void = {}
    let config = Realm.Configuration(
        // Otras configuraciones de Realm...
        schemaVersion: 1,
        objectTypes: [
            Action.self,
            KeyResult.self,
            IndicatorItem.self,
            PDVsCoder.self,
            PDV.self,
            ResponsibleIndicator.self,
            TacticalPlan.self,
            FileData.self,
            CommentData.self,
            UserProfile.self,
        ] // Agrega KeyResult a la lista de clases administradas
    )
    
    var body: some View {
        ContentView(employeeNumber: employeeNumber, dismiss: dismiss)
            .environment(\.realmConfiguration, config)
    }
}
