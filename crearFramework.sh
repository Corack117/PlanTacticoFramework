if [[ $1 == s ]] ; then
echo "////Archivando para simulador"
xcodebuild archive \
 -workspace PlanTacticoFramework.xcworkspace \
 -scheme PlanTacticoFramework\
 -archivePath ~/Desktop/ArchiveFrameworks/PlanTacticoFramework-iphonesimulator.xcarchive \
 -sdk iphonesimulator \
 SKIP_INSTALL=NO
fi

if [[ $1 == d ]] ; then
echo "///Archivando para Dispositivos"
xcodebuild archive \
 -workspace PlanTacticoFramework.xcworkspace \
 -scheme PlanTacticoFramework\
 -archivePath ~/Desktop/ArchiveFrameworks/PlanTacticoFramework-iphoneos.xcarchive \
 -sdk iphoneos \
 SKIP_INSTALL=NO
fi

